﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using NFinal;

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinalCorePlugTest.Views.Index
{
    /// <summary>
    /// 模板类
    /// </summary>
    [View("/NFinalCorePlugTest/Views/Index/Default.cshtml")]
    public class Default_cshtml : NFinal.View.RazorView<NFinalCorePlugTest.Controllers.IndexController_Model.Default>
    {
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">数据</param>
        public Default_cshtml(NFinal.IO.Writer writer, NFinalCorePlugTest.Controllers.IndexController_Model.Default Model) : base(writer, Model)
        {
            Layout = "/NFinalCorePlugTest/Views/Shared/_Start.cshtml";
        }
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            this.ExecuteAsync().Wait();
        }
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
        {
            await writer.WriteAsync("");
             await writer.WriteAsync("\r\n<h2>Message:");
            await writer.WriteAsync(Model.Message);
            await writer.WriteAsync("</h2>");
        }
    }
}
