﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFinal;
using Dapper;
using System.Threading.Tasks;

namespace NFinalCorePlugTest.Controllers
{
    [NFinal.ActionExport("UpdateA")]
    public class IndexController : BaseController
    {
        [Code.UserCheck]
        [Code.AfterAction]
        public async Task<ActionResult> Default()
        {
            this.ViewBag.Message = "Hello World!";
            this.ViewBag.Title = "Title";
            this.ViewBag.ViewData = new NFinal.Collections.FastDictionary<string, object>();
            return View();
        }
        public ActionResult Ajax()
        {
            this.ViewBag.Message = "Hello Json!";
            this.ViewBag.id = 2;
            this.ViewBag.time = DateTime.Now;
            return Json();
        }
        public ActionResult SetSession()
        {
            //Code.User user = new Code.User();
            //user.Name = "lucas";
            //this.Session.SetUser(user);
            return null;
        }
        public ActionResult WriteSession()
        {
            //var user = this.Session.GetUser<Code.User>();
            //Write(user.Name);
            return null;
        }
    }
}
 