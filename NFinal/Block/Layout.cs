﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Models
{
    /// <summary>
    /// Layout
    ///</summary>
    public class Layout
    {
            /// <summary>
            /// Id
            /// </summary>
            
        public System.Int64 Id { get; set; }
            /// <summary>
            /// Path
            /// </summary>
        public System.String Path { get; set; }
            /// <summary>
            /// Name
            /// </summary>
        public System.String Name { get; set; }
    }
}