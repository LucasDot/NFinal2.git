﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Models
{
    /// <summary>
    /// Component
    ///</summary>
    public class Component
    {
            /// <summary>
            /// Id
            /// </summary>
            
        public System.Int64 Id { get; set; }
            /// <summary>
            /// Name
            /// </summary>
        public System.String Name { get; set; }
            /// <summary>
            /// TypeName
            /// </summary>
        public System.String TypeName { get; set; }
            /// <summary>
            /// Parameters
            /// </summary>
        public System.String Parameters { get; set; }
    }
}