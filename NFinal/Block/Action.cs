﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Models
{
    /// <summary>
    /// Action
    ///</summary>
    public class Action
    {
            /// <summary>
            /// Id
            /// </summary>
            
        public System.Int64 Id { get; set; }
            /// <summary>
            /// actionUrl
            /// </summary>
        public System.String actionUrl { get; set; }
            /// <summary>
            /// ClassName
            /// </summary>
        public System.String ClassName { get; set; }
            /// <summary>
            /// AreaName
            /// </summary>
        public System.String AreaName { get; set; }
            /// <summary>
            /// ControllerName
            /// </summary>
        public System.String ControllerName { get; set; }
            /// <summary>
            /// ActionName
            /// </summary>
        public System.String ActionName { get; set; }
            /// <summary>
            /// MethodName
            /// </summary>
        public System.String MethodName { get; set; }
            /// <summary>
            /// ContentType
            /// </summary>
        public System.String ContentType { get; set; }
            /// <summary>
            /// UrlString
            /// </summary>
        public System.String UrlString { get; set; }
            /// <summary>
            /// Compress
            /// </summary>
        public System.String Compress { get; set; }
            /// <summary>
            /// Method
            /// </summary>
        public System.String Method { get; set; }
            /// <summary>
            /// Layout
            /// </summary>
        public System.String Layout { get; set; }
    }
}