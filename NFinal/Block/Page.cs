﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Models
{
    /// <summary>
    /// Page
    ///</summary>
    public class Page
    {
            /// <summary>
            /// Id
            /// </summary>
            
        public System.Int64 Id { get; set; }
            /// <summary>
            /// Title
            /// </summary>
        public System.String Title { get; set; }
            /// <summary>
            /// Descrption
            /// </summary>
        public System.String Descrption { get; set; }
            /// <summary>
            /// KeyWord
            /// </summary>
        public System.String KeyWord { get; set; }
            /// <summary>
            /// ControllerName
            /// </summary>
        public System.String ControllerName { get; set; }
            /// <summary>
            /// MethodName
            /// </summary>
        public System.String MethodName { get; set; }
            /// <summary>
            /// Parameters
            /// </summary>
        public System.String Parameters { get; set; }
    }
}