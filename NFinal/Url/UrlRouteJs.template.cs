﻿using System.Collections.Generic;
using System;
using System.IO;
using System.Net;
using NFinal;
using System.Threading.Tasks;

//此代码由NFinalRazorGenerator生成。
//http://bbs.nfinal.com
namespace NFinal.Url
{
    [View("/NFinal/Url/UrlRouteJs.cshtml")]
    public class UrlRouteJs : NFinal.View.RazorView<NFinal.Url.UrlRouteJsModel>
    {
        public UrlRouteJs(NFinal.IO.Writer writer, NFinal.Url.UrlRouteJsModel Model) : base(writer, Model)
        {
            
        }
        public override void Execute()
        {
            this.ExecuteAsync().Wait();
        }
        //如果此处报错，请添加NFinal引用
        //PMC命令为：Install-Package NFinal
        public override async Task ExecuteAsync()
        {
            await writer.WriteAsync("");
            await writer.WriteAsync("function StringFormat() {\r\n    if (arguments.length == 0)\r\n        return null;\r\n    var str = arguments[0];\r\n    for (var i = 1; i < arguments.length; i++) {\r\n        var re = new RegExp(\'\\\\{\' + (i - 1) + \'\\\\}\', \'gm\');\r\n        str = str.replace(re, arguments[i]);\r\n    }\r\n    return str;\r\n}\r\nvar Url={\r\n");
            bool isFirstAction = true; bool isFirstController = true; await writer.WriteAsync("\r\n");
            foreach (KeyValuePair<RuntimeTypeHandle, Dictionary<string, NFinal.Url.FormatData>> formatController in Model.formatControllerDictionary)
            {
                Type controllerType = Type.GetTypeFromHandle(formatController.Key);
                string controllerName = controllerType.Namespace.Replace('.', '_') + "_" + controllerType.Name;

                if (isFirstController)
                {
                    isFirstController = false;
                }
                else
                {
                    await writer.WriteAsync("        ");
                    await writer.WriteAsync(",\r\n");
                }
                isFirstAction = true;
                await writer.WriteAsync("    ");
                await writer.WriteAsync("\"");
                await writer.WriteAsync(controllerName);
                await writer.WriteAsync("\":{\r\n");
                foreach (KeyValuePair<string, NFinal.Url.FormatData> formatMethod in formatController.Value)
                {
                    if (isFirstAction)
                    {
                        isFirstAction = false;
                    }
                    else
                    {
                        await writer.WriteAsync("            ");
                        await writer.WriteAsync(",\r\n");
                    }

                    if (formatMethod.Value.actionUrlNames != null && formatMethod.Value.actionUrlNames.Length > 0)
                    {
                        await writer.WriteAsync("            ");
                        await writer.WriteAsync("\"");
                        await writer.WriteAsync(formatMethod.Key);
                        await writer.WriteAsync("\":function(");
                        await writer.WriteAsync(string.Join(",", formatMethod.Value.actionUrlNames));
                        await writer.WriteAsync(")\r\n            ");
                        await writer.WriteAsync("{\r\n            ");
                        await writer.WriteAsync("return StringFormat(\"");
                        await writer.WriteAsync(formatMethod.Value.formatUrl);
                        await writer.WriteAsync("\",");
                        await writer.WriteAsync(string.Join(",", formatMethod.Value.actionUrlNames));
                        await writer.WriteAsync(");\r\n            ");
                        await writer.WriteAsync("}\r\n");
                    }
                    else
                    {
                        await writer.WriteAsync("            ");
                        await writer.WriteAsync("\"");
                        await writer.WriteAsync(formatMethod.Key);
                        await writer.WriteAsync("\":function()\r\n            ");
                        await writer.WriteAsync("{\r\n            ");
                        await writer.WriteAsync("return \"");
                        await writer.WriteAsync(formatMethod.Value.formatUrl);
                        await writer.WriteAsync("\";\r\n            ");
                        await writer.WriteAsync("}\r\n");
                    }
                }
                await writer.WriteAsync("    ");
                await writer.WriteAsync("}\r\n");
            }
            await writer.WriteAsync("};");
        }
    }
}
