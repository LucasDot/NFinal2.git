﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using NFinal;

//此代码由NFinalRazorGenerator生成。
//http://bbs.nfinal.com
namespace NFinal.Url
{
    /// <summary>
    /// 模板类
    /// </summary>
	[View("/NFinal/Url/Debug.cshtml")]
	public class Debug : NFinal.View.RazorView<NFinal.Url.DebugData>
	{
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">数据</param>
		public Debug(NFinal.IO.Writer writer,NFinal.Url.DebugData Model) : base(writer ,Model)
		{
		}
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            this.ExecuteAsync().Wait();
        }
        
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
		{
			await writer.WriteAsync("");
            await writer.WriteAsync("﻿<!DOCTYPE html>\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n    <meta http-equiv=\"pragma\" content=\"no-cache\">\r\n    <meta http-equiv=\"Cache-Control\" content=\"no-cache, must-revalidate\">\r\n    <meta http-equiv=\"expires\" content=\"Wed, 26 Feb 1997 08:21:57 GMT\">\r\n    <title></title>\r\n    <script src=\"");
            await writer.WriteAsync(Model.debugUrl);
            await writer.WriteAsync("/Scripts/Url.js\"></script>\r\n</head>\r\n<body>\r\n    此文件负责跳转到 ");
            await writer.WriteAsync(Model.classFullName);
            await writer.WriteAsync(" 下的 ");
            await writer.WriteAsync(Model.methodName);
            await writer.WriteAsync(" 方法\r\n    把此文件设为首页，即可调试对应的函数。\r\n</body>\r\n</html>\r\n<script>\r\n");
    if (Model.formatData.actionUrlNames != null && Model.formatData.actionUrlNames.Length > 0)
    {
        foreach (string name in Model.formatData.actionUrlNames)
        {
                    await writer.WriteAsync("            ");
                    await writer.WriteAsync("var ");
                    await writer.WriteAsync(name);
                    await writer.WriteAsync(" = \'");
                    await writer.WriteAsync(name);
                    await writer.WriteAsync("\';\r\n");
                }
                await writer.WriteAsync("        ");
                await writer.WriteAsync("var urlString = Url.");
                await writer.WriteAsync(Model.classFullName.Replace('.', '_'));
                await writer.WriteAsync(".");
                await writer.WriteAsync(Model.methodName);
                await writer.WriteAsync(" (");
                await writer.WriteAsync(string.Join(",", Model.formatData.actionUrlNames));
                await writer.WriteAsync(");\r\n                    ");
                await writer.WriteAsync("window.location.href =\"");
                await writer.WriteAsync(Model.debugUrl);
                await writer.WriteAsync("\"+ urlString;\r\n");
            }
    else
    {
                await writer.WriteAsync("        ");
                await writer.WriteAsync("var urlString = Url.");
                await writer.WriteAsync(Model.classFullName.Replace('.', '_'));
                await writer.WriteAsync(".");
                await writer.WriteAsync(Model.methodName);
                await writer.WriteAsync(" ();\r\n                ");
                await writer.WriteAsync("window.location.href =\"");
                await writer.WriteAsync(Model.debugUrl);
                await writer.WriteAsync("\" + urlString;\r\n");
        }
            await writer.WriteAsync("</script>");
		}
	}
}
