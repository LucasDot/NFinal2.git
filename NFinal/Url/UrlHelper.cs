﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// URL生成帮助类
    /// </summary>
    public class UrlHelper :NFinal.DependencyInjection.ServiceCollectionHandler, IUrlHelper
    {
        /// <summary>
        /// 获取Url
        /// </summary>
        /// <typeparam name="TController">控制器类型</typeparam>
        /// <param name="methodName">Action函数名</param>
        /// <param name="parameters">Url所需的参数</param>
        /// <returns>返回调用该Action的Url</returns>
        public string GetUrl<TController>(string methodName, params StringContainer[] parameters)
        {
            string url;
            if (Application.globalConfig.debug.enable)
            {
                url = Application.globalConfig.debug.url;
            }
            else
            {
                url = Application.globalConfig.server.url;
            }
            Dictionary<string, NFinal.Url.FormatData> dictionaryFormatData = null;
            if (NFinal.Url.ActionUrlHelper.formatControllerDictionary.TryGetValue(typeof(TController).TypeHandle, out dictionaryFormatData))
            {
                NFinal.Url.FormatData formatData = null;
                if (dictionaryFormatData.TryGetValue(methodName, out formatData))
                {
                    return url + NFinal.Url.ActionUrlHelper.Format(formatData.formatUrl, parameters);
                }
            }
            var exception= new NFinal.Exceptions.UrlNotFoundException(typeof(TController), methodName, parameters);
            ServiceCollection.logger.Fatal(exception.Message);
            throw exception;
        }
        /// <summary>
        /// 获取Content路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string Content(string path)
        {
            string url;
            if (Application.globalConfig.debug.enable)
            {
                url = Application.globalConfig.debug.url;
            }
            else
            {
                url = Application.globalConfig.server.url;
            }
            return url + NFinal.IO.Path.MapPath((int)Application.globalConfig.projectType,Application.globalConfig.debug.enable, path);
        }
    }
}
