﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// MVC返回结果
    /// </summary>
    public interface IActionResult
    {
        /// <summary>
        /// 文档类型
        /// </summary>
        string ContentType { get; set; }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TRequest">Http请求类型</typeparam>
        /// <typeparam name="TContext">Http上下文类型</typeparam>
        /// <param name="context">NFinal中的Action对象</param>
        void ExecuteResult<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context);
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http上下文类型</typeparam>
        /// <typeparam name="TRequest">Http请求类型</typeparam>
        /// <param name="context">NFinal中的Action对象</param>
        /// <returns></returns>
        Task ExecuteResultAsync<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context);
    }
}