﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.IO;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// 图片类型
    /// </summary>
    public enum ImageType
    {
        /// <summary>
        /// .jpg
        /// </summary>
        Jpeg,
        /// <summary>
        /// .png
        /// </summary>
        Png,
        /// <summary>
        /// .bmp
        /// </summary>
        Bmp,
        /// <summary>
        /// .gif
        /// </summary>
        Gif,
        /// <summary>
        /// .webp
        /// </summary>
        Webp
    }
    /// <summary>
    /// 图像
    /// </summary>
    public class ImageResult : ActionResult
    {
        /// <summary>
        /// 图像流
        /// </summary>
        public Stream stream;
        /// <summary>
        /// 图像
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="imageType"></param>
        public ImageResult(Stream stream,ImageType imageType)
        {
            this.stream = stream;
            switch (imageType)
            {
                case ImageType.Bmp:this.ContentType = "image/bmp"; break;
                case ImageType.Gif:this.ContentType = "image/gif";break;
                case ImageType.Png:this.ContentType = "image/png";break;
                case ImageType.Jpeg:this.ContentType = "image/jpeg";break;
                default:this.ContentType = "image/webp";break;
            }
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            context.SetResponseHeader(Constant.HeaderContentType, this.ContentType);
            if (this.ContentType?.Length > 0)
            {
                context.contentType = this.ContentType;
            }
            this.stream.Seek(0, SeekOrigin.Begin);
            try
            {
                byte[] buffer = new byte[512];
                int count = 0;
                while ((count = this.stream.Read(buffer, 0, 512)) > 0)
                {
                    context.Write(buffer, 0, count);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.stream.Dispose();
            }
        }
    }
}
