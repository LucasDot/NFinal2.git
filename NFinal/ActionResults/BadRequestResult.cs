﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 损坏的请求头
    /// </summary>
    public class BadRequestResult :StatusCodeResult
    {
        /// <summary>
        /// 损坏的请求头
        /// </summary>
        public BadRequestResult() : base(400)
        { }
    }
}
