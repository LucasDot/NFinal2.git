﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 正常返回
    /// </summary>
    public class OKResult : StatusCodeResult
    {
        /// <summary>
        /// 正常返回
        /// </summary>
        public OKResult() : base(200)
        { }
    }
}
