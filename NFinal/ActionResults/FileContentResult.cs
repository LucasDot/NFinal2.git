﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NFinal.Action;

namespace NFinal
{
    /// <summary>
    /// 文件
    /// </summary>
    public class FileContentResult : FileResult
    {
        /// <summary>
        /// 字节流
        /// </summary>
        public byte[] FileContents { get; set; }
        /// <summary>
        /// 文件
        /// </summary>
        /// <param name="fileContents">文件存储字节</param>
        /// <param name="fileName">文件名</param>
        /// <param name="contentType">文件类型</param>
        public FileContentResult(byte[] fileContents,string fileName=null,string contentType=null) :base(contentType)
        {
            if (fileContents==null)
            {
                throw new ArgumentNullException(nameof(fileContents));
            }
            this.FileDownloadName = fileName;
            if (contentType == null)
            {
                this.ContentType = "application/octet-stream";
            }
            else
            {
                this.ContentType = contentType;
            }
            this.FileContents = fileContents;
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            if (FileDownloadName != null)
            {
                context.SetResponseHeader(Constant.HeaderContentDisposition, "attachment;filename=" + Uri.EscapeUriString(this.FileDownloadName));
            }
            context.SetResponseHeader(Constant.HeaderContentType, this.ContentType);
            context.Write(FileContents);
        }
    }
}
