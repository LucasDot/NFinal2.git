﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    public static class JsonResultExtension
    {
        public static JsonResult ToJsonResult(this object obj)
        {
            string json= NFinal.DependencyInjection.ServiceCollectionHandler.ServiceCollection.jsonSerialize.SerializeObject(obj);
            return new JsonResult(json);
        }
    }
}
