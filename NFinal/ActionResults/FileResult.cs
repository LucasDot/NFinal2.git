﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.IO;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// 文件，虚拟类
    /// </summary>
    public abstract class FileResult : ActionResult
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileDownloadName { get; set; }
        /// <summary>
        /// 文件，虚拟类
        /// </summary>
        /// <param name="contentType">文件类型</param>
        public FileResult(string contentType= "application/octet-stream")
        {
            this.ContentType = contentType;
        }
    }
}
