﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NFinal.Action;

namespace NFinal
{
    /// <summary>
    /// 重定向
    /// </summary>
    public class RedirectResult : ActionResult
    {
        /// <summary>
        /// 重定向的Url
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 重定向
        /// </summary>
        /// <param name="url">重定向后的url</param>
        public RedirectResult(string url)
        {
            this.Url = url;
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            context.SetResponseStatusCode(302);
            context.SetResponseHeader(NFinal.Constant.HeaderContentType, NFinal.Constant.ResponseContentType_Text_html);
            context.SetResponseHeader(NFinal.Constant.HeaderLocation, this.Url);
        }
    }
}
