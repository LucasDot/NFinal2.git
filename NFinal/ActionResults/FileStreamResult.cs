﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.IO;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// 流文件
    /// </summary>
    public class FileStreamResult : FileResult
    {
        /// <summary>
        /// 存储流
        /// </summary>
        public Stream Stream { get; set; }
        /// <summary>
        /// 流文件
        /// </summary>
        /// <param name="stream">存储流</param>
        /// <param name="fileName">文件名</param>
        /// <param name="contentType">文件类型</param>
        public FileStreamResult(Stream stream,string fileName=null,string contentType=null):base(contentType)
        {
            this.FileDownloadName = fileName;
            if (contentType == null)
            {
                this.ContentType = "application/octet-stream";
            }
            else
            {
                this.ContentType = contentType;
            }
            this.Stream = stream;
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            context.SetResponseHeader(Constant.HeaderContentType,this.ContentType);
            if (FileDownloadName != null)
            {
                context.SetResponseHeader(Constant.HeaderContentDisposition, "attachment;filename=" + Uri.EscapeUriString(this.FileDownloadName));
            }
            context.SetResponseHeader(Constant.HeaderContentLength, this.Stream.Length.ToString());
            this.Stream.Seek(0, SeekOrigin.Begin);
            byte[] buffer = new byte[512];
            int count = 0;
            try
            {
                while ((count = this.Stream.Read(buffer, 0, 512)) > 0)
                {
                    context.Write(buffer, 0, count);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Stream.Dispose();
            }
        }
    }
}
