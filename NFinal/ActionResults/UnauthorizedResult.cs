﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;

namespace NFinal
{
    /// <summary>
    /// 验证结果类
    /// </summary>
    public class UnauthorizedResult: StatusCodeResult
    {
        /// <summary>
        /// 验证结果类
        /// </summary>
        public UnauthorizedResult() : base(403) { }
    }
}
