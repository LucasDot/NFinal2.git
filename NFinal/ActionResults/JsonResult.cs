﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// Json文本
    /// </summary>
    public class JsonResult : ContentResult
    {
        /// <summary>
        /// Json文本
        /// </summary>
        /// <param name="json"></param>
        public JsonResult(string json=null):base(json, "application/json; charset=utf-8")
        {
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override async Task ExecuteResultAsync<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            if (this.Content == null)
            {
                //发现一个异步Bug，帮忙看看。
                //写法1：
                string json = ServiceCollection.jsonSerialize.SerializeObject(context.ViewBag);
                await context.WriteAsync(json);
                //写法2：
                //这种写法如果高压执行会有Bug.Bug详细信息为：
                //无法将类型为“System.Runtime.CompilerServices.TaskAwaiter”的对象强制转换为
                //类型“System.Runtime.CompilerServices.INotifyCompletion”。
                //await context.WriteAsync(Newtonsoft.Json.JsonConvert.SerializeObject(context.ViewBag,
                //new Newtonsoft.Json.Converters.JavaScriptDateTimeConverter()));
            }
            else
            {
                await context.WriteAsync(this.Content);
            }
            context.contentType = this.ContentType;
        }
    }
}
