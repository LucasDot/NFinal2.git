﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace NFinal
{
    /// <summary>
    /// MVC结果虚拟类
    /// </summary>
    public abstract class ActionResult : NFinal.DependencyInjection.ServiceCollectionHandler, IActionResult
    {
        /// <summary>
        /// 文档类型
        /// </summary>
        public string ContentType { get; set; }
        /// <summary>
        /// 输出文档异步
        /// </summary>
        /// <typeparam name="TRequest">Http请求类型</typeparam>
        /// <typeparam name="TContext">Http上下文类型</typeparam>
        /// <param name="context">NFinal中的Action对象</param>
        //public abstract void ExecuteResult<TContext, TRequest>(NFinal.Action.AbstractAction<TContext,TRequest> context);
        public virtual Task ExecuteResultAsync<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context)
        {
            ExecuteResult(context);
            return Constant.CompletedTask;
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public virtual void ExecuteResult<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context)
        {

        }
        /// <summary>
        /// 返回以text/html格式的字符串内容
        /// </summary>
        /// <param name="empty"></param>
        public static implicit operator ActionResult(string empty)
        {
            return new EmptyResult();
        }
        /// <summary>
        /// 返回状态码，200=正常,403=验证失败,404=文件找不到，400=请求内容异常
        /// </summary>
        /// <param name="statusCode"></param>
        public static implicit operator ActionResult(int statusCode)
        {
            return new StatusCodeResult(statusCode);
        }
        /// <summary>
        /// 返回字节流文件
        /// </summary>
        /// <param name="content"></param>
        public static implicit operator ActionResult(byte[] content)
        {
            return new FileContentResult(content);
        }
        /// <summary>
        /// 返回流文件
        /// </summary>
        /// <param name="stream"></param>
        public static implicit operator ActionResult(System.IO.Stream stream)
        {
            return new FileStreamResult(stream);
        }
        /// <summary>
        /// 返回Json数据,
        /// 其格式为:{"Success":true,"Message":"",Code:0}
        /// 其中Message值为this.Message设置的值
        /// 其中Code值为this.Code设置的值
        /// </summary>
        /// <param name="success">返回Json中Success的值</param>
        public static implicit operator ActionResult(bool success)
        {
            return new BooleanResult(success);
        }
    }
}
