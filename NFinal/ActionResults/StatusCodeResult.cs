﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;

namespace NFinal
{
    /// <summary>
    /// 状态码结果类
    /// </summary>
    public class StatusCodeResult : ActionResult
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// 状态码结果类
        /// </summary>
        /// <param name="statusCode">状态码</param>
        public StatusCodeResult(int statusCode)
        {
            this.StatusCode = statusCode;
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            context.response.statusCode = this.StatusCode;
            context.response.stream = System.IO.Stream.Null;
        }
    }
}
