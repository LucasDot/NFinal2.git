﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 文本结果类
    /// </summary>
    public class TextResult : ContentResult
    {
        /// <summary>
        /// 文本结果类
        /// </summary>
        /// <param name="content">内容</param>
        public TextResult(string content) : base(content, "text/plain; charset=utf-8")
        {
        }
    }
}
