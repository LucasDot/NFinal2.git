﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 找不到文件
    /// </summary>
    public class NotFoundResult:StatusCodeResult
    {
        /// <summary>
        /// 找不到文件
        /// </summary>
        public NotFoundResult() : base(404)
        {

        }
    }
}
