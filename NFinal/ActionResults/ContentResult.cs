﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// 文本内容
    /// </summary>
    public class ContentResult: ActionResult
    {
        /// <summary>
        /// 文本内容
        /// </summary>
        /// <param name="content">文本</param>
        /// <param name="contentType">类型</param>
        public ContentResult(string content,string contentType)
        {
            this.Content = content;
            this.ContentType = contentType;
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 执行输出
        /// </summary>
        /// <typeparam name="TContext">Http上下文</typeparam>
        /// <typeparam name="TRequest">Http请头头</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            if (this.ContentType?.Length > 0)
            {
                context.contentType = this.ContentType;
            }
            context.SetResponseHeader(Constant.HeaderContentType, this.ContentType);
            context.Write(this.Content);
        }
    }
}