﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// 空
    /// </summary>
    public class EmptyResult : ActionResult
    {
        /// <summary>
        /// 空
        /// </summary>
        public EmptyResult()
        {
           
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override void ExecuteResult<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            context.SetResponseHeader(Constant.HeaderContentType, Constant.ResponseContentType_Text_html);
            context.response.stream=System.IO.Stream.Null;
        }
    }
}
