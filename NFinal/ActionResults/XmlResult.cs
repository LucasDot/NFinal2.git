﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// xml内容结果类
    /// </summary>
    public class XmlResult: ContentResult
    {
        /// <summary>
        /// xml内容结果类
        /// </summary>
        /// <param name="content">xml文本</param>
        public XmlResult(string content):base(content,"text/xml;")
        {

        }
        /// <summary>
        /// xml内容结果类
        /// </summary>
        /// <param name="doc">xml文档对象</param>
        public XmlResult(System.Xml.XmlDocument doc):base(doc.OuterXml,"text/xml")
        {
        }
    }
}
