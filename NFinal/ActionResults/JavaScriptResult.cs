﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// Javascript脚本
    /// </summary>
    public class JavaScriptResult:ContentResult
    {
        /// <summary>
        /// Javascript脚本
        /// </summary>
        /// <param name="content">脚本内容</param>
        public JavaScriptResult(string content):base(content,"application/x-javascript")
        {

        }
    }
}
