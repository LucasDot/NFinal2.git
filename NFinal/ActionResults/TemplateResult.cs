﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    ///  模板结果类
    /// </summary>
    public class TemplateResult : ActionResult
    {
        /// <summary>
        /// 模板路径
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 模板结果类
        /// </summary>
        /// <param name="contentType">输出类型</param>
        public TemplateResult(string contentType="text/html; charset=utf-8")
        {
            this.Url = null;
        }
        /// <summary>
        /// 模板结果类
        /// </summary>
        /// <param name="url">模板路径</param>
        /// <param name="contentType">输出类型</param>
        public TemplateResult(string url, string contentType = "text/html; charset=utf-8")
        {
            this.Url = url;
        }
        /// <summary>
        /// 输出文档
        /// </summary>
        /// <typeparam name="TContext">Http请求类型</typeparam>
        /// <typeparam name="TRequest">Http上下文类型</typeparam>
        /// <param name="context"></param>
        public override  Task ExecuteResultAsync<TContext, TRequest>(AbstractAction<TContext, TRequest> context)
        {
            if (this.ContentType?.Length > 0)
            {
                context.contentType = this.ContentType;
            }
            if (Url == null)
            {
                return context.Render();
            }
            else
            {
                return context.Render(this.Url);
            }
        }
    }
}
