﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
using System.Threading.Tasks;
using NFinal.Logs.NLogger;
using System.Reflection;
using System.Linq;
namespace NFinal
{
    public class Application: NFinal.DependencyInjection.ServiceCollectionHandler
    {
        private static NFinal.Logs.ILogger initLogger;
        private static bool isCreated = false;  
        private static bool isInit = false;
        /// <summary>
        /// 应用程序
        /// </summary>
        /// <param name="logger">初始化记录</param>
        public Application()
        {
            Create();
        }
        /// <summary>
        /// 创建应用程序
        /// </summary>
        public static void Create()
        { 
            //防止重复调用.
            if (!isCreated)
            {
                isCreated = true;
                //设置初始化日志服务
                string initLoggerName = "init";
                initLogger = ServiceCollection.GetLogger(initLoggerName);
                if (initLogger == null)
                {
                    ServiceCollection.SetLoggersNlogger();
                    initLogger = ServiceCollection.GetLogger(initLoggerName);
                }
                isCreated = true;
                //设置默认服务
                initLogger.Info("开始注入默认服务,如果已经注入,则自动忽略.");
                ServiceCollection.SetDefault();
                string applicationLoggerName = "application";
                ServiceCollection.logger = ServiceCollection.GetLogger(applicationLoggerName);
                //初始化全局服务缓存,加快获取缓存效率
                initLogger.Info("对常用服务进行缓存");
                ServiceCollection.serializable = ServiceCollection.GetSerializable();
                ServiceCollection.cache = ServiceCollection.GetCache(ServiceCollection.serializable);
                ServiceCollection.sessionConstructor = ServiceCollection.GetGetSessionMethod();
                ServiceCollection.jsonSerialize = ServiceCollection.GetJsonSerialize();
                initLogger.Info("加载全局配置文件nfinal.json及插件配置文件plug.json");
                //初始化全局配置以及插件配置
                configrationManager = new Config.ConfigrationManager(initLogger);
            }
        }
        /// <summary>
        /// 全局初始化
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <typeparam name="TRequest"></typeparam>
        public void Init<TContext, TRequest>()
        {
            //防止重复调用
            if (!isInit)
            {
                isInit = true;
                //初始化插件
                //即通过插件配置信息,把程序集加载到内存中.
                initLogger.Info("加载并初始化插件程序集");
                plugManager = new Plugs.PlugManager(initLogger);
                //视图初始化
                //通过反射分析视图类,利用Emit动态初始化并调用视图类的函数,并通过字典缓存起来.
                //字典为:NFinal.ViewDelegate.viewFastDic
                initLogger.Info("从程序集中取出视图并进行视图初始化及缓存操作");
                ServiceCollection.SetViewRazor(initLogger, plugManager.GetAssemblys().ToArray(),false);
                ServiceCollection.templateConstructor = ServiceCollection.GetViewConstructor();
                //控制器初始化
                //通过反射分析控制器中的函数,利用Emit动态生成执行函数,并通过字典缓存起来.
                //字典为:Middleware.ActionFastDic
                initLogger.Info("从程序集中取出控制器并进行初始化及缓存操作");
                actionHelper = new Action.ActionHelper(initLogger);
                actionHelper.Init<TContext, TRequest>(globalConfig);
            }
        }
        /// <summary>
        /// 全局依赖注入容器
        /// </summary>
        //public static DependencyInjection.IServiceCollection serviceCollection = new DependencyInjection.ServiceCollection();
        /// <summary>
        /// 插件配置
        /// </summary>
        public static IDictionary<string, NFinal.Config.Plug.PlugConfig> plugConfigDictionary = null;
        /// <summary>
        /// 全局配置
        /// </summary>
        public static Config.Global.GlobalConfig globalConfig;
        /// <summary>
        /// 操作初始化帮助类
        /// </summary>
        private static NFinal.Action.ActionHelper actionHelper;
        /// <summary>
        /// 插件初始化管理类
        /// </summary>
        private static Plugs.PlugManager plugManager;
        /// <summary>
        /// nfinal.json及plug.json配置加载
        /// </summary>
        private static NFinal.Config.ConfigrationManager configrationManager;
    }
}
