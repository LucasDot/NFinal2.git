﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : Configration.cs
//        Description :NFinal2配置类
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace NFinal.Config
{
    /// <summary>
    /// 配置管理类
    /// </summary>
    public class ConfigrationManager:NFinal.DependencyInjection.ServiceCollectionHandler
    {
        private NFinal.Logs.ILogger logger;
        /// <summary>
        /// 配置管理类
        /// </summary>
        /// <param name="logger">初始化日志记录对象</param>
        public ConfigrationManager(NFinal.Logs.ILogger logger)
        {
            this.logger = logger;
            if (!isInit)
            {
                Init();
                isInit = true;
            }
        }
        /// <summary>
        /// 是否初始化
        /// </summary>
        public static bool isInit=false;
        /// <summary>
        /// 删除Json配置中的注释，以免解释失败
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string DeleteComment(string source)
        {
            System.Text.RegularExpressions.Regex commentRegex =
                            new System.Text.RegularExpressions.Regex("(^//[^\r\n]*|[^:]//[^\r\n]*)");
            source = commentRegex.Replace(source, string.Empty);
            return source;
        }
        /// <summary>
        /// 加载配置
        /// </summary>
        public void Init()
        {
            ConfigrationManager.isInit = true;
            //全局插件配置缓存
            Application.plugConfigDictionary = new Dictionary<string, NFinal.Config.Plug.PlugConfig>();
            string fileName = Directory.GetCurrentDirectory();
            //获取全局配置文件路径
            string nfinalConfigPath= NFinal.IO.Path.GetApplicationPath("/nfinal.json");
            if (File.Exists(nfinalConfigPath))
            {
                logger.Info("找到nfinal.json并开始加载:"+nfinalConfigPath);
                //打开全局配置
                using (StreamReader nfinalConfigReader = System.IO.File.OpenText(nfinalConfigPath))
                {
                    string nfinalJsonText = nfinalConfigReader.ReadToEnd();
                    nfinalConfigReader.Dispose();
                    //删除注释
                    nfinalJsonText = DeleteComment(nfinalJsonText);
                    //加载配置
                    try
                    {
                        Application.globalConfig = ServiceCollection.jsonSerialize.DeserializeObject<NFinal.Config.Global.GlobalConfig>(nfinalJsonText);
                        Application.globalConfig.JsonObject = SimpleJSON.JSON.Parse(nfinalJsonText).AsObject;
                    }
                    catch
                    {
                        throw new NFinal.Exceptions.NFinalConfigLoadException(nfinalJsonText);
                    }
                    NFinal.Config.Plug.PlugConfig plugConfig = null;
                    //获取插件目录
                    string nfinalPlugFolder = NFinal.IO.Path.GetApplicationPath("/Plugs/");
                    logger.Info("获取插件目录:"+nfinalPlugFolder);
                    //搜索插件配置文件
                    string[] plugJsonFileNameList = Directory.GetFiles(nfinalPlugFolder, "plug.json", SearchOption.AllDirectories);
                    //Configration.globalConfig.plugs = new Plug.Plug[plugJsonFileNameList.Length];
                    logger.Info("找到"+plugJsonFileNameList.Count()+"个插件");
                    //循环加载插件配置
                    for (int i = 0; i < plugJsonFileNameList.Length; i++)
                    {
                        string plugJsonFileName = plugJsonFileNameList[i];
                        if (File.Exists(plugJsonFileName))
                        {
                            logger.Info("加载插件配置文件:"+plugJsonFileName);
                            string plugJsonText = "";
                            using (StreamReader streamReader = System.IO.File.OpenText(plugJsonFileName))
                            {
                                plugJsonText = streamReader.ReadToEnd();
                            }
                            //删除注释
                            plugJsonText = DeleteComment(plugJsonText);
                            //加载插件配置
                            try
                            {
                                plugConfig = ServiceCollection.jsonSerialize.DeserializeObject<NFinal.Config.Plug.PlugConfig>(plugJsonText);
                                plugConfig.JsonObject = SimpleJSON.JSON.Parse(nfinalJsonText).AsObject;
                                logger.Info("插件名称为:"+plugConfig.plug.name);
                                logger.Info("插件程序集为:"+NFinal.IO.Path.GetApplicationPath(plugConfig.plug.assemblyPath));
                                //logger.Info("插件的静态文件夹为:"+NFinal.IO.Path.GetApplicationPath());
                                //把插件信息放入全局应用程序缓存中
                                Application.plugConfigDictionary.Add(plugConfig.plug.name, plugConfig);
                            }
                            //加载失败抛出异常
                            catch(Exception ex)
                            {
                                var plugConfigLoadException = new NFinal.Exceptions.PlugConfigLoadException(plugJsonFileName,plugJsonText);
                                logger.Error(plugConfigLoadException.Message);
                                throw plugConfigLoadException;
                            }
                        }
                        //找不到插件配置文件,抛出异常
                        else
                        {
                            
                            var fileNotFoundException= new FileNotFoundException("找不到NFinal插件配置文件："
                                + plugJsonFileName,
                                plugJsonFileName);
                            logger.Error(fileNotFoundException.Message);
                            throw fileNotFoundException;
                        }
                    }
                }
            }
            //找不到全局配置文件,抛出异常
            else
            {
                var fileNotFoundException = new FileNotFoundException("找不到NFinal全局配置文件："
                                    + nfinalConfigPath,
                                    nfinalConfigPath);
                logger.Error(fileNotFoundException.Message);
                throw fileNotFoundException;
            }
        }
    }
}
