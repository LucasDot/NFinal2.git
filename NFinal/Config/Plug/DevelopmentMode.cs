﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Config.Plug
{
    /// <summary>
    /// 开发模式
    /// </summary>
    public enum DevelopmentMode
    {
        /// <summary>
        /// 严格模式，必须添加完整的控制器注释，及方法注释
        /// 适合企业开发设置
        /// </summary>
        Strict=2,
        /// <summary>
        /// 可以不添加任何注释
        /// 适合个人开发设置
        /// </summary>
        Free=0,
        /// <summary>
        /// 一般限制模式，必须添加代码书写的作者
        /// 比较适合小团队设置
        /// </summary>
        Limited=1
    }
}
