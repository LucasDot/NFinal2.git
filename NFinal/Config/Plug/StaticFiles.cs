﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Config.Plug
{
    public class StaticFiles
    {
        public string fileProvider;
        public string requestPath;
        public MimeMap[] fileExtensionContentTypeProvider;
        public bool serveUnknownFileTypes;
        public string defaultContentType;
    }
    public class MimeMap
    {
        public string fileExtension;
        public string contentType;
    }
}
