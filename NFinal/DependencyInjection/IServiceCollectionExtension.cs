﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Cache.SimpleCache;

namespace NFinal.DependencyInjection
{
    /// <summary>
    /// 服务注册扩展类
    /// </summary>
    public static class IServiceCollectionExtension
    {
        /// <summary>
        /// 设置NFinal框架的默认服务
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void SetDefault(this IServiceCollection serviceCollection)
        {
            serviceCollection.SetCacheSimple(30);
            serviceCollection.SetSerializeProtobuf();
            serviceCollection.SetJsonNewtonsoft();
            serviceCollection.SetSession("userKey");
            serviceCollection.SetSessionSimple(30);
           
            serviceCollection.SetUrlHelper();
            //serviceCollection.SetService<NFinal.Cache.ICache<string>, NFinal.Serialize.ISerializable>(typeof(NFinal.Cache.SimpleCache.SimpleCache),false).Configure(30);
            //serviceCollection.SetService<NFinal.Serialize.ISerializable>(typeof(NFinal.Serialize.ProtobufSerialize.ProtobufSerialize),false);
            //serviceCollection.SetService<NFinal.Json.IJsonSerialize>(typeof(NFinal.Json.Newtonsoft.NewtonsoftJsonSerialize), false);
            //serviceCollection.SetService<NFinal.Logs.ILogger,string>(typeof(NFinal.Logs.NLogger.NLogger),false);
        }
        public static void SetUrlHelper(this IServiceCollection serviceCollection)
        {
            serviceCollection.SetService<NFinal.IUrlHelper>(typeof(NFinal.UrlHelper), false);
        }
        public static NFinal.Cache.ICache<string> GetCache(this IServiceCollection serviceCollection,NFinal.Serialize.ISerializable serializable)
        {
            return serviceCollection.GetService<NFinal.Cache.ICache<string>,
                        //ICache初始化时的参数类型
                        NFinal.Serialize.ISerializable>(serializable);
        }
        public static Func<string, object, NFinal.View.ITemplate> GetViewConstructor(this IServiceCollection serviceCollection)
        {
            return serviceCollection.GetServiceConstructor<NFinal.View.ITemplate, string, object, NFinal.View.ITemplate>();
        }
        public static NFinal.Logs.ILogger GetLogger(this IServiceCollection serviceCollection, string name)
        {
            return serviceCollection.GetService<NFinal.Logs.ILogger, string>(name);
        }
        public static NFinal.Serialize.ISerializable GetSerializable(this IServiceCollection serviceCollection)
        {
            return serviceCollection.GetService<NFinal.Serialize.ISerializable>();
        }
        public static NFinal.Json.IJsonSerialize GetJsonSerialize(this IServiceCollection serviceCollection)
        {
            return serviceCollection.GetService<NFinal.Json.IJsonSerialize>();
        }
    }
}
