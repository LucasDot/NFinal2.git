﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Exceptions
{
    /// <summary>
    /// 操作对应的URL不存在的异常
    /// </summary>
    public class UrlNotFoundException : System.Exception
    {
        /// <summary>
        /// 操作对应的URL不存在的异常
        /// </summary>
        /// <param name="controller">控制器类型</param>
        /// <param name="methodName">操作名,即Action函数名</param>
        /// <param name="parameters">参数</param>
        public UrlNotFoundException(Type controller,string methodName,params StringContainer[] parameters) : 
            base($"Url未能找到.请确认该操作是否存在!controller:{controller.FullName},method:{methodName},parameters:{parameters}")
        { }
    }
}
