﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Middleware.Static
{
    public class Request
    {
        public string requestPath;
        public string version;
        public string cache; 
    }
    public class Response
    {
        public CacheControl cacheControl;
        public int maxAge;
        public DateTime date;
        public DateTime expires;
        public DateTime lastModify;
        public System.IO.Stream body;
        public int statusCode;
    }
    public class Context
    {
        public Request request;
        public Response response;
    }
    public class Cache
    {
        public CacheControl cacheControl;
        public int maxAge;
        public DateTime date;
        public DateTime expires;
        public DateTime lastModify;
    }
    public enum CacheControl
    {
        NoCache,
        Private,
        Public
    }
}
