﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : Middleware.cs
//        Description :Http中间件泛型类
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using NFinal.Action;
using NFinal.Http;
using NFinal.DependencyInjection;

namespace NFinal.Middleware
{
    /// <summary>
    /// Http执行代理
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <param name="context"></param>
    /// <returns></returns>
    public delegate Task InvokeDelegate<TContext>(TContext context);
    /// <summary>
    /// Http中间件泛型接口
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TRequest"></typeparam>
    public interface IMiddleware<TContext,TRequest>
    {
        /// <summary>
        /// 处理Http上下文
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        Task Invoke(TContext context);
    }
    /// <summary>
    /// Http中间件泛型类
    /// </summary>
    /// <typeparam name="TContext">Context或Environment等</typeparam>
    /// <typeparam name="TRequest">Request或IOwinContext等</typeparam>
    public abstract class Middleware<TContext,TRequest>: NFinal.DependencyInjection.ServiceCollectionHandler,IMiddleware<TContext,TRequest>
    {
        /// <summary>
        /// 管道中下一个Http上下文处理函数
        /// </summary>
        public readonly InvokeDelegate<TContext> _next;
        /// <summary>
        /// 控制器行为字典
        /// </summary>
        public static NFinal.Collections.FastDictionary<string,ActionData<TContext,TRequest>> actionFastDic=null;
        /// <summary>
        /// 中间件初始化
        /// </summary>
        /// <param name="next">Http上下文处理函数</param>
        public Middleware(InvokeDelegate<TContext> next)
        {
            Application application = new Application();
            application.Init<TContext, TRequest>();
            this._next = next;
        }
        /// <summary>
        /// 获取请求Url
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract string GetRequestPath(TContext context);
        /// <summary>
        /// 获取默认Controller
        /// </summary>
        /// <param name="context">http上下文</param>
        /// <param name="plugConfig">插件配置</param>
        /// <returns></returns>
        public abstract IAction<TContext,TRequest> GetAction(TContext context,NFinal.Config.Plug.PlugConfig plugConfig);
        /// <summary>
        /// 获取请求信息
        /// </summary>
        /// <param name="context">http上下文</param>
        /// <returns></returns>
        public abstract TRequest GetRequest(TContext context);
        /// <summary>
        /// 获取请求参数
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public abstract NameValueCollection GetParameters(TRequest request);
        /// <summary>
        /// 获取请求方法
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract string GetRequestMethod(TContext context);
        /// <summary>
        /// 获取二级域名
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract string GetSubDomain(TContext context);
        /// <summary>
        /// 输出
        /// </summary>
        /// <param name="response"></param>
        public abstract Task WriteResponse(TContext context,NFinal.Owin.Response response);
        /// <summary>
        /// 获取SessionId
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract Http.HttpConnection GetHttpConnection(TContext context);
        /// <summary>
        /// 主入口
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(TContext context)
        {

            string requestPath = GetRequestPath(context);
            //路径为"/"时变为类"/Index.html"
            if (requestPath.Length==1)
            {
                requestPath = Application.globalConfig.server.indexDocument;
            }
            
            string actionKey;
            int shortActionKeyLength;
            //获取actionKey
            if (Application.globalConfig.debug.enable)
            {
                actionKey = NFinal.Url.ActionKey.GetActionKey(GetRequestMethod(context), requestPath,out shortActionKeyLength);
            }
            else
            {
                actionKey = NFinal.Url.ActionKey.GetActionKey(GetRequestMethod(context), requestPath,out shortActionKeyLength);
            }
            bool hasError = false;
            NFinal.Action.ActionData<TContext,TRequest> actionData;
            string actionKeyShort = actionKey.Substring(0,actionKey.Length);
            if (actionFastDic.TryGetValue(actionKeyShort, out actionData))
            {
                if (actionData.IConnectionFilters != null)
                {
                    HttpConnection httpConnection = GetHttpConnection(context);
                    if (!NFinal.Filter.FilterHelper.ConnectionFilter(actionData.IConnectionFilters,context, httpConnection))
                    {
                        await Constant.CompletedTask;
                    }
                }

                TRequest request = default(TRequest);
                try
                {
                    request = GetRequest(context);
                }
                catch
                {
                    hasError = true;
                    if (actionData.plugConfig.customErrors.mode ==Config.Plug.CustomErrorsMode.Off)
                    {
                        using (IAction<TContext, TRequest> controller = GetAction(context,actionData.plugConfig))
                        {
                            controller.Initialization(context,actionData, null, request, CompressMode.GZip, actionData.plugConfig);
                            controller.SetResponseHeader("Content-Type", "text/html; charset=utf-8");
                            //解析出错
                            controller.SetResponseStatusCode(200);
                            await controller.Close();
                        }
                    }
                }
                NameValueCollection parameters = GetParameters(request);
                //获取Url中的参数
                if (actionData.actionUrlData != null && !actionData.actionUrlData.hasNoParamsInUrl)
                {
                    if (actionData.actionUrlData.isSimpleUrl)
                    {
                        NFinal.Url.ActionUrlHelper.SimpleParse(requestPath, parameters, actionData.actionUrlData.actionUrlNames, shortActionKeyLength, actionData.actionUrlData.extensionLength);
                    }
                    else
                    {
                        NFinal.Url.ActionUrlHelper.RegexParse(requestPath, parameters, actionData.actionUrlData.actionUrlNames,actionData.actionUrlData.parameterRegex);
                    }
                }
                //生产环境下
                if (!Application.globalConfig.debug.enable)
                {
                    if (!NFinal.Filter.FilterHelper.ParamaterFilter(actionData.IParametersFilters,context, parameters))
                    {
                        await Constant.CompletedTask;
                    }
                    try
                    {
                        await actionData.actionExecute(context, actionData, request, parameters);
                    }
                    catch(Exception)
                    {
                        hasError = true;
                        if (actionData.plugConfig.customErrors.mode == Config.Plug.CustomErrorsMode.Off)
                        {
                            using (IAction<TContext, TRequest> controller = GetAction(context,actionData.plugConfig))
                            {
                                controller.Initialization(context,null, null, request, CompressMode.GZip, actionData.plugConfig);
                                controller.SetResponseHeader("Content-Type", "text/html; charset=utf-8");
                                //服务器错误
                                controller.SetResponseStatusCode(500);
                                await controller.Close();
                            }
                        }
                    }
                    if (hasError)
                    {
                        using (IAction<TContext, TRequest> controller = GetAction(context,actionData.plugConfig))
                        {
                            controller.Initialization(context,null, null, request, CompressMode.GZip,actionData.plugConfig);
                            controller.Redirect(actionData.plugConfig.customErrors.defaultRedirect);
                        }
                    }
                }
                //测试环境下
                else
                {
                    //actionData.actionExecute(context, a ctionData, request, parameters);
                    try
                    {
                        await actionData.actionExecute(context, actionData, request, parameters);
                    }

                    catch (System.Exception e)
                    //if(false)
                    {
                        #region 输出错误信息
                        using (
                        IAction<TContext, TRequest> controller = GetAction(context, actionData.plugConfig))
                        {
                            controller.SetResponseHeader("Content-Type", "text/html; charset=utf-8");
                            controller.SetResponseStatusCode(200);

                            controller.Write("错误消息：<br/>");
                            controller.Write(e.Message);
                            controller.Write("<br/>");
                            controller.Write("请求时发生错误：<br>");
                            controller.Write(GetRequestPath(context));
                            controller.Write("<br/>");
                            controller.Write("错误跟踪：</br>");
                            string[] stackTraces = e.StackTrace.Split('\n');
                            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"\s+at\s([\S\s]+)(\sin\s([\S\s]+):line\s([0-9]+))+?\s*");
                            System.Text.RegularExpressions.Regex regCh = new System.Text.RegularExpressions.Regex(@"\s+在\s([\S\s]+)(\s位置\s([\S\s]+):行号\s([0-9]+))+?\s*");
                            System.Text.RegularExpressions.Match mat;
                            string atfunctionName;
                            string infilePostion;
                            string fileName;
                            int lineNum;
                            string fileText;
                            int currentLineNum;
                            controller.Write("<html>");
                            controller.Write("<head></head>");
                            controller.Write("<body>");
                            controller.Write("<div id=\"traces\">");
                            for (int i = 0; i < stackTraces.Length; i++)
                            {
                                controller.Write("<dl>");
                                mat = reg.Match(stackTraces[i]);
                                if (!mat.Success)
                                {
                                    mat = regCh.Match(stackTraces[i]);
                                }
                                controller.Write("<dt style=\"background:grey;cursor:pointer;\">");
                                controller.Write(stackTraces[i]);
                                controller.Write("</dt>");
                                if (i == 0)
                                {
                                    controller.Write("<dd style=\"display: block;\">");
                                }
                                else
                                {
                                    controller.Write("<dd style=\"display: none;\">");
                                }
                                if (mat.Success)
                                {
                                    atfunctionName = mat.Groups[1].Value;
                                    //controller.Write(atfunctionName);

                                    if (mat.Groups[2].Success)
                                    {
                                        infilePostion = mat.Groups[2].Value;
                                        fileName = mat.Groups[3].Value;
                                        int.TryParse(mat.Groups[4].Value, out lineNum);
                                        controller.Write(fileName);
                                        controller.Write("<br/>");
                                        using (StreamReader reader = File.OpenText(fileName))
                                        {
                                            currentLineNum = 0;
                                            controller.Write("<ul style=\"list-style:none;\">");
                                            while (!reader.EndOfStream)
                                            {
                                                currentLineNum++;
                                                fileText = reader.ReadLine();
                                                if (currentLineNum == lineNum)
                                                {
                                                    controller.Write("<li style=\"background:red;min-width:500px;\">");
                                                }
                                                else
                                                {
                                                    controller.Write("<li>");
                                                }
                                                controller.Write(string.Format("{0:0000}", currentLineNum));
                                                controller.Write(":");
                                                if (currentLineNum == lineNum)
                                                {
                                                    controller.Write(fileText.Replace("<", "&lt")
                                                        .Replace(">", "&gt;")
                                                        .Replace("\t", "&nbsp&nbsp&nbsp&nbsp").Replace(" ", "&nbsp"));
                                                }
                                                else
                                                {
                                                    controller.Write(fileText.Replace("<", "&lt")
                                                        .Replace(">", "&gt;")
                                                        .Replace("\t", "&nbsp&nbsp&nbsp&nbsp").Replace(" ", "&nbsp"));
                                                }
                                                controller.Write("</li>");
                                                //controller.Write("<br/>");

                                            }
                                            controller.Write("</ul>");
                                        }
                                    }
                                    else
                                    {
                                        controller.Write(atfunctionName);
                                    }

                                }
                                controller.Write("</dd>");
                                controller.Write("</dl>");
                            }
                            controller.Write("</div>");
                            controller.Write("</body>");
                            controller.Write("</html>");
                            controller.Write(@"<script>
                                var tracesDiv = document.getElementById('traces');
                                var traceTitles = tracesDiv.getElementsByTagName('dt');
                                for (var i = 0; i < traceTitles.length; i++)
                                {
                                    traceTitles[i].addEventListener('click',function() {
                                        if (this.nextSibling.style.display == 'none')
                                        {
                                            this.nextSibling.style.display = 'block';
                                        }
                                        else
                                        {
                                            this.nextSibling.style.display = 'none';
                                        }
                                    });
                                }
                            </script>");
                            await controller.Close();
                        }
                        #endregion
                        await Constant.CompletedTask;
                    }
                }
                await Constant.CompletedTask;
            }
            else
            {
                //NFinal.Owin.Response response = null;
                //if (Config.Configration.staticFilesCache.TryGetValue(requestPath, out response))
                //{
                //    await WriteResponse(context, response);
                //}
                //else
                //{
                //    string FilePath = null;
                //    if (File.Exists(Path.Combine(FilePath, requestPath)))
                //    {
                //        Config.Configration.staticFilesCache.Add(requestPath, response);
                //        await WriteResponse(context, response);
                //    }
                //}
                await _next.Invoke(context);
            }
        }
        #region 参数解析
        /// <summary>
        /// Http参数解析函数
        /// </summary>
        /// <param name="requestMethod"></param>
        /// <param name="contentTypeString"></param>
        /// <param name="requestQueryString"></param>
        /// <param name="requestBody"></param>
        /// <returns></returns>
        public NameValueCollection GetParameters(string requestMethod, string contentTypeString, string requestQueryString, Stream requestBody)
        {
            //请求参数
            var parameters = new NFinal.NameValueCollection();
            MethodType methodType = MethodType.NONE;
            //获取POST方法
            switch (requestMethod)
            {
                case NFinal.Constant.MethodTypePOST: methodType = MethodType.POST; break;
                case NFinal.Constant.MethodTypeGET: methodType = MethodType.GET; break;
                case NFinal.Constant.MethodTypeDELETE: methodType = MethodType.DELETE; break;
                case NFinal.Constant.MethodTypePUT: methodType = MethodType.PUT; break;
                case NFinal.Constant.MethodTypeAJAX: methodType = MethodType.AJAX; break;
                default: methodType = MethodType.NONE; break;
            }
            //提取内容类型
            if (methodType == MethodType.POST)
            {
                ContentType contentType = ContentType.NONE;
                switch (contentTypeString.Split(NFinal.Constant.CharSemicolon)[0])
                {
                    case NFinal.Constant.ContentType_Multipart_form_data: contentType = ContentType.Multipart_form_data; break;
                    case NFinal.Constant.ContentType_Text_json:
                    case NFinal.Constant.ContentType_Application_json: contentType = ContentType.Application_json; break;
                    case NFinal.Constant.ContentType_Application_x_www_form_urlencoded: contentType = ContentType.Application_x_www_form_urlencoded; break;
                    case NFinal.Constant.ContentType_Application_xml:
                    case NFinal.Constant.ContentType_Text_xml: contentType = ContentType.Text_xml; break;
                    default: contentType = ContentType.NONE; break;
                }
                //提取form参数
                if (requestBody != System.IO.Stream.Null)
                {
                    if (contentType == ContentType.Application_x_www_form_urlencoded)
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(requestBody);
                        string body = sr.ReadToEnd();
                        sr.Dispose();
                        if (!string.IsNullOrEmpty(body))
                        {
                            string[] formArray = body.Split(NFinal.Constant.CharAnd, NFinal.Constant.CharEqual);
                            if (formArray.Length > 1 && (formArray.Length & 1) == 0)
                            {
                                for (int i = 0; i < formArray.Length; i += 2)
                                {
                                    parameters.Add(formArray[i], formArray[i + 1].UrlDecode());
                                }
                            }
                        }
                    }
                    //else if (contentType == NFinal.ContentType.Text_xml)
                    //{
                    //    System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                    //    string body = sr.ReadToEnd();
                    //    sr.Dispose();
                    //    if (!string.IsNullOrEmpty(body))
                    //    {
                    //        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    //        doc.LoadXml(body);
                    //        if (doc.DocumentElement != null)
                    //        {
                    //            foreach (System.Xml.XmlElement xmlNode in doc.DocumentElement.ChildNodes)
                    //            {
                    //                get.Add(xmlNode.Name, xmlNode.Value);
                    //            }
                    //        }
                    //    }
                    //}
                    else if (contentType == ContentType.Application_json)
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(requestBody);
                        string body = sr.ReadToEnd();
                        sr.Dispose();
                        NFinal.Json.IJsonSerialize serializable = ServiceCollection.jsonSerialize;
                        IDictionary<string, object> data = serializable.DeserializeObject<IDictionary<string, object>>(body);
                        foreach (var ele in data)
                        {
                            parameters.Add(ele.Key, ele.Value.ToString());
                        }
                    }
                    //else if (contentType == ContentType.Multipart_form_data)
                    //{
                    //    //multipart/form-data
                    //    string boundary = NFinal.Http.HttpMultipart.HttpMultipart.boundaryReg.Match(contentTypeString).Value;
                    //    var multipart = new NFinal.Http.HttpMultipart.HttpMultipart(requestBody, boundary);
                    //    files = new NFinal.Collections.FastDictionary<string, NFinal.Http.HttpMultipart.HttpFile>(StringComparer.Ordinal);
                    //    foreach (var httpMultipartBoundary in multipart.GetBoundaries())
                    //    {
                    //        if (string.IsNullOrEmpty(httpMultipartBoundary.Filename))
                    //        {
                    //            string name = httpMultipartBoundary.Name;
                    //            if (!string.IsNullOrEmpty(name))
                    //            {
                    //                string value = new System.IO.StreamReader(httpMultipartBoundary.Value).ReadToEnd();
                    //                parameters.Add(name, value);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            files.Add(httpMultipartBoundary.Name, new NFinal.Http.HttpMultipart.HttpFile(httpMultipartBoundary));
                    //        }
                    //    }
                    //}
                    //提取URL?后的参数
                    if (requestQueryString.Length > 0)
                    {
                        string[] queryArray = requestQueryString.Split(NFinal.Constant.CharAnd, NFinal.Constant.CharEqual);
                        if (queryArray.Length > 1 && (queryArray.Length & 1) == 0)
                        {
                            for (int i = 0; i < queryArray.Length; i += 2)
                            {
                                parameters.Add(queryArray[i], queryArray[i + 1].UrlDecode());
                            }
                        }
                    }
                    
                }
            }
            return parameters;
        }
#endregion
        /// <summary>
        /// 返回一个已经完成的任务
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="resultValue"></param>
        /// <returns></returns>
        public static Task<TResult> FromResult<TResult>(TResult resultValue)
        {
            var completionSource = new TaskCompletionSource<TResult>();
            completionSource.SetResult(resultValue);
            return completionSource.Task;
        }
    }
}
