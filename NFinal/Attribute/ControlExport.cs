﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 此控件将会被导出到数据库中
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ControlExportAttribute : System.Attribute
    {
        public string Name { get; set; }
        public ControlExportAttribute()
        {
           
        }
    }
}
