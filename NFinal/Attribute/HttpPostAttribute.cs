﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpPostAttribute:HttpMethodAttribute
    {
        public HttpPostAttribute() : base(Http.MethodType.POST)
        {

        }
        public HttpPostAttribute(string routeString) : base(Http.MethodType.POST, routeString)
        { }
    }
}
