﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Attribute
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Method)]
    public class BaseModleTypeAttribute : System.Attribute
    {
        string baseModelTypeFullName { get; set; }
        public BaseModleTypeAttribute(Type type)
        {
            this.baseModelTypeFullName = type.FullName;
        }
        public BaseModleTypeAttribute(string baseModelTypeFullName)
        {
            this.baseModelTypeFullName = baseModelTypeFullName;
        }
    }
}
