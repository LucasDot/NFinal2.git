﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace NFinal.Attribute
{
    /// <summary>
    /// 数据分发特性,指该数据会自动按照某类型进行分发,该类型必须是原类型的父类型或接口类型
    /// </summary>
    public class DistributeMemberAttribute : System.Attribute
    {
        public string Name { get; set; }
        public DistributeMemberAttribute()
        { }
        public DistributeMemberAttribute(string name)
        {
            this.Name = name;
        }
    }
}
