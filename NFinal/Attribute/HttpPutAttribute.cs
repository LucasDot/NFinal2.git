﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpPutAttribute : HttpMethodAttribute
    {
        public HttpPutAttribute() : base(Http.MethodType.PUT)
        { }
        public HttpPutAttribute(string routeString) : base(Http.MethodType.PUT, routeString)
        { }
    }
}
