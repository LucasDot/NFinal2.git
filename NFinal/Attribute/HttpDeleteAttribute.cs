﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpDeleteAttribute:HttpMethodAttribute
    {
        public HttpDeleteAttribute():base(Http.MethodType.DELETE)
        {
        }
        public HttpDeleteAttribute(string routeString) : base(Http.MethodType.DELETE, routeString)
        { }
    }
}
