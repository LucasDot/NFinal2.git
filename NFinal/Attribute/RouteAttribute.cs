﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RouteAttribute : System.Attribute
    {
        public string RouteString { get; set; }
        public RouteAttribute(string routeString)
        {
            this.RouteString = routeString;
        }
    }
}
