﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 描述
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class DescriptionAttribute : System.Attribute
    {
        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }
        public DescriptionAttribute(string content)
        {
            this.Content = content;
        }
    }
}
