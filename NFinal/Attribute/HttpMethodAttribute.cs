﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpMethodAttribute : System.Attribute
    {
        public Http.MethodType MethodType {get;set;}
        public string RouteString { get; set; }
        public HttpMethodAttribute(Http.MethodType methodType)
        {
            this.MethodType = methodType;
            this.RouteString = null;
        }
        public HttpMethodAttribute(Http.MethodType methodType, string routeString)
        {
            this.MethodType = methodType;
            this.RouteString = routeString;
        }
    }
}
