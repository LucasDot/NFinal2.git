﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HttpGetAttribute : HttpMethodAttribute
    {
        public HttpGetAttribute() : base(Http.MethodType.GET)
        {
        }
        public HttpGetAttribute(string routeString) : base(Http.MethodType.GET, routeString)
        { }
    }
}
