﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Attribute
{
    /// <summary>
    /// 用于标识此代码由谁所写,以及其所实现的功能是什么
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class AuthorAttribute:System.Attribute
    {
        /// <summary>
        /// 作者
        /// </summary>
        public string AuthorName{get;set;}
        public string description { get; set; }
        public AuthorAttribute(string authorName,string description)
        {

        }
    }
}
