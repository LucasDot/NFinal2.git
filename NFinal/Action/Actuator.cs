﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : Actuator.cs
//        Description :控制器执行类，用于组装执行控制器对应的行为的代理。
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;  
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using NFinal.Owin;
using System.Reflection;
using System.Reflection.Emit;
using NFinal.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace NFinal.Action
{
    //public delegate void RunActionDelegate<TContext>(TContext context,NFinal.Middleware.ActionData<TContext> actionData);
    /// <summary>
    /// 控制器行为执行类
    /// </summary>
    public class Actuator
    {
        /// <summary>
        /// 用户权限过滤器执行函数的反射信息
        /// </summary>
        public static MethodInfo AuthorizationFilterMethodInfo = typeof(NFinal.Filter.FilterHelper).GetMethod("AuthorizationFilter");//, new Type[] { typeof(NFinal.Filter.IEnvironmentFilter[]), typeof(IDictionary<string, object>) });
        /// <summary>
        /// 控制器行为执行之前过滤器执行函数的反射信息
        /// </summary>
        public static MethodInfo BeforeActionFilterMethodInfo = typeof(NFinal.Filter.FilterHelper).GetMethod("BeforeActionFilter");//, new Type[] { typeof(NFinal.Filter.IRequestFilter[]), typeof(IDictionary<string, object>), typeof(NFinal.Owin.Request) });
        /// <summary>
        /// 控制器行为执行之后过滤器执行函数的反射信息
        /// </summary>
        public static MethodInfo AfterActionFilterMethodInfo = typeof(NFinal.Filter.FilterHelper).GetMethod("AfterActionFilter");
        /// <summary>
        /// 控制器行为响应过滤器执行函数的反射信息
        /// </summary>
        public static MethodInfo ResponseFiltersMethodInfo = typeof(NFinal.Filter.FilterHelper).GetMethod("ResponseFilter");//, new Type[] { typeof(NFinal.Filter.IResponseFilter[]), typeof(NFinal.Owin.Response) });
        /// <summary>
        /// 内存流初始化函数的反射信息
        /// </summary>
        public static ConstructorInfo MemoryStreamConstructorInfo = typeof(System.IO.MemoryStream).GetConstructor(Type.EmptyTypes);
        /// <summary>
        /// NameValueCollection获取Value值的函数的反射信息
        /// </summary>
        public static MethodInfo nameValueCollectionGetItemMethodInfo = typeof(NFinal.NameValueCollection).GetMethod("get_Item", new Type[] { typeof(string) });
        /// <summary>
        /// 获取ModelHelper中GetModel的方法的反射信息
        /// </summary>
        public static MethodInfo modelHelperGetModelMethodInfo = typeof(NFinal.Model.ModelHelper).GetMethod("GetModel");
        /// <summary>
        /// 根据类型查找自动转换成该类型的函数的字典缓存对象
        /// </summary>
        public static Dictionary<RuntimeTypeHandle, System.Reflection.MethodInfo> StringContainerOpImplicitMethodInfoDic = null;
        /// <summary>
        /// 获取已经完成的Task
        /// </summary>
        public static MethodInfo GetCompletedTaskMethodInfo = typeof(NFinal.Constant).GetProperty("CompletedTask").GetGetMethod();
        /// <summary>
        /// 获取执行控制器行为的代理函数
        /// </summary>
        /// <typeparam name="TContext">Http上下文信息类型</typeparam>
        /// <typeparam name="TRequest">Http请求信息类型</typeparam>
        /// <param name="assembly">程序集</param>
        /// <param name="controllerType">控制器类型</param>
        /// <param name="actionMethodInfo">控制器行为对应方法的反射信息</param>
        /// <returns></returns>
        public static ActionExecute<TContext,TRequest> GetRunActionDelegate<TContext,TRequest>(Assembly assembly,Type controllerType,System.Reflection.MethodInfo actionMethodInfo,ActionData<TContext,TRequest> actionData)
        {
            var TContextType = typeof(TContext);
            if (StringContainerOpImplicitMethodInfoDic == null)
            {
                StringContainerOpImplicitMethodInfoDic = new Dictionary<RuntimeTypeHandle, System.Reflection.MethodInfo>();
                System.Reflection.MethodInfo[] methodInfos = typeof(StringContainer).GetMethods();
                foreach (var methodInfo in methodInfos)
                {
                    if (methodInfo.Name == "op_Implicit" && methodInfo.ReturnType != typeof(StringContainer))
                    {
                        StringContainerOpImplicitMethodInfoDic.Add(methodInfo.ReturnType.TypeHandle, methodInfo);
                    }
                }
            }
            DynamicMethod method = new DynamicMethod("RunActionX", typeof(Task), new Type[] { typeof(TContext),typeof(NFinal.Action.ActionData<TContext,TRequest>),typeof(TRequest),typeof(NameValueCollection)},true);
            ILGenerator methodIL = method.GetILGenerator();
            var methodEnd = methodIL.DefineLabel();
            var request= methodIL.DeclareLocal(typeof(TRequest));
            var controller = methodIL.DeclareLocal(controllerType);
            var actionResult = methodIL.DeclareLocal(typeof(NFinal.IActionResult));
            var taskActionResult = methodIL.DeclareLocal(actionMethodInfo.ReturnType);
            var task = methodIL.DeclareLocal(typeof(System.Threading.Tasks.Task));

            var defaultConstructor = controllerType.GetConstructor(Type.EmptyTypes);
            methodIL.Emit(OpCodes.Newobj, defaultConstructor);
            methodIL.Emit(OpCodes.Stloc, controller);

            //0.context,1.actionData,2.request,3.parameters
            //try
            if (!Application.globalConfig.debug.enable)
            {
                methodIL.BeginExceptionBlock();
            }
            {
                //controller
                methodIL.Emit(OpCodes.Ldloc, controller);
                //controller,environment
                methodIL.Emit(OpCodes.Ldarg_0);
                //controller,environment,"methodName"
                methodIL.Emit(OpCodes.Ldarg_1);
                //controller,environment,"methodName",null
                methodIL.Emit(OpCodes.Ldnull);
                //controller,environment,"methodName",null,Request
                methodIL.Emit(OpCodes.Ldarg_2);
                //controller,environment,"methodName",null,Request,CompressMode
                methodIL.Emit(OpCodes.Ldc_I4, (int)CompressMode.Deflate);
                //controller,environment,"methodName",null,Request,CompressMode,actionData
                methodIL.Emit(OpCodes.Ldarg_1);
                //controller,environment,"methodName",null,Request,CompressMode,actionData.plugConfig
                methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>).GetField("plugConfig"));
                methodIL.Emit(OpCodes.Callvirt, controllerType.GetMethod("Initialization",
                    new Type[] { typeof(TContext),
                        typeof(ActionData<TContext,TRequest>),
                        typeof(System.IO.Stream),
                        typeof(TRequest),
                        typeof(CompressMode),
                        typeof(NFinal.Config.Plug.PlugConfig)
                    }));
                #region 执行用户权限过滤器
                ////actionData
                //methodIL.Emit(OpCodes.Ldarg_1);
                ////actionData.IAuthorizationFilters//泛型的类型必须固定
                //methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext,TRequest>).GetField("IAuthorizationFilters"));
                ////controller
                //methodIL.Emit(OpCodes.Ldloc, controller);//泛型的类型必须固定
                //methodIL.Emit(OpCodes.Call, AuthorizationFilterMethodInfo.MakeGenericMethod(new Type[] { typeof(TContext),typeof(TRequest)}));
                //var BeforeAuthorizationEnd = methodIL.DefineLabel();
                //methodIL.Emit(OpCodes.Brtrue_S, BeforeAuthorizationEnd);
                ////++
                //methodIL.Emit(OpCodes.Call, GetCompletedTaskMethodInfo);
                //methodIL.Emit(OpCodes.Stloc_S,task);
                //methodIL.Emit(OpCodes.Leave, methodEnd);//离函数体较远，使用leave_s会报错。
                //methodIL.MarkLabel(BeforeAuthorizationEnd);
                #endregion

                #region 执行Action执行之前的过滤器
                ////actionData
                //methodIL.Emit(OpCodes.Ldarg_1);
                ////actionData.IBeforeActionFilters
                //methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>).GetField("IBeforeActionFilters"));
                ////controller
                //methodIL.Emit(OpCodes.Ldloc, controller);
                ////FilterHelper.BeforeActionFilter();
                //methodIL.Emit(OpCodes.Call, BeforeActionFilterMethodInfo.MakeGenericMethod(new Type[] { typeof(TContext), typeof(TRequest) }));
                //var BeforeActionEnd = methodIL.DefineLabel();
                //methodIL.Emit(OpCodes.Brtrue_S, BeforeActionEnd);
                ////++
                //methodIL.Emit(OpCodes.Call,GetCompletedTaskMethodInfo);
                //methodIL.Emit(OpCodes.Stloc_S,task);
                //methodIL.Emit(OpCodes.Leave, methodEnd);
                //methodIL.MarkLabel(BeforeActionEnd);
                #endregion

                //controller
                methodIL.Emit(OpCodes.Ldloc, controller);
                //controller.Before();
                methodIL.Emit(OpCodes.Callvirt, controllerType.GetMethod("Before", Type.EmptyTypes));
                //bool,0
                var BeforeEnd = methodIL.DefineLabel();
                methodIL.Emit(OpCodes.Brtrue_S, BeforeEnd);
                //++
                methodIL.Emit(OpCodes.Call,GetCompletedTaskMethodInfo);
                methodIL.Emit(OpCodes.Stloc,task);
                methodIL.Emit(OpCodes.Leave, methodEnd);
                methodIL.MarkLabel(BeforeEnd);

                //ViewBag初始化
                if (true)
                {
                    var ControllerViewBagFieldInfo = controllerType.GetField("ViewBag");
                    Type ViewBagType =Type.GetTypeFromHandle(actionData.viewBagType);
                    //if (ControllerViewBagFieldInfo.FieldType == typeof(object))
                    //{
                    //    string modelTypeName = controllerType.Namespace + "." + controllerType.Name + "_Model";
                    //    modelTypeName += "." + actionMethodInfo.Name;
                    //    ViewBagType = assembly.GetType(modelTypeName);
                    //    if (ViewBagType == null)
                    //    {
                    //        throw new NFinal.Exceptions.ModelNotFoundException(modelTypeName);
                    //    }
                    //}
                    //else
                    //{
                    //    ViewBagType = ControllerViewBagFieldInfo.FieldType;
                    //}
                    var ViewBagContructorInfo = ViewBagType.GetConstructor(Type.EmptyTypes);
                    var ViewBag = methodIL.DeclareLocal(ViewBagType);
                    bool viewBagTypeIsExpandoObject = false;
                    if (ViewBagType == typeof(System.Dynamic.ExpandoObject))
                    {
                        viewBagTypeIsExpandoObject = true;
                    }
                    //controller
                    methodIL.Emit(OpCodes.Newobj, ViewBagContructorInfo);
                    methodIL.Emit(OpCodes.Stloc, ViewBag);
                    List<FieldInfo> viewBagFiledInfoList = new List<FieldInfo>();
                    List<FieldInfo> controllerFieldInfoList = new List<FieldInfo>();
                    //获取所有字段
                    var controllerFieldInfos = controllerType.GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static|BindingFlags.FlattenHierarchy);
                    foreach (var controllerFieldInfo in controllerFieldInfos)
                    {
                        //查找Controller中具有ViewBagMember特性的字段
                        var viewBagMemberAttribute = controllerFieldInfo.GetCustomAttributes(typeof(ViewBagMemberAttribute), true);
                        if (viewBagMemberAttribute.Count() > 0)
                        {
                            if (viewBagTypeIsExpandoObject)
                            {
                                controllerFieldInfoList.Add(controllerFieldInfo);
                            }
                            else
                            {
                                var ViewBagFiledInfo = ViewBagType.GetField(
                                    controllerFieldInfo.Name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
                                //查找到ViewBag中具有相同名字的字段
                                if (ViewBagFiledInfo != null && ViewBagFiledInfo.FieldType == controllerFieldInfo.FieldType)
                                {
                                    viewBagFiledInfoList.Add(ViewBagFiledInfo);
                                    controllerFieldInfoList.Add(controllerFieldInfo);
                                }
                            }
                        }
                    }
                    
                    List<MethodInfo> controllerProperyInfoGetMethodList = new List<MethodInfo>();
                    List<MethodInfo> viewBagPropertyInfoSetMethodList = new List<MethodInfo>();
                    var controllerPropertyInfos = controllerType.GetProperties(BindingFlags.Public|BindingFlags.Instance|BindingFlags.Static| BindingFlags.FlattenHierarchy);
                    foreach (var controllerPropertyInfo in controllerPropertyInfos)
                    {
                        var viewBagMemberAttribute = controllerPropertyInfo.GetCustomAttributes(typeof(ViewBagMemberAttribute), true);
                        if (viewBagMemberAttribute.Count() > 0)
                        {
                            if (viewBagTypeIsExpandoObject)
                            {
                                controllerProperyInfoGetMethodList.Add(controllerPropertyInfo.GetGetMethod());
                            }
                            else
                            {
                                var viewBagPropertyInfo = ViewBagType.GetProperty(
                                    controllerPropertyInfo.Name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
                                if (viewBagPropertyInfo != null && viewBagPropertyInfo.PropertyType == controllerPropertyInfo.PropertyType)
                                {
                                    MethodInfo controllerPropertyInfoGetMethod = controllerPropertyInfo.GetGetMethod();
                                    MethodInfo viewBagPropertyInfoSetMethod = viewBagPropertyInfo.GetSetMethod();
                                    controllerProperyInfoGetMethodList.Add(controllerPropertyInfoGetMethod);
                                    viewBagPropertyInfoSetMethodList.Add(viewBagPropertyInfoSetMethod);
                                }
                            }
                        }
                    }
                    Type viewBagOperateStaticType = null;
                    if (viewBagTypeIsExpandoObject)
                    {
                    }
                    for (int i = 0; i < viewBagFiledInfoList.Count; i++)
                    {
                        FieldInfo controllerFieldInfo = controllerFieldInfoList[i];
                        FieldInfo viewBagFiledInfo = viewBagFiledInfoList[i];
                        if (controllerFieldInfo.IsStatic)
                        {
                            methodIL.Emit(OpCodes.Ldloc, ViewBag);
                            methodIL.Emit(OpCodes.Ldsfld, controllerFieldInfo);
                            methodIL.Emit(OpCodes.Stfld, viewBagFiledInfo);
                        }
                        else
                        {
                            //赋值操作
                            methodIL.Emit(OpCodes.Ldloc, ViewBag);
                            methodIL.Emit(OpCodes.Ldloc, controller);
                            methodIL.Emit(OpCodes.Ldfld, controllerFieldInfo);
                            methodIL.Emit(OpCodes.Stfld, viewBagFiledInfo);
                        }
                    }
                    for (int i = 0; i < viewBagPropertyInfoSetMethodList.Count; i++)
                    {
                        MethodInfo controllerPropertyInfoGetMethod = controllerProperyInfoGetMethodList[i];
                        MethodInfo viewBagPropertyInfoSetMethod = viewBagPropertyInfoSetMethodList[i];
                        if (controllerPropertyInfoGetMethod.IsStatic)
                        {
                            methodIL.Emit(OpCodes.Ldloc, ViewBag);
                            methodIL.Emit(OpCodes.Call, controllerPropertyInfoGetMethod);
                            methodIL.Emit(OpCodes.Callvirt, viewBagPropertyInfoSetMethod);
                        }
                        else
                        {
                            methodIL.Emit(OpCodes.Ldloc, ViewBag);
                            methodIL.Emit(OpCodes.Ldloc, controller);
                            methodIL.Emit(OpCodes.Callvirt, controllerPropertyInfoGetMethod);
                            methodIL.Emit(OpCodes.Callvirt, viewBagPropertyInfoSetMethod);
                        }
                    }
                    methodIL.Emit(OpCodes.Ldloc, controller);
                    methodIL.Emit(OpCodes.Ldloc, ViewBag);
                    methodIL.Emit(OpCodes.Stfld, ControllerViewBagFieldInfo);
                }

                //controller
                methodIL.Emit(OpCodes.Ldloc, controller);
                System.Reflection.ParameterInfo[] parameterInfos = actionMethodInfo.GetParameters();
                //添加参数
                if (parameterInfos.Length > 0)
                {
                    for (int i = 0; i < parameterInfos.Length; i++)
                    {
                        Type parameterType = parameterInfos[i].ParameterType;
                        if (parameterInfos[i].ParameterType
#if (NET40 || NET451 || NET461)
                            .IsGenericType
#endif
#if NETCORE
                            .GetTypeInfo().IsGenericType
#endif
                            && parameterInfos[i].ParameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            parameterType = parameterInfos[i].ParameterType.GetGenericArguments()[0];
                        }
                        if (parameterType == typeof(System.String) ||
                            parameterType == typeof(System.Int32) ||
                            parameterType == typeof(System.Int16) ||
                            parameterType == typeof(System.Int64) ||
                            parameterType == typeof(System.UInt32) ||
                            parameterType == typeof(System.UInt16) ||
                            parameterType == typeof(System.UInt64) ||
                            parameterType == typeof(System.Byte) ||
                            parameterType == typeof(System.SByte) ||
                            parameterType == typeof(System.Single) ||
                            parameterType == typeof(System.Double) ||
                            parameterType == typeof(System.Decimal) ||
                            parameterType == typeof(System.DateTime) ||
                            parameterType == typeof(System.DateTimeOffset) ||
                            parameterType == typeof(System.Char) ||
                            parameterType == typeof(System.Boolean) ||
                            parameterType == typeof(System.Guid))
                        {
                            //parameters
                            methodIL.Emit(OpCodes.Ldarg_3);
                            //parameters,name
                            methodIL.Emit(OpCodes.Ldstr, parameterInfos[i].Name);
                            //parameters[name]
                            methodIL.Emit(OpCodes.Callvirt, nameValueCollectionGetItemMethodInfo);
                            //ParameterType op_Implicit(request.parameters[name]);隐式转换
                            methodIL.Emit(OpCodes.Call, StringContainerOpImplicitMethodInfoDic[parameterInfos[i].ParameterType.TypeHandle]);
                        }
                        else
                        {
                            //new ParameterModel();
                            //methodIL.Emit(OpCodes.Newobj, parameterInfos[i].ParameterType.GetConstructor(Type.EmptyTypes));
                            //parameters
                            methodIL.Emit(OpCodes.Ldarg_3);
                            //ModelHelper.GetModel(new ParameterModel(),parameters);
                            methodIL.Emit(OpCodes.Call, modelHelperGetModelMethodInfo.MakeGenericMethod(parameterType));
                        }
                    }
                }
                //controller.Action(par1,par2,par3.....);
                methodIL.Emit(OpCodes.Callvirt, actionMethodInfo);
#if NETCORE
                if(actionMethodInfo.ReturnType.IsConstructedGenericType)
#else
                if (actionMethodInfo.ReturnType.IsGenericType)
#endif
                {
                    methodIL.Emit(OpCodes.Stloc, taskActionResult);
                }
                if (typeof(NFinal.IActionResult).IsAssignableFrom(actionMethodInfo.ReturnType))
                {
                    methodIL.Emit(OpCodes.Stloc,actionResult);
                }
                ////controller
                //methodIL.Emit(OpCodes.Ldloc, controller);
                ////controller.After();
                //methodIL.Emit(OpCodes.Callvirt, controllerType.GetMethod("After", Type.EmptyTypes));

#region 执行Action执行之后的过滤器
                ////actionData
                //methodIL.Emit(OpCodes.Ldarg_1);
                ////actionData.IBeforeActionFilters
                //methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>).GetField("IAfterActionFilters"));
                ////controller
                //methodIL.Emit(OpCodes.Ldloc, controller);
                ////FilterHelper.BeforeActionFilter();
                //methodIL.Emit(OpCodes.Call, AfterActionFilterMethodInfo.MakeGenericMethod(new Type[] { typeof(TContext), typeof(TRequest) }));
                //var AfterActionEnd = methodIL.DefineLabel();
                //methodIL.Emit(OpCodes.Brtrue_S, AfterActionEnd);
                ////++
                //methodIL.Emit(OpCodes.Call, GetCompletedTaskMethodInfo);
                //methodIL.Emit(OpCodes.Stloc_S,task);
                //methodIL.Emit(OpCodes.Leave, methodEnd);
                //methodIL.MarkLabel(AfterActionEnd);
#endregion
#region 执行Response过滤器
                ////actionData
                //methodIL.Emit(OpCodes.Ldarg_1);
                ////actionData.IResponseFilters
                //methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext,TRequest>).GetField("IResponseFilters"));
                ////actionData.IResponseFilters,controller
                //methodIL.Emit(OpCodes.Ldloc, controller);
                ////actionData.IResponseFilters,controller.response
                //methodIL.Emit(OpCodes.Callvirt, controllerType.GetProperty("response").GetGetMethod());
                ////FilterHelper.Filter(actionData.IResponseFilters,controller.response)
                //methodIL.Emit(OpCodes.Call, ResponseFiltersMethodInfo);
                //var ifResponseFiltersEnd = methodIL.DefineLabel();
                //methodIL.Emit(OpCodes.Brtrue_S, ifResponseFiltersEnd);
                ////++
                //methodIL.Emit(OpCodes.Call,GetCompletedTaskMethodInfo);
                //methodIL.Emit(OpCodes.Stloc_S,task);
                //methodIL.Emit(OpCodes.Leave, methodEnd);
                //methodIL.MarkLabel(ifResponseFiltersEnd);
#endregion
                var tryEnd = methodIL.DefineLabel();
                if (
#if NETCORE
                    actionMethodInfo.ReturnType.IsConstructedGenericType
#else
                    actionMethodInfo.ReturnType.IsGenericType
#endif
                    )
                {
                    //controller
                    methodIL.Emit(OpCodes.Ldloc, controller);
                    //taskActionResult
                    methodIL.Emit(OpCodes.Ldloc, taskActionResult);
                    //actionData
                    methodIL.Emit(OpCodes.Ldarg_1);
                    //actionData.IAfterFilters,controller
                    methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>)
                        .GetField(nameof(actionData.IAfterActionFilters)));
                    //actionData
                    methodIL.Emit(OpCodes.Ldarg_1);
                    //actionData.IBeforeRenderFilters
                    methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>)
                        .GetField(nameof(actionData.IBeforeRenderFilters)));
                    //actionData
                    methodIL.Emit(OpCodes.Ldarg_1);
                    //actionData.IAfterRenderFilters
                    methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>)
                        .GetField(nameof(actionData.IAfterRenderFilters)));
                    //Task<TActionResult>
                    var actionResultType = actionMethodInfo.ReturnType.GetGenericArguments()[0];
                    //controller.FinishActionResultTask
                    methodIL.Emit(OpCodes.Callvirt, typeof(NFinal.Action.AbstractAction<TContext, TRequest>)
                        .GetMethod("FinishActionResultTask", BindingFlags.Instance|BindingFlags.NonPublic).MakeGenericMethod(actionResultType));
                    //task=
                    methodIL.Emit(OpCodes.Stloc, task);
                    //go to end
                    //methodIL.Emit(OpCodes.Leave,methodEnd);
                }
                if(typeof(NFinal.IActionResult).IsAssignableFrom(actionMethodInfo.ReturnType))
                {
                    //controller
                    methodIL.Emit(OpCodes.Ldloc, controller);
                    //actionResult
                    methodIL.Emit(OpCodes.Ldloc, actionResult);
                    //actionData
                    methodIL.Emit(OpCodes.Ldarg_1);
                    //actionData.IAfterFilters
                    methodIL.Emit(OpCodes.Ldfld,typeof(NFinal.Action.ActionData<TContext,TRequest>).GetField("IAfterActionFilters"));
                    //actionData
                    methodIL.Emit(OpCodes.Ldarg_1);
                    //actionData.IBeforeRenderFilters
                    methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>).GetField("IBeforeRenderFilters"));
                    //actionData
                    methodIL.Emit(OpCodes.Ldarg_1);
                    //actionData.IAfterRenderFilters
                    methodIL.Emit(OpCodes.Ldfld, typeof(NFinal.Action.ActionData<TContext, TRequest>).GetField("IAfterRenderFilters"));
                    //controller.FinishActionResult()
                    methodIL.Emit(OpCodes.Callvirt, typeof(NFinal.Action.AbstractAction<TContext, TRequest>).GetMethod("FinishActionResult", BindingFlags.Instance | BindingFlags.NonPublic));
                    //task=
                    methodIL.Emit(OpCodes.Stloc,task);
                    //go to end
                    //methodIL.Emit(OpCodes.Leave,methodEnd);
  
                } 
                methodIL.MarkLabel(tryEnd);
                //跳出异常
                methodIL.Emit(OpCodes.Leave, methodEnd);
            }
            if (!Application.globalConfig.debug.enable)
            {
                methodIL.BeginCatchBlock(typeof(System.Exception));
                {
                    methodIL.Emit(OpCodes.Throw);
                }
            }
            if (!Application.globalConfig.debug.enable)
            {
                //finally
                methodIL.BeginFinallyBlock();
                {
                    var finallyEnd = methodIL.DefineLabel();
                    //controller
                    methodIL.Emit(OpCodes.Ldloc, controller);
                    //controller==null
                    methodIL.Emit(OpCodes.Brfalse_S, finallyEnd);
                    //controller
                    methodIL.Emit(OpCodes.Ldloc, controller);
                    //controller.Release();
                    methodIL.Emit(OpCodes.Callvirt, controllerType.GetMethod("Release", Type.EmptyTypes));
                    //controller
                    methodIL.Emit(OpCodes.Ldloc, controller);
                    //controller.Dispose();
                    methodIL.Emit(OpCodes.Callvirt, controllerType.GetMethod("Dispose", Type.EmptyTypes));
                    //methodIL.Emit(OpCodes.Callvirt,typeof(System.IDisposable).GetMethod("Dispose",Type.EmptyTypes));
                    methodIL.MarkLabel(finallyEnd);
                }
                //end
                methodIL.EndExceptionBlock();
            }
            methodIL.MarkLabel(methodEnd);
            methodIL.Emit(OpCodes.Ldloc,task);
            methodIL.Emit(OpCodes.Ret);
            ActionExecute<TContext,TRequest> getRunActionDelegate =(ActionExecute<TContext,TRequest>)method.CreateDelegate(typeof(ActionExecute<TContext,TRequest>));
            return getRunActionDelegate;
        }
    }
#if EMITDEBUG
    public class ViewBag
    {
        public string a;
        public string b { get; set; }
        public string c { get; set; }
    }
    public class ViewBagDynamic : BaseController
    {
        public string a;
        public string b { get; set; }
        public static string c { get; set; }
        public void Show()
        {
        }
    }
    public class ViewBagDynamicInit
    {
        public void Run()
        {
            ViewBagDynamic controller = new ViewBagDynamic();
            ViewBag viewBag = new ViewBag();
            viewBag.a = controller.a;
            viewBag.b = controller.b;
            viewBag.c = ViewBagDynamic.c;
            controller.ViewBag = viewBag;
        }
    }
    public class RunAction:Index
    {
        public static Task Run1<TContext, TRequest>(TContext context, NFinal.Action.ActionData<IDictionary<string,object>, Request> actionData, TRequest request, NameValueCollection parameters)
        {
            using (Index controller = new Index())
            {
                Request request1 = null;
                IDictionary<string, object> context1 = null;
                //初始化
                try
                {
                    controller.Initialization(context1, actionData, null, request1, CompressMode.None, new Config.Plug.PlugConfig());
                    ViewModel ViewBag = new ViewModel();
                    ViewBag.a = 2;
                    ViewBag.b = "3";

                    ViewBag.c = Index.c;
                    ViewBag.c1 = Index.c1;
                    controller.ViewBag = ViewBag;
                    if (!controller.Before())
                    {
                        return Application.CompletedTask;
                    }
                    Task task = controller.StartTask(actionData.IBeforeActionFilters);
                    if (task != null)
                    {
                        return task;
                    }
                    bool b = true;

                    //添参数问题。。。
                    //string a = request.parameters["a"];
                    //int b = request.parameters["b"];
                    //int? b1 = request.parameters["b1"];
                    //ParameterModel c = new ParameterModel();
                    //c = NFinal.Model.ModelHelper.GetModel(new ParameterModel(), request.parameters);
                    NFinal.IActionResult actoinResult = controller.Index1(parameters["a"], parameters["b"], NFinal.Model.ModelHelper.GetModel<ParameterModel>(parameters));

                    var resultTask = controller.Index2(parameters["a"]);
                    if (b)
                    {
                        return controller.FinishActionResult(actoinResult, actionData.IAfterActionFilters,
                            actionData.IBeforeRenderFilters, actionData.IAfterRenderFilters);
                    }
                    else
                    {
                        return controller.FinishActionResultTask(resultTask, actionData.IAfterActionFilters,
                            actionData.IBeforeRenderFilters, actionData.IAfterRenderFilters);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    controller.Release();
                    controller.Dispose();
                }
            }
        }
        public void Run2()
        {

        }
    }
    public class User
    {
        public int userId;
        public string userName;
    }
    public class BaseController : OwinAction
    {
        [ViewBagMember]
        public string imageUrl;
        public override bool Before()
        {
            return base.Before();
        }
        public override void After()
        {
            base.After();
        }
    }
    [ResFilter]
    public class Index : BaseController
    {
        public static string c;
        public static string c1;
        [Route("/Index.html")]
        public ActionResult Index1(string a,int b,ParameterModel c)
        {
            this.Write("hello!");
            return View();
        }
        [Route("/Index2.php")]
        public async Task<ActionResult> Index2(string a)
        {
            this.ViewBag.a = 1;
            return Json();
        }
        public void Index3(int a)
        {
            this.Redirect("sss");
        }
        public void Index4(ParameterModel c)
        {

        }
    }
    public class Index_Index1Action : BaseController
    {
        
        public void Index1()
        {
            this.ViewBag.b = "2";
           
        }
    }
    public class Index_Index2Action : BaseController
    {
        public void Index2()
        {
            this.ViewBag.a = 1;
        }
    }
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Method)]
    public class ResFilter : NFinal.Filter.AfterActionFilterAttribute
    {
        public override bool ActionFilter<TContext, TRequest>(AbstractAction<TContext, TRequest> action)
        {
            throw new NotImplementedException();
        }
    }
    public class ParameterModel
    {
        public int a;
        public string b;
    }
    public class ViewModel
    {
        public string c;
        public string c1 { get; set; }
        public int a;
        public string b;
    }
#endif
}
