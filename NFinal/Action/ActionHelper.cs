﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :ActionHelper.cs
//        Description :控制器行为帮助类，用于分析及执行控制器行为
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using NFinal.Http;
using System.Threading.Tasks;

/// <summary>
/// 控制器Action执行代理
/// </summary>
/// <typeparam name="TContext">Http上下文类型</typeparam>
/// <typeparam name="TResquest">Http请求信息类型</typeparam>
/// <param name="context">Http上下文</param>
/// <param name="actionData">控制器执行所需信息</param>
/// <param name="request">Http请求信息</param>
/// <param name="parameters">Http请求信息，KeyValue集合类型</param>
public delegate Task ActionExecute<TContext, TResquest>(TContext context,NFinal.Action.ActionData<TContext,TResquest> actionData,TResquest request,NFinal.NameValueCollection parameters);
namespace NFinal.Action
{
    /// <summary>
    /// 控制器中所有行为的URL格式化信息
    /// </summary>
    public class ControllerData
    {
        /// <summary>
        /// URl格式化信息
        /// </summary>
        public Dictionary<RuntimeTypeHandle, Dictionary<string,string>> formatList;
    }
    /// <summary>
    /// 控制器行为的相关信息
    /// </summary>
    /// <typeparam name="TContext">Http上下文类型</typeparam>
    /// <typeparam name="TRequest">Http请求类型</typeparam>
    public class ActionData<TContext,TRequest>
    {
        /// <summary>
        /// 默认视图Url
        /// </summary>
        public string viewUrl;
        /// <summary>
        /// Assembly
        /// </summary>
        public string assemblyName;
        /// <summary>
        /// 控制器全称
        /// </summary>
        public string controllerFullName;
        /// <summary>
        /// 请求URL
        /// </summary>
        public string actionUrl;
        /// <summary>
        /// 视图数据类型
        /// </summary>
        public RuntimeTypeHandle viewBagType;
        /// <summary>
        /// 视图的基础类型
        /// </summary>
        public string viewBagBaseType;
        /// <summary>
        /// 控制器行为执行代理
        /// </summary>
        public ActionExecute<TContext,TRequest> actionExecute;
        /// <summary>
        /// 插件配置信息
        /// </summary>
        public NFinal.Config.Plug.PlugConfig plugConfig;
        /// <summary>
        /// 当前行为对应方法的特性提供者
        /// </summary>
        public System.Reflection.ICustomAttributeProvider methodProvider;
        /// <summary>
        /// 网络连接接口数组,在获取Request参数之前执行
        /// </summary>
        public NFinal.Filter.IConnectionFilter[] IConnectionFilters;
        /// <summary>
        /// 参数验证接口数组,在获取Request参数之后,Action执行之前
        /// </summary>
        public NFinal.Filter.IParameterFilter[] IParametersFilters;
        /// <summary>
        /// 在控制器行为之前过滤接口数组
        /// </summary>
        public NFinal.Filter.IBeforeActionFilter[] IBeforeActionFilters;
        /// <summary>
        /// 在控制器行为之后过滤接口数组
        /// </summary>
        public NFinal.Filter.IAfterActionFilter[] IAfterActionFilters;
        /// <summary>
        /// 渲染前函数
        /// </summary>
        public NFinal.Filter.IBeforeRenderFilter[] IBeforeRenderFilters;
        /// <summary>
        /// 渲染后函数
        /// </summary>
        public NFinal.Filter.IAfterRenderFilter[] IAfterRenderFilters;
        /// <summary>
        /// 控制器行为对应方法的URL特性中的Url字符串
        /// </summary>
        public string routeString;
        /// <summary>
        /// 控制器对应的类名
        /// </summary>
        public string className;
        /// <summary>
        /// 控制器行为对应的方法名
        /// </summary>
        public string methodName;
        /// <summary>
        /// 控制器名称
        /// </summary>
        public string controllerName;
        /// <summary>
        /// 控制器行为名称
        /// </summary>
        public string actionName;
        /// <summary>
        /// 控制器区域名称
        /// </summary>
        public string areaName;
        ///// <summary>
        ///// 可接受的请求方法数组
        ///// </summary>
        //public string[] method;
        /// <summary>
        /// 可接受的请求方法
        /// </summary>
        public MethodType acceptMethods;
        ///// <summary>
        ///// Http响应类型
        ///// </summary>
        //public string contentType;
        ///// <summary>
        ///// Http压缩模式
        ///// </summary>
        //public CompressMode compressMode;
        /// <summary>
        /// 控制器行为对应的请求URl相关信息
        /// </summary>
        public NFinal.Url.ActionUrlData actionUrlData;
    }
    /// <summary>
    /// 控制器行为帮助类，用于分析及执行控制器行为
    /// </summary>
    public class ActionHelper
    {
        //public static Dictionary<string, ActionExecute> actionDic = new Dictionary<string, ActionExecute>();
        //public static NFinal.Collections.FastDictionary<ActionData> actionFastDic;
        private NFinal.Logs.ILogger logger;
        public ActionHelper(NFinal.Logs.ILogger logger)
        {
            this.logger = logger;
        }
        /// <summary>
        /// 控制器行为是否初始化
        /// </summary>
        public static bool isInit = false;
        /// <summary>
        /// 控制器行为初始化函数
        /// </summary>
        /// <typeparam name="TContext">Htpp上下文类型</typeparam>
        /// <typeparam name="TRequest">Http请求信息类型</typeparam>
        /// <param name="globalConfig">全局配置数据</param>
        public void Init<TContext, TRequest>(NFinal.Config.Global.GlobalConfig globalConfig)
        {
            if (isInit)
            {
                return;
            }
            else
            {
                isInit = true;
            }
            Module[] modules = null;
            Type[] types = null;

            NFinal.Collections.FastDictionary<string, ActionData<TContext, TRequest>> actionDataDictionary = new NFinal.Collections.FastDictionary<string, ActionData<TContext, TRequest>>();
            //List<KeyValuePair<string, ActionData<TContext, TRequest>>> actionDataList = new List<KeyValuePair<string, ActionData<TContext, TRequest>>>();
            NFinal.Collections.FastDictionary<RuntimeTypeHandle, Dictionary<string, NFinal.Url.FormatData>> formatControllerDictionary = new NFinal.Collections.FastDictionary<RuntimeTypeHandle, Dictionary<string, NFinal.Url.FormatData>>();
            Type controller = null;
            for (int i = 0; i < NFinal.Plugs.PlugManager.plugInfoList.Count; i++)
            {
                
                /////////////////////////////////////////////////////////////////////
                //
                //Assembly.Load
                //精确加载
                //
                //
                //Assembly.LoadFrom 
                //加载dll的引入
                //
                //
                //Assembly.LoadFile
                //仅加载自己
                //
                /////////////////////////////////////////////////////////////////////
                NFinal.Plugs.PlugInfo plugInfo = NFinal.Plugs.PlugManager.plugInfoList[i];
                if (!plugInfo.loadSuccess || plugInfo.enable == false)
                {
                    continue;
                }
                Assembly assembly = plugInfo.assembly;
                modules = assembly.GetModules();
                for (int j = 0; j < modules.Length; j++)
                {
                    try
                    {
                        types = modules[j].GetTypes();
                    }
                    catch (System.Reflection.ReflectionTypeLoadException loadException)
                    {
                        logger.Fatal("dll加载错误!");
                        foreach (var loaderException in loadException.LoaderExceptions)
                        {
                            logger.Fatal(loaderException.Message);
                            logger.Fatal(loaderException.Source);
                            logger.Fatal(loaderException.StackTrace);
                        }
                        throw loadException;
                    }
                    for (int k = 0; k < types.Length; k++)
                    {
                        controller = types[k];
                        //控制器必须以Controller结尾
                        if ((!controller.FullName.EndsWith("Controller", StringComparison.Ordinal))
                            )
                        {
                            continue;
                        }
                        //该类型继承自IAction并且其泛不是dynamic类型
#if (NET40 || NET451 || NET461)
                        if (typeof(NFinal.Action.IAction<TContext, TRequest>).IsAssignableFrom(controller))
                        {
                            if (!controller.IsGenericType)
#endif
#if NETCORE
                        if (typeof(NFinal.Action.IAction<TContext, TRequest>).IsAssignableFrom(controller))
                        {
                            if (!controller.GetTypeInfo().IsGenericType)
#endif
                            {
                                var configureMethodInfo = controller.GetMethod("Configure", BindingFlags.Static | BindingFlags.Public);
                                if (configureMethodInfo != null)
                                {
                                    var parameters = configureMethodInfo.GetParameters();
                                    if (parameters.Length == 1 && parameters[0].ParameterType == typeof(NFinal.Config.Plug.PlugConfig))
                                    {
                                        try
                                        {
                                            configureMethodInfo.Invoke(null, new object[] { plugInfo.config });
                                        }
                                        catch (Exception ex)
                                        {
                                            string message = "控制器" + controller.FullName + "下的" + configureMethodInfo.Name + "函数执行出错";
                                            logger.Fatal(message);
                                            logger.Fatal(ex.Message);
                                            logger.Fatal(ex.Source);
                                            logger.Fatal(ex.StackTrace);
                                            throw new Exception(message, ex);
                                        }
                                    }
                                }
                                Dictionary<string, NFinal.Url.FormatData> formatMethodDic = new Dictionary<string, NFinal.Url.FormatData>();
                                AddActionData(actionDataDictionary, formatMethodDic, assembly, controller, globalConfig, plugInfo);
                                formatControllerDictionary.Add(controller.TypeHandle, formatMethodDic);
                            }
                        }

                    }
                }
            }
            //如果未找到任何一个控制器类型，则抛出异常
            if (formatControllerDictionary.Count < 1)
            {
                var hasNoControllerInProjectException= new NFinal.Exceptions.HasNoControllerInProjectException();
                logger.Fatal(hasNoControllerInProjectException.Message);
                throw hasNoControllerInProjectException;
            }
            NFinal.Url.ActionUrlHelper.formatControllerDictionary = formatControllerDictionary;
            if (globalConfig.debug.enable)
            {
                NFinal.Url.ActionUrlHelper.GetUrlRouteJsContent(globalConfig);
                NFinal.Url.ActionUrlHelper.GenerateActionDebugHtml(globalConfig);
            }

            //}
            //添加图标响应
            //Icon.Favicon.Init(actionDataList);
            Middleware.Middleware<TContext, TRequest>.actionFastDic = new Collections.FastDictionary<string,ActionData<TContext, TRequest>>(actionDataDictionary);
            actionDataDictionary.Clear();
        }
        /// <summary>
        /// 向全局添加控制器行为信息
        /// </summary>
        /// <typeparam name="TContext">Http请求上下文类型</typeparam>
        /// <typeparam name="TRequest">Http请求类型</typeparam>
        /// <param name="actionDataDictionary">控制器行为数据信息字典</param>
        /// <param name="formatMethodDictionary">控制器行为URl格式化字典</param>
        /// <param name="assembly">当前程序集</param>
        /// <param name="controller">控制器类型信息</param>
        /// <param name="globalConfig">全局配置信息</param>
        /// <param name="plugInfo">插件信息</param>
        public static void AddActionData<TContext, TRequest>(NFinal.Collections.FastDictionary<string, ActionData<TContext, TRequest>> actionDataDictionary,
            Dictionary<string, NFinal.Url.FormatData> formatMethodDictionary,
            Assembly assembly, Type controller,
            NFinal.Config.Global.GlobalConfig globalConfig,
            NFinal.Plugs.PlugInfo plugInfo)
        {
            Type viewBagType = null;
            viewBagType = controller.GetField("ViewBag").FieldType;
            if (viewBagType == typeof(object))
            {
                MethodInfo[] actions = null;
                string controllerName;
                string areaName;

                ActionData<TContext, TRequest> actionData;
                GetControllerUrl(out controllerName, out areaName, controller, globalConfig);
                actions = controller.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
#if NETCORE
                //查找函数
                var controllerAttributeList = controller.GetTypeInfo().GetCustomAttributes(typeof(NFinal.ActionExportAttribute), true);
#else
                var controllerAttributeList = controller.GetCustomAttributes(typeof(NFinal.ActionExportAttribute), true);
#endif
                MethodInfo action = null;
                bool hasAction = false;
                ActionExportAttribute actionExportAttribute = null;
                foreach (var controllerAttribute in controllerAttributeList)
                {
                    actionExportAttribute = (NFinal.ActionExportAttribute)controllerAttribute;
                    if (actionExportAttribute.types == null)
                    {
                        action = controller.GetMethod(actionExportAttribute.methodName);
                    }
                    else
                    {
                        action = controller.GetMethod(actionExportAttribute.methodName, actionExportAttribute.types);
                    }
                    if (action == null)
                    {
                        throw new NFinal.Exceptions.NotFoundExportActionException(controller.FullName, actionExportAttribute.methodName);
                    }
                    hasAction = false;
                    foreach (var actionTmp in actions)
                    {
                        if (actionTmp == action)
                        {
                            hasAction = true;
                        }
                    }
                    if (!hasAction)
                    {
                        actionData = new ActionData<TContext, TRequest>();
                        actionData.viewBagType = actionExportAttribute.viewBagType.TypeHandle;
                        AddActionDataForMethod(actionDataDictionary, formatMethodDictionary,
                            assembly, controller,
                            actionData, action,
                            controllerName, areaName,
                            globalConfig, plugInfo);
                    }
                }

                //MethodInfo actionExportList =
                for (int m = 0; m < actions.Length; m++)
                {
#if !NETCORE
                    if (actions[m].ReturnType.IsGenericType
#else
                    if(actions[m].ReturnType.IsConstructedGenericType
#endif
                        )
                    {
                        if (actions[m].ReturnType.GetGenericTypeDefinition() == typeof(System.Threading.Tasks.Task<>))
                        {
                            Type[] genericArguments = actions[m].ReturnType.GetGenericArguments();
                            if (genericArguments.Length == 1)
                            {
                                if (!typeof(NFinal.IActionResult).IsAssignableFrom(genericArguments[0]))
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (actions[m].IsAbstract || actions[m].IsVirtual || actions[m].IsGenericMethod || actions[m].IsPrivate
                        || (!typeof(NFinal.IActionResult).IsAssignableFrom(actions[m].ReturnType))
                        || actions[m].IsStatic || actions[m].IsConstructor)
                        {
                            continue;
                        }
                    }
                    actionData = new ActionData<TContext, TRequest>();
                    string modelTypeName = controller.Namespace + "." + controller.Name + "_Model";
                    modelTypeName += "." + actions[m].Name;
                    viewBagType = assembly.GetType(modelTypeName);
                    if (viewBagType == null)
                    {
                        throw new NFinal.Exceptions.ModelNotFoundException(modelTypeName);
                    }
                    else
                    {
                        actionData.viewBagType = viewBagType.TypeHandle;
                    }
                    AddActionDataForMethod(actionDataDictionary, formatMethodDictionary,
                        assembly, controller,
                        actionData, actions[m],
                        controllerName, areaName,
                        globalConfig, plugInfo);
                }
            }
        }
        public static void AddActionDataForMethod<TContext, TRequest>(NFinal.Collections.FastDictionary<string, ActionData<TContext, TRequest>> actionDataDictionary,
            Dictionary<string, NFinal.Url.FormatData> formatMethodDictionary,
            Assembly assembly, Type controller,
            ActionData<TContext, TRequest> actionData,
            MethodInfo methodInfo, string controllerName, string areaName,
            NFinal.Config.Global.GlobalConfig globalConfig,
            NFinal.Plugs.PlugInfo plugInfo)
        {
#if !NETCORE
            if (methodInfo.ReturnType.IsGenericType
#else
                if(methodInfo.ReturnType.IsConstructedGenericType
#endif
                )
            {
                if (methodInfo.ReturnType.GetGenericTypeDefinition() == typeof(System.Threading.Tasks.Task<>))
                {
                    Type[] genericArguments = methodInfo.ReturnType.GetGenericArguments();
                    if (genericArguments.Length == 1)
                    {
                        if (!typeof(NFinal.IActionResult).IsAssignableFrom(genericArguments[0]))
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                if (methodInfo.IsAbstract || methodInfo.IsVirtual || methodInfo.IsGenericMethod || methodInfo.IsPrivate
                    || (!typeof(NFinal.IActionResult).IsAssignableFrom(methodInfo.ReturnType))
                    || methodInfo.IsStatic || methodInfo.IsConstructor)
                {
                    return;
                }
            }
            string actionName;
            string actionUrl;
            string[] actionKeys;
            string[] method;
            RouteAttribute routeAttribute;
            MethodType acceptMethods;
            NFinal.Url.ActionUrlData actionUrlData;
            actionKeys = GetActionKeys(controllerName, areaName, out actionUrl, out actionName,
                out routeAttribute,out acceptMethods, out actionUrlData,
                methodInfo, globalConfig, plugInfo);
            
            if (routeAttribute == null)
            {
                actionData.routeString = null;
            }
            else
            {
                actionData.routeString = routeAttribute.RouteString;
            }
            actionData.assemblyName = new AssemblyName(assembly.FullName).Name;
            actionData.acceptMethods = acceptMethods;
            actionData.className = controller.Name;
            actionData.controllerFullName = controller.FullName;
            actionData.methodName = methodInfo.Name;
            actionData.actionUrlData = actionUrlData;
            actionData.controllerName = controllerName;
            actionData.areaName = areaName;
            actionData.actionUrl = actionUrl;
            actionData.actionName = actionName;
            actionData.plugConfig = plugInfo.config;
            if (actionData.actionUrlData != null)
            {
                formatMethodDictionary.Add(methodInfo.Name, new NFinal.Url.FormatData(actionData.actionUrlData.formatUrl, actionData.actionUrlData.actionUrlNames));
            }
            else
            {
                formatMethodDictionary.Add(methodInfo.Name, new NFinal.Url.FormatData(actionData.actionUrl, null));
            }
            actionData.IConnectionFilters = GetFilters<NFinal.Filter.IConnectionFilter>(
                typeof(NFinal.Filter.IConnectionFilter), controller, methodInfo);
            actionData.IParametersFilters = GetFilters<NFinal.Filter.IParameterFilter>(
                typeof(NFinal.Filter.IParameterFilter), controller, methodInfo);
            actionData.IBeforeActionFilters = GetFilters<NFinal.Filter.IBeforeActionFilter>(
                typeof(NFinal.Filter.IBeforeActionFilter), controller, methodInfo
                );
            actionData.IAfterActionFilters = GetFilters<NFinal.Filter.IAfterActionFilter>(
                typeof(NFinal.Filter.IAfterActionFilter), controller, methodInfo
                );
            actionData.IBeforeRenderFilters = GetFilters<NFinal.Filter.IBeforeRenderFilter>(
                typeof(NFinal.Filter.IBeforeRenderFilter), controller, methodInfo);
            actionData.IAfterRenderFilters = GetFilters<NFinal.Filter.IAfterRenderFilter>(
                typeof(NFinal.Filter.IAfterRenderFilter), controller, methodInfo);
            actionData.actionExecute = NFinal.Action.Actuator.GetRunActionDelegate<TContext, TRequest>(assembly, controller, methodInfo,actionData);
            foreach (var actionKey in actionKeys)
            {
                if (actionDataDictionary.ContainsKey(actionKey))
                {
                    var oldActionData = actionDataDictionary[actionKey];
                    throw new NFinal.Exceptions.DuplicateActionUrlException(oldActionData.className, oldActionData.methodName, actionData.className, actionData.methodName);
                }
                else
                {
                    actionDataDictionary.Add(actionKey, actionData);
                }
            }
        }
        /// <summary>
        /// 获取控制器行为的URL信息
        /// </summary>
        /// <param name="controllerName">控制器名称</param>
        /// <param name="areaName">控制器区域名称</param>
        /// <param name="controller">控制器类型信息</param>
        /// <param name="globalConfig">全局配置信息</param>
        public static void GetControllerUrl(out string controllerName, out string areaName, Type controller, NFinal.Config.Global.GlobalConfig globalConfig)
        {
            ControllerAttribute[] controllerAttributes = null;
            AreaAttribute[] areaAttributes = null;
            controllerAttributes = (ControllerAttribute[])
#if (NET40 || NET451 || NET461)
                controller
#endif
#if NETCORE
                controller.GetTypeInfo()
#endif
                .GetCustomAttributes(typeof(ControllerAttribute), true);
            if (controllerAttributes.Length > 0)
            {
                if (!string.IsNullOrEmpty(controllerAttributes[0].Name))
                {
                    controllerName = controllerAttributes[0].Name;
                }
                else
                {
                    controllerName =NFinal.Url.ActionUrlHelper.GetControllerName(controller);
                }
            }
            else
            {
                controllerName = NFinal.Url.ActionUrlHelper.GetControllerName(controller);
            }
            areaAttributes = (AreaAttribute[])
#if (NET40 || NET451 || NET461)
                controller
#endif
#if NETCORE
                controller.GetTypeInfo()
#endif
                .GetCustomAttributes(typeof(AreaAttribute), true);
            if (areaAttributes.Length > 0)
            {
                areaName = areaAttributes[0].Name;
            }
            else
            {
                areaName = null;
            }
        }
        /// <summary>
        /// 组装并返回控制器对应行为的查找关键字
        /// </summary>
        /// <param name="controllerName">控制器名称</param>
        /// <param name="areaName">区域名称</param>
        /// <param name="actionUrl">请求URL</param>
        /// <param name="actionName">控制器行为名称</param>
        /// <param name="routeAttribute">路由参数</param>
        /// <param name="methodType">可接受的方法</param>
        /// <param name="actionUrlData"></param>
        /// <param name="methodInfo"></param>
        /// <param name="globalConfig"></param>
        /// <param name="plugInfo"></param>
        /// <returns></returns>
        public static string[] GetActionKeys(string controllerName, string areaName,
            out string actionUrl, out string actionName,
            out RouteAttribute routeAttribute,out MethodType methodType,out NFinal.Url.ActionUrlData actionUrlData,
            MethodInfo methodInfo, NFinal.Config.Global.GlobalConfig globalConfig,
            NFinal.Plugs.PlugInfo plugInfo)
        {
            string urlPrefix = null;
            if (plugInfo.urlPrefix?.Length > 0)
            {
                urlPrefix = plugInfo.urlPrefix;
            }
            List<string> actionKeys = new List<string>();
            actionUrlData = null;
            routeAttribute = null;
            HttpMethodAttribute httpMethodsAttribute = null;
            actionUrl = urlPrefix;
            actionName = null;
            string routeString = null;
            
            if (!string.IsNullOrEmpty(areaName))
            {
                actionUrl += "/" + areaName;
            }
            if (!string.IsNullOrEmpty(controllerName))
            {
                actionUrl += "/" + controllerName;
            }
            var attributes = methodInfo.GetCustomAttributes(true);
            bool hasActionAttribute = false;
            foreach (var attr in attributes)
            {
                var attrType = attr
#if (NET40 || NET451 || NET461)
                    .GetType();
#endif
#if NETCORE
                    .GetType().GetTypeInfo();
#endif
                //获取自定义路由特性
                if (
                    attrType.IsSubclassOf(typeof(RouteAttribute)))
                {
                    routeAttribute = (RouteAttribute)attr;
                    if (!string.IsNullOrEmpty(routeAttribute.RouteString))
                    {
                        routeString = routeAttribute.RouteString;
                        break;
                    }
                }
                //获取HttpMethodAttribute
                else if (attrType.IsSubclassOf(typeof(HttpMethodAttribute)))
                {
                    httpMethodsAttribute = (HttpMethodAttribute)attr;
                    if (!string.IsNullOrEmpty(httpMethodsAttribute.RouteString))
                    {
                        routeString = httpMethodsAttribute.RouteString;
                    }
                }
                else if (attrType.IsSubclassOf(typeof(ActionAttribute)))
                {
                    hasActionAttribute = true;
                    actionName = ((ActionAttribute)attr).Name;
                }
            }
            if (routeString!=null)
            {
                if (routeString?[0] == '~')
                {
                    actionUrlData = NFinal.Url.ActionUrlHelper.GetActionUrlData(plugInfo.config.url.prefix
                    + routeString.TrimStart('~'));
                }
                else
                {
                    actionUrlData = NFinal.Url.ActionUrlHelper.GetActionUrlData(plugInfo.config.url.prefix
                    + actionUrl + routeString);
                }
                actionUrl = actionUrlData.actionKey;
            }
            else
            {
                if (hasActionAttribute)
                {
                    actionUrl += "/" + actionName + plugInfo.config.url.extension;
                }
                else
                {
                    actionUrl += "/" + methodInfo.Name + plugInfo.config.url.extension;
                }
            }
            if (actionUrl!=null &&  actionUrl.Length== 1)
            {
                actionUrl = plugInfo.config.url.defaultDocument;
            }
            if (httpMethodsAttribute == null)
            {
                methodType = MethodType.NONE;
                foreach (var methodName in plugInfo.config.verbs)
                {
                    if (methodName == "GET")
                    {
                        methodType = methodType | MethodType.GET;
                    }
                    else if (methodName == "POST")
                    {
                        methodType = methodType | MethodType.POST;
                    }
                    else if (methodName == "AJAX")
                    {
                        methodType = methodType | MethodType.AJAX;
                    }
                    else if (methodName == "DELETE")
                    {
                        methodType = methodType | MethodType.DELETE;
                    }
                    else if (methodName == "PUT")
                    {
                        methodType = methodType | MethodType.PUT;
                    }
                }
            }
            else
            {
                methodType = httpMethodsAttribute.MethodType;
            }
            if ((methodType & MethodType.GET) != 0)
            {
                actionKeys.Add("GET:" + actionUrl);
            }
            if ((methodType & MethodType.POST) != 0)
            {
                actionKeys.Add("POST:" + actionUrl);
            }
            if ((methodType & MethodType.AJAX) != 0)
            {
                actionKeys.Add("AJAX:" + actionUrl);
            }
            if ((methodType & MethodType.DELETE) != 0)
            {
                actionKeys.Add("DELETE:" + actionUrl);
            }
            if ((methodType & MethodType.PUT) != 0)
            {
                actionKeys.Add("PUT:" + actionUrl);
            }

            return actionKeys.ToArray();
        }
        /// <summary>
        /// 获取控制器行为中的过滤器
        /// </summary>
        /// <typeparam name="TArrayType">过滤器类型</typeparam>
        /// <param name="filterType">过滤器类型信息</param>
        /// <param name="controller">控制器类型</param>
        /// <param name="action">控制器行为类型</param>
        /// <returns></returns>
        public static TArrayType[] GetFilters<TArrayType>(Type filterType, Type controller, MethodInfo action)
        {
            List<TArrayType> filterList = new List<TArrayType>();
            var controllerFilters =
#if (NET40 || NET451 || NET461)
                controller
#endif
#if NETCORE
                controller.GetTypeInfo()   
#endif
                .GetCustomAttributes(true);
            foreach(var controllerFilter in controllerFilters)
            {
                if (filterType.IsAssignableFrom(controllerFilter.GetType()))
                {
                    filterList.Add((TArrayType)(object)controllerFilter);
                }
            }
            var actionFilters =action.GetCustomAttributes(true);
            foreach (var actionFilter in actionFilters)
            {
                if (filterType.IsAssignableFrom(actionFilter.GetType()))
                {
                    filterList.Add((TArrayType)(object)actionFilter);
                }
            }
            if (filterList.Count > 0)
            {
                return filterList.ToArray();
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 组装并返回控制器行为执行代理
        /// </summary>
        /// <typeparam name="TContext">Http上下文类型</typeparam>
        /// <typeparam name="TRequest">Http请求类型</typeparam>
        /// <param name="assmeblyName">程序集名称</param>
        /// <param name="methodInfo">控制器行为对应的方法反射信息</param>
        /// <returns></returns>
        public static ActionExecute<TContext,TRequest> GetActionExecute<TContext, TRequest>(string assmeblyName,MethodInfo methodInfo)
        {
            DynamicMethod method = new DynamicMethod(assmeblyName+methodInfo.DeclaringType.FullName, null, new Type[] { typeof(TContext) });
            ILGenerator methodIL = method.GetILGenerator();
            methodIL.Emit(OpCodes.Nop);
            methodIL.Emit(OpCodes.Ldarg_0);
            methodIL.Emit(OpCodes.Call, methodInfo);
            methodIL.Emit(OpCodes.Ret);
            ActionExecute<TContext,TRequest> methodDelegate = (ActionExecute<TContext,TRequest>)method.CreateDelegate(typeof(ActionExecute<TContext,TRequest>));
            return methodDelegate;
        }
    }
}
