﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal;

namespace NFinal
{
    /// <summary>
    /// Action扩展类，用于Url生成
    /// </summary>
    public static class AbstractActionExtension
    {
        /// <summary>
        /// URl
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <typeparam name="TRequest"></typeparam>
        /// <param name="action"></param>
        /// <param name="methodName"></param>
        /// <param name="urlParameters"></param>
        /// <returns></returns>
        public static string Url<TContext, TRequest>(this NFinal.Action.AbstractAction<TContext, TRequest> action,string methodName, params NFinal.StringContainer[] urlParameters)
        {
            return NFinal.Url.ActionUrlHelper.Format(NFinal.Url.ActionUrlHelper.formatControllerDictionary[action.GetType().TypeHandle][methodName].formatUrl, urlParameters);
        }
    }
}
