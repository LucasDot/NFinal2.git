﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :ContextAction.cs
//        Description :IOwinContext对应的控制器基类，未实现
//
//        created by Lucas at  2015-6-30
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
#if (NET40 || NET451 || NET461)
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Owin;
using NFinal.Owin;
using NFinal.Http;
using System.Data;
using System.Threading.Tasks;

namespace NFinal
{
    /// <summary>
    /// IOwinContext
    /// </summary>
    public class ContextAction : NFinal.Action.AbstractAction<IOwinContext, IOwinRequest>
    {
        /// <summary>
        /// 控制器行为执行后函数
        /// </summary>
        public override void After()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="context"></param>
        /// <param name="methodName"></param>
        /// <param name="plugConfig"></param>
        public override void BaseInitialization(IOwinContext context,Action.ActionData<IOwinContext,IOwinRequest> actionData, NFinal.Config.Plug.PlugConfig plugConfig)
        {
        }
        /// <summary>
        /// 控制器行为执行前函数
        /// </summary>
        /// <returns></returns>
        public override bool Before()
        {
            return true;
        }

        public override Task Close()
        {
             return this.response.stream.CopyToAsync(this.outputStream);
        }
        public override void Release()
        {
            throw new NotImplementedException();
        }
        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        public override string GetRemoteIpAddress()
        {
            return this.request.RemoteIpAddress;
        }

        public override Stream GetRequestBody()
        {
            return this.request.Body;
        }

        public override string GetRequestHeader(string key)
        {
            return this.request.Headers[key];
        }
        public override string GetRequestMethod()
        {
            return this.request.Method;
        }
        public override string GetRequestPath()
        {
            return this.request.Path.Value;
        }

        public override string GetSubDomain(IOwinContext context)
        {
            return this.request.Host.Value.Split('.')[0];
        }

        public override void Initialization(IOwinContext context,Action.ActionData<IOwinContext,IOwinRequest> actionData, Stream outputStream, IOwinRequest request, CompressMode compressMode, NFinal.Config.Plug.PlugConfig plugConfig)
        {
            base.Initialization(context, actionData, outputStream, request, compressMode, plugConfig);
            this.parameters = new NFinal.NameValueCollection();
            var requestOwin = context.Request.Environment.GetRequest();
            this.parameters = requestOwin.parameters;
            IDictionary<string, string> requestCookie = new Dictionary<string, string>();
            foreach (var cookie in this.request.Cookies)
            {
                requestCookie.Add(cookie.Key, cookie.Value);
            }

            this.Cookie = new Cookie(requestCookie);
            this.Session = GetSession(Cookie.SessionId);
            if (outputStream == null)
            {
                this.outputStream = context.Response.Body;
            }
            else
            {
                this.outputStream = outputStream;
            }
        }

        public override void SetResponseHeader(string key, string[] value)
        {
            throw new NotImplementedException();
        }

        public override void SetResponseHeader(string key, string value)
        {
            throw new NotImplementedException();
        }

        public override void SetResponseStatusCode(int statusCode)
        {
            throw new NotImplementedException();
        }

        public override void Write(string value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}
#endif