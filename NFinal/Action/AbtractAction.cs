﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :AbtractAction.cs
//        Description :控制器抽象类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.IO;
using NFinal.Owin;
using System.Data;
using NFinal.Http;
//using Dapper;
using System.Threading.Tasks;

namespace NFinal.Action
{
    /// <summary>
    /// NFinal的控制器基类，核心类，类似于.net mvc中的BaseController类或者是asp.net中的Page类
    /// </summary>
    /// <typeparam name="TContext">上下文IOwinContext,Enviroment,Context</typeparam>
    /// <typeparam name="TRequest">请求信息</typeparam>
    public abstract class AbstractAction<TContext, TRequest> : NFinal.IO.Writer, IAction<TContext, TRequest>
    {
        #region Json专用
        /// <summary>
        /// 返回信息
        /// return true时,接口会返回以下信息
        /// {"Success":true,"Code":0,"Message":""}
        /// 其中的Message值就是在这里设置的
        /// </summary>
        public string Message;
        /// <summary>
        /// 错误编码
        /// return true时,接口会返回以下信息
        /// {"Success":true,"Code":0,"Message":""}
        /// 其中的Code值就是在这里设置的
        /// </summary>
        public int Code;
        #endregion
        public IActionResult Result { get; set; }
        #region 执行异步专用函数
        /// <summary>
        /// 系统使用,由于是泛型类，无法在非子类中执行。
        /// </summary>
        /// <param name="beforeActionFilters"></param>
        /// <returns></returns>
        internal Task StartTask(NFinal.Filter.IBeforeActionFilter[] beforeActionFilters)
        {
            if (beforeActionFilters != null)
            {
                for (int i = 0; i < beforeActionFilters.Length; i++)
                {
                    if (!beforeActionFilters[i].ActionFilter(this))
                    {
                        return beforeActionFilters[i].ActionResult.ExecuteResultAsync(this);
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// 系统使用,由于是泛型类，无法在非子类中执行。
        /// </summary>
        /// <returns></returns>
        internal async Task FinishTask()
        {
            await this.Close();
        }
        /// <summary>
        /// 系统使用,由于是泛型类，无法在非子类中执行。
        /// </summary>
        /// <param name="actionResult"></param>
        /// <returns></returns>
        internal async Task FinishTask(IActionResult actionResult)
        {
            if (actionResult != null)
            {
                await actionResult.ExecuteResultAsync(this);
                await this.FinishTask();
            }
            else
            {
                await this.FinishTask();
            }
        }
        /// <summary>
        /// 系统使用,由于是泛型类，无法在非子类中执行。
        /// </summary>
        /// <typeparam name="TActionResult"></typeparam>
        /// <param name="actionResultTask"></param>
        /// <param name="afterActionFilters"></param>
        /// <param name="beforeRenderFilters"></param>
        /// <param name="afterRenderFilters"></param>
        /// <returns></returns>
        internal async Task FinishActionResultTask<TActionResult>(Task<TActionResult> actionResultTask,
            NFinal.Filter.IAfterActionFilter[] afterActionFilters,
            NFinal.Filter.IBeforeRenderFilter[] beforeRenderFilters, NFinal.Filter.IAfterRenderFilter[] afterRenderFilters
            ) where TActionResult : IActionResult
        {
            if (afterActionFilters != null)
            {
                for (int i = 0; i < afterActionFilters.Length; i++)
                {
                    if (!afterActionFilters[i].ActionFilter(this))
                    {
                        //接着再处理
                        await afterActionFilters[i].ActionResult.ExecuteResultAsync(this);
                        break;
                    }
                }
            }
            if (beforeRenderFilters != null)
            {
                for (int i = 0; i < beforeRenderFilters.Length; i++)
                {
                    if (!beforeRenderFilters[i].RenderFilter(this))
                    {
                        //接着再处理
                        await beforeRenderFilters[i].ActionResult.ExecuteResultAsync(this);
                        break;
                    }
                }
            }
            //处理一次
            var actionResult = await actionResultTask;
            if (actionResult != null)
            {
                await actionResult.ExecuteResultAsync(this);
                this.After();
                if (afterRenderFilters != null)
                {
                    for (int i = 0; i < afterRenderFilters.Length; i++)
                    {
                        if (!afterRenderFilters[i].RenderFilter(this))
                        {
                            //接着再处理
                            await afterRenderFilters[i].ActionResult.ExecuteResultAsync(this);
                            break;
                        }
                    }
                }
                //流复制
                await this.FinishTask();
            }
            else
            {
                await FinishTask();
            }
        }
        /// <summary>
        /// 系统使用,由于是泛型类，无法在非子类中执行。
        /// </summary>
        /// <param name="actionResult"></param>
        /// <param name="afterActionFilters"></param>
        /// <param name="beforeRenderFilters"></param>
        /// <param name="afterRenderFilters"></param>
        /// <returns></returns>
        internal async Task FinishActionResult(IActionResult actionResult, NFinal.Filter.IAfterActionFilter[] afterActionFilters,
            NFinal.Filter.IBeforeRenderFilter[] beforeRenderFilters, NFinal.Filter.IAfterRenderFilter[] afterRenderFilters)
        {

            if (afterActionFilters != null)
            {
                for (int i = 0; i < afterActionFilters.Length; i++)
                {
                    if (!afterActionFilters[i].ActionFilter(this))
                    {
                        //接着再处理
                        await afterActionFilters[i].ActionResult.ExecuteResultAsync(this);
                        break;
                    }
                }
            }
            if (beforeRenderFilters != null)
            {
                for (int i = 0; i < beforeRenderFilters.Length; i++)
                {
                    if (!beforeRenderFilters[i].RenderFilter(this))
                    {
                        //接着再处理
                        await beforeRenderFilters[i].ActionResult.ExecuteResultAsync(this);
                        break;
                    }
                }
            }
            //处理一次
            if (actionResult != null)
            {
                await actionResult.ExecuteResultAsync(this);
                this.After();
                if (afterRenderFilters != null)
                {
                    for (int i = 0; i < afterRenderFilters.Length; i++)
                    {
                        if (!afterRenderFilters[i].RenderFilter(this))
                        {
                            //接着再处理
                            await afterRenderFilters[i].ActionResult.ExecuteResultAsync(this);
                            break;
                        }
                    }
                }
                //流复制
                await this.FinishTask();
            }
            else
            {
                await FinishTask();
            }
        }
        #endregion
        /// <summary>
        /// 布局页路径
        /// </summary>
        public string Layout { get; set; }
        public ActionData<TContext, TRequest> actionData { get; set; }
        /// <summary>
        /// 获取Session对象
        /// </summary>
        /// <param name="sessionId">存储在Cookie的SessionId的名称</param>
        /// <returns></returns>
        public virtual NFinal.Http.ISession GetSession(string sessionId)
        {
            return ServiceCollection.sessionConstructor(sessionId);
        }
        /// <summary>
        /// 配置数据
        /// </summary>
        [NFinal.ViewBagMember]
        [Newtonsoft.Json.JsonIgnore]
        public Config.Plug.PlugConfig config = null;
        #region 数据库相关
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        private IDbConnection con = null;
        /// <summary>
        /// 是否使用了Con属性
        /// </summary>
        protected bool hasOpenConnection = false;
        /// <summary>
        /// 获取并打开数据库连接
        /// </summary>
        public IDbConnection Con
        {
            get
            {
                //if (this is NFinal.Model.IConnection)
                //{
                if (con == null)
                {
                    con = GetDbConnection();
                    con.Open();
                    hasOpenConnection = true;
                }
                return con;
                //}
                //else
                //{
                //    throw new NFinal.Exceptions.IConnectionNotImplementedException(this.GetType());
                //}
            }
        }
        /// <summary>
        /// 解析请求参数组成实体类并插入数据库中
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
		public int Insert<TModel>(string sqlWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            return Con.SimpleInsert<int, TModel>(parameters, sqlWhere, transaction);
        }
        /// <summary>
        /// 插入Model信息
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        public TKey Insert<TKey, TModel>(string sqlWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            return Con.SimpleInsert<TKey, TModel>(parameters, sqlWhere, transaction);
        }
        /// <summary>
        /// 解析请求参数组成实体类并插入数据库中
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
		public int InsertKeyInt<TModel>(string sqlWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            return Con.SimpleInsert<int, TModel>(parameters, sqlWhere, transaction);
        }
        /// <summary>
        /// 解析请求参数组成实体类并插入数据库中
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
		public long InsertKeyLong<TModel>(string sqlWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            TModel model = this.GetModel<TModel>();
            return Con.SimpleInsert<long, TModel>(parameters, sqlWhere, transaction);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sqlWhere"></param>
        /// <returns></returns>
        public bool InsertOrUpdate<TModel>(string sqlWhere, IDbTransaction transaction = null) where TModel : class
        {
            Type modelType = typeof(TModel);
            int count = Con.SimpleExecuteScalar<int>($"select count(*) from {modelType.Name} where " + sqlWhere, null, transaction);
            if (count > 0)
            {
                return Con.SimpleUpdate<TModel>(parameters, sqlWhere, transaction);
            }
            else
            {
                var id = Con.SimpleInsert<long, TModel>(parameters, sqlWhere, transaction);
                return id > 0;
            }
        }
        /// <summary>
        /// 解析请求参数组成实体类并更新数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sqlWhere"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public bool Update<TModel>(string sqlWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            return Con.SimpleUpdate<TModel>(parameters, sqlWhere, transaction);
        }
        /// <summary>
        /// 解析请求参数组中的id并放入实体中，然后删除该记录
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="strWhere"></param>
        /// <returns></returns>
		public bool Delete<TModel>(string strWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            TModel model = this.GetModel<TModel>();
            return Con.SimpleDelete<TModel>(parameters, strWhere, transaction);
        }
        /// <summary>
        /// 获取Model信息
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        public TModel Get<TModel>(string strWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            return Con.SimpleGet<TModel>(parameters, strWhere, transaction);
        }
        /// <summary>
        /// 根据查询条件获取实体列表
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sqlWhere"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public IEnumerable<TModel> GetAll<TModel>(string sqlWhere = null, IDbTransaction transaction = null) where TModel : class
        {
            return Con.SimpleGetAll<TModel>(parameters, sqlWhere, transaction);
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<TModel> Page<TModel>(int pageIndex, int pageSize, out int count)
        {
            return Con.SimpleGetPage<TModel>(pageIndex, pageSize, out count);
        }

        /// <summary>
        /// 获取数据库接连，需要子类重写
        /// </summary>
        /// <returns></returns>
        public virtual IDbConnection GetDbConnection()
        {
            Type currentType = this.GetType();
            throw new NotImplementedException(string.Format("{0}.{1}必须重写GetDbConnection方法",
                currentType.Namespace, currentType.Name));
        }
        #endregion
        #region 返回结果相关
        /// <summary>
        /// 返回视图
        /// </summary>
        /// <param name="templateUrl"></param>
        /// <returns></returns>
        public TemplateResult View(string templateUrl = null)
        {
            return new TemplateResult(templateUrl, this.contentType);
        }
        /// <summary>
        /// 返回Json数据
        /// </summary>
        /// <returns></returns>
        public JsonResult Json<TObject>(TObject json)
        {
            return new JsonResult(json.ToString());
        }
        /// <summary>
        /// 返回true,false
        /// </summary>
        /// <param name="boolean"></param>
        /// <returns></returns>
        public JsonResult Json(bool boolean)
        {
            return new JsonResult(boolean ? "true" : "false");
        }
        /// <summary>
        /// 返回Json数据
        /// 此函数会把this.ViewBag中的值进行Json序列化
        /// </summary>
        /// <returns></returns>
        public JsonResult Json()
        {
            return new JsonResult();
        }
        #endregion
        /// <summary>
        /// 请求参数信息
        /// </summary>
        [NFinal.ViewBagMember]
        [Newtonsoft.Json.JsonIgnore]
        public NameValueCollection parameters;
        private NFinal.Collections.FastDictionary<string, NFinal.Http.HttpMultipart.HttpFile> _files;
        /// <summary>
        /// 请求的文件
        /// </summary>
        public NFinal.Collections.FastDictionary<string, NFinal.Http.HttpMultipart.HttpFile> files
        {
            get
            {
                if (_files == null)
                {
                    string requestContentType = GetRequestHeader(NFinal.Constant.HeaderContentType);
                    if (requestContentType.Split(NFinal.Constant.CharSemicolon)[0] == NFinal.Constant.ContentType_Multipart_form_data
                       )
                    {
                        string boundary = NFinal.Http.HttpMultipart.HttpMultipart.boundaryReg.Match(requestContentType).Value;
                        var multipart = new NFinal.Http.HttpMultipart.HttpMultipart(GetRequestBody(), boundary);
                        _files = new NFinal.Collections.FastDictionary<string, NFinal.Http.HttpMultipart.HttpFile>(StringComparer.Ordinal);
                        foreach (var httpMultipartBoundary in multipart.GetBoundaries())
                        {
                            if (string.IsNullOrEmpty(httpMultipartBoundary.Filename))
                            {
                                string name = httpMultipartBoundary.Name;
                                if (!string.IsNullOrEmpty(name))
                                {
                                    string value = new System.IO.StreamReader(httpMultipartBoundary.Value).ReadToEnd();
                                    parameters.Add(name, value);
                                }
                            }
                            else
                            {
                                _files.Add(httpMultipartBoundary.Name, new NFinal.Http.HttpMultipart.HttpFile(httpMultipartBoundary));
                            }
                        }
                    }
                }
                return _files;
            }
        }
        /// <summary>
        /// 输出类型
        /// </summary>
        public string contentType = "text/html; charset=utf-8";
        ///// <summary>
        ///// 内容
        ///// </summary>
        //public Stream writeStream;
        /// <summary>
        /// 服务器类型
        /// </summary>
        public ServerType _serverType = ServerType.UnKnown;
        /// <summary>
        /// Cookie
        /// </summary>
        public ICookie Cookie;
        /// <summary>
        /// Session
        /// </summary>
        public ISession Session;
        //private TUser _user;
        private string _methodName;
        /// <summary>
        /// 方法名，只读
        /// </summary>
        public string methodName { get { return _methodName; } }
        ///// <summary>
        ///// 用户
        ///// </summary>
        //public TUser user
        //{
        //    get
        //    {
        //        return Session.Get<TUser>("user");
        //    }

        //    set
        //    {
        //        _user = value;
        //        Session.Set<TUser>("user",_user);
        //    }
        //}
        /// <summary>
        /// 视图数据，通过ViewBag.字段名=值;可直接添加视图数据
        /// </summary>
        public dynamic ViewBag = null;
        /// <summary>
        /// 上下文
        /// </summary>
        internal TContext context { get; set; }
        /// <summary>
        /// 请求信息
        /// </summary>
        internal TRequest request { get; set; }
        /// <summary>
        /// 响应内容
        /// </summary>
        public Response response { get; set; }
        /// <summary>
        /// 输出内容
        /// </summary>
        public Stream outputStream { get; set; }
        /// <summary>
        /// 压缩方式
        /// </summary>
        public CompressMode compressMode { get; set; }
        #region 子类必须实现的方法
        /// <summary>
        /// 基础初始化函数
        /// </summary>
        /// <param name="plugConfig">插件配置</param>
        /// <param name="context">Http上下文</param>
        /// <param name="methodName">Http请求方法</param>
        public virtual void BaseInitialization(TContext context, ActionData<TContext, TRequest> actionData, NFinal.Config.Plug.PlugConfig plugConfig)
        {
            this.config = plugConfig;
            this.context = context;
            this.actionData = actionData;
            this._methodName = actionData?.methodName;
            this.compressMode = CompressMode.Deflate;
            this.response = new Response();
            this.response.headers = new Dictionary<string, string[]>(StringComparer.Ordinal);
            this.response.statusCode = 200;
            this.response.stream = new MemoryStream();
        }
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="plugConfig">插件配置</param>
        /// <param name="context">Http上下文</param>
        /// <param name="methodName">Http请求方法</param>
        /// <param name="outputStream">Http输出流</param>
        /// <param name="request">Http请求信息</param>
        /// <param name="compressMode">压缩模式</param>
        public virtual void Initialization(TContext context, ActionData<TContext, TRequest> actionData, Stream outputStream, TRequest request, CompressMode compressMode, NFinal.Config.Plug.PlugConfig plugConfig)
        {
            this.config = plugConfig;
            this.context = context;
            this.actionData = actionData;
            this._methodName = actionData.methodName;
            this.outputStream = outputStream;
            this.request = request;
            this.compressMode = compressMode;
            this.response = new Response();
            this.response.headers = new Dictionary<string, string[]>(StringComparer.Ordinal);
            this.response.statusCode = 200;
            this.response.stream = new MemoryStream();
        }
        ///// <summary>
        ///// 设置压缩方式
        ///// </summary>
        ///// <param name="compressMode"></param>
        //public void SetResponse(CompressMode compressMode)
        //{
        //    this.response = new Owin.Response();
        //    this.response.headers = new Dictionary<string, string[]>(StringComparer.Ordinal);
        //    this.response.statusCode = 200;
        //    this.response.stream = new MemoryStream();
        //    this.compressMode = compressMode;
        //    if (compressMode == CompressMode.None)
        //    {
        //        this.writeStream = this.response.stream;
        //    }
        //    else
        //    {
        //        if (compressMode == CompressMode.GZip)
        //        {
        //            this.writeStream = new System.IO.Compression.GZipStream(this.response.stream, System.IO.Compression.CompressionMode.Compress, true);
        //            this.response.headers.Add(NFinal.Constant.HeaderContentEncoding, NFinal.Constant.HeaderContentEncodingGzip);
        //        }
        //        else if (compressMode == CompressMode.Deflate)
        //        {
        //            this.writeStream = new System.IO.Compression.DeflateStream(this.response.stream, System.IO.Compression.CompressionMode.Compress, true);
        //            this.response.headers.Add(NFinal.Constant.HeaderContentEncoding, NFinal.Constant.HeaderContentEncodingDeflate);
        //        }
        //    }
        //}
        /// <summary>
        /// 获取请求Url
        /// </summary>
        /// <returns></returns>
        public abstract string GetRequestPath();
        /// <summary>
        /// 获取IP地址
        /// </summary>
        /// <returns></returns>
        public abstract string GetRemoteIpAddress();
        /// <summary>
        /// 获取请求头信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public abstract string GetRequestHeader(string key);
        /// <summary>
        /// 获取请求方法
        /// </summary>
        /// <returns></returns>
        public abstract string GetRequestMethod();
        /// <summary>
        /// 获取请求内容
        /// </summary>
        /// <returns></returns>
        public abstract Stream GetRequestBody();
        /// <summary>
        /// 设置请求头
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public abstract void SetResponseHeader(string key, string value);
        /// <summary>
        /// 设置请求头(多项)
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public abstract void SetResponseHeader(string key, string[] value);
        /// <summary>
        /// 设置返回状态
        /// </summary>
        /// <param name="statusCode"></param>
        public abstract void SetResponseStatusCode(int statusCode);
        /// <summary>
        /// 控制器执行前方法
        /// </summary>
        /// <returns></returns>
        public abstract bool Before();
        /// <summary>
        /// 控制器执行后方法
        /// </summary>
        public abstract void After();
        /// <summary>
        /// 关闭控制器相关资源，如输入输出流等
        /// </summary>
        public abstract Task Close();
        public abstract void Dispose();
        //public abstract void Write(byte[] buffer, int offset, int count);

        //public abstract void Write(string value);
        /// <summary>
        /// 释放控制器相关资源，如输入输出流等
        /// </summary>
        public abstract void Release();
        #endregion
        #region 扩展方法
        /// <summary>
        /// 输出字节流，用于输出二进制流，如图象，文件等。
        /// </summary>
        /// <param name="buffer">缓冲区</param>
        /// <param name="offset">起始偏移量</param>
        /// <param name="count">缓冲区大小</param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            this.response.stream.Write(buffer, 0, count);
        }
        /// <summary>
        /// 输出字节流(异步)，用于输出二进制流，如图象，文件等。
        /// </summary>
        /// <param name="buffer">缓冲区</param>
        /// <param name="offset">起始偏移量</param>
        /// <param name="count">缓冲区大小</param>
        public override Task WriteAsync(byte[] buffer, int offset, int count)
        {
            return this.response.stream.WriteAsync(buffer, offset, count);
        }
        /// <summary>
        /// 输出文本内容
        /// </summary>
        /// <param name="value">文本</param>
        public override void Write(string value)
        {
            if (value != null && value.Length != 0)
            {
                byte[] buffer = Constant.encoding.GetBytes(value);
                this.response.stream.Write(buffer, 0, buffer.Length);
            }
        }
        /// <summary>
        /// 输出文本内容
        /// </summary>
        /// <param name="value">文本</param>
        public override Task WriteAsync(string value)
        {
            if (value != null && value.Length != 0)
            {
                byte[] buffer = Constant.encoding.GetBytes(value);
                return this.response.stream.WriteAsync(buffer, 0, buffer.Length);
            }
            return Constant.CompletedTask;
        }

        /// <summary>
        /// 页面重定向
        /// </summary>
        /// <param name="url">请求URL字符串</param>
        public NFinal.RedirectResult Redirect(string url)
        {
            return new NFinal.RedirectResult(url);
            //this.SetResponseStatusCode(302);
            //this.SetResponseHeader(NFinal.Constant.HeaderContentType, NFinal.Constant.ResponseContentType_Text_html);
            //this.SetResponseHeader(NFinal.Constant.HeaderLocation, url);
        }
        /// <summary>
        /// 把视图数据转换为JSON格式，并输出
        /// </summary>
        public void AjaxReturn()
        {
            this.contentType = "application/json; charset=utf-8";
            this.Write(ServiceCollection.serializable.Serialize(this.ViewBag));
        }
        /// <summary>
        /// 输出json{code:1,msg:"",result:[json字符串]}
        /// </summary>
        /// <param name="json">json字符串</param>
        public void AjaxReturn(string json)
        {
            this.SetResponseHeader(NFinal.Constant.HeaderContentType, NFinal.Constant.ResponseContentType_Application_json);
            this.Write(json);
        }
        /// <summary>
        /// 输出true或false
        /// </summary>
        /// <param name="obj">bool变量</param>
        public void AjaxReturn(bool obj)
        {
            this.SetResponseHeader(NFinal.Constant.HeaderContentType, NFinal.Constant.ResponseContentType_Application_json);
            this.Write(obj ? NFinal.Constant.trueString : NFinal.Constant.falseString);
        }
        /// <summary>
        /// 把请求信息根据名称自动封装进TModel类型中，并返回该类型对象
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        public TModel GetModel<TModel>()
        {
            TModel model = NFinal.Model.ModelHelper.GetModel<TModel>(this.parameters);
            System.Diagnostics.Debug.WriteLine(parameters.ToString());
            System.Diagnostics.Debug.WriteLine(model);
            return model;
        }
        /// <summary>
        /// 返回数据库实体类对应的json对象（对时间进行了数字处理）
        /// </summary>
        /// <param name="model">数据库实体类</param>
        public string GetModelJson<TModel>(TModel model)
        {
            if (model == null)
            {
                return Constant.nullString;
            }
            else
            {
                NFinal.IO.StringWriter sw = new IO.StringWriter();
                NFinal.Json.JsonHelper.WriteJson(model, sw, NFinal.Json.DateTimeFormat.LocalTimeNumber);
                return sw.ToString();
            }
        }
        /// <summary>
        /// 从请求内容中解析出Json类
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <returns></returns>
        public T GetJson<T>()
        {
            string content = this.parameters.Content;
            return ServiceCollection.GetService<NFinal.Json.IJsonSerialize>().DeserializeObject<T>(content);
        }
        /// <summary>
        /// 把任意类对象转换为Json字符串
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        public string GetJson<TModel>(TModel model)
        {
            if (model == null)
            {
                return Constant.nullString;
            }
            else
            {
                return ServiceCollection.jsonSerialize.SerializeObject(model);
            }
        }
        /// <summary>
        /// 返回json字符串{code:[code状态码],msg:[msg消息],result:[json字符串]}
        /// </summary>
        /// <param name="json">json字符串</param>
        /// <param name="code">状态码</param>
        /// <param name="msg">消息</param>
        public void AjaxReturn(string json, int code, string msg)
        {
            this.SetResponseHeader(Constant.HeaderContentType, NFinal.Constant.ResponseContentType_Application_json);
            this.Write(Constant.AjaxReturnJson_Code);
            this.Write(code);
            this.Write(Constant.AjaxReturnJson_Msg);
            this.Write(msg);
            this.Write(Constant.AjaxReturnJson_Json);
            this.Write(json);
            this.Write(Constant.AjaxReturnJson_End);
        }
        /// <summary>
        /// 获取二级域名
        /// </summary>
        /// <param name="context">上下文</param>
        /// <returns></returns>
        public abstract string GetSubDomain(TContext context);
        /// <summary>
        /// 是否是手机端
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns></returns>
        public bool IsMobile(string userAgent)
        {
            string agent = userAgent;
            bool isMobile = false;
            //排除 苹果桌面系统  
            if (!userAgent.Contains(NFinal.Constant.UserAgnet_Windows_NT) && !userAgent.Contains(NFinal.Constant.UserAgent_Macintosh))
            {
                for (int i = 0; i < NFinal.Constant.UserAgent_MobileKeyWords.Length; i++)
                {
                    if (agent.Contains(NFinal.Constant.UserAgent_MobileKeyWords[i]))
                    {
                        isMobile = true;
                        break;
                    }
                }
            }
            return isMobile;
        }
        /// <summary>
        /// 返回服务器路径
        /// </summary>
        /// <param name="path">相对路径</param>
        /// <returns></returns>
        public string MapPath(string path)
        {
            return NFinal.IO.Path.MapPath((int)Application.globalConfig.projectType, Application.globalConfig.debug.enable, path);
        }
        /// <summary>
        /// 获取Url
        /// </summary>
        /// <param name="methodName">方法名</param>
        /// <param name="urlParameters">Url中的get参数</param>
        /// <returns></returns>
        public static string Url<TController>(string methodName, params StringContainer[] urlParameters)
        {
            Dictionary<string, NFinal.Url.FormatData> dictionaryFormatData = null;
            if (NFinal.Url.ActionUrlHelper.formatControllerDictionary.TryGetValue(typeof(TController).TypeHandle, out dictionaryFormatData))
            {
                NFinal.Url.FormatData formatData = null;
                if (dictionaryFormatData.TryGetValue(methodName, out formatData))
                {
                    return NFinal.Url.ActionUrlHelper.Format(formatData.formatUrl, urlParameters);
                }
            }
            var exception = new NFinal.Exceptions.UrlNotFoundException(typeof(TController), methodName, urlParameters);
            ServiceCollection.logger.Fatal(exception.Message);
            throw exception;
        }
        /// <summary>
        /// 获取Url
        /// </summary>
        /// <param name="methodName">方法名</param>
        /// <param name="urlParameters">Url参数</param>
        /// <returns></returns>
        public string Url(string methodName, params StringContainer[] urlParameters)
        {
            Dictionary<string, NFinal.Url.FormatData> dictionaryFormatData = null;
            if (NFinal.Url.ActionUrlHelper.formatControllerDictionary.TryGetValue(this.GetType().TypeHandle, out dictionaryFormatData))
            {
                NFinal.Url.FormatData formatData = null;
                if (dictionaryFormatData.TryGetValue(methodName, out formatData))
                {
                    return NFinal.Url.ActionUrlHelper.Format(formatData.formatUrl, urlParameters);
                }
            }
            var exception = new NFinal.Exceptions.UrlNotFoundException(this.GetType(), methodName, urlParameters);
            ServiceCollection.logger.Fatal(exception.Message);
            throw exception;
        }
        ///// <summary>
        ///// 模板渲染函数
        ///// </summary>
        ///// <typeparam name="T">视图数据类型</typeparam>
        ///// <param name="url">视图URL路径</param>
        ///// <param name="t">视图数据，即ViewBag</param>
        //public Task RenderModel<T>(string url, T t)
        //{
        //    NFinal.ViewDelegateData dele;
        //    if (NFinal.ViewDelegate.viewFastDic != null)
        //    {
        //        if (NFinal.ViewDelegate.viewFastDic.TryGetValue(url, out dele))
        //        {
        //            if (dele.renderMethod == null)
        //            {
        //                dele.renderMethod = NFinal.ViewDelegate.GetRenderDelegate<T>(url, Type.GetTypeFromHandle(dele.viewTypeHandle));
        //                NFinal.ViewDelegate.viewFastDic[url] = dele;
        //            }
        //            var render = (NFinal.RenderMethod<T>)dele.renderMethod;
        //            var renderClass = render(this, t);
        //            return renderClass.ExecuteAsync();
        //        }
        //        else
        //        {
        //            //模板未找到异常
        //            throw new NFinal.Exceptions.ViewNotFoundException(url);
        //        }
        //    }
        //    else
        //    {
        //        throw new NFinal.Exceptions.ViewNotFoundException(url);
        //    }
        //}
        /// <summary>
        /// 按照默认路径渲染模板
        /// </summary>
        public Task Render()
        {
            //CoreWebTest/Index1.cshtml
            NFinal.View.ITemplate template= ServiceCollection.templateConstructor(config.plug.templatePath, null);
            string url = template.GetPath(config.defaultSkin, actionData.assemblyName, actionData.controllerFullName, this.methodName);
            return template.RenderAsync(this,url, ViewBag);
        }

        /// <summary>
        /// 按照指定路径渲染模板
        /// </summary>
        /// <param name="templateUrl"></param>
        public Task Render(string templateUrl)
        {
            NFinal.View.ITemplate template = ServiceCollection.templateConstructor(config.plug.templatePath, null);
            return template.RenderAsync(this,templateUrl, this.ViewBag);
            //return this.RenderModel(templateUrl, ViewBag);
        }
        ///// <summary>
        ///// 根据母模板路径和当前路径渲染模板
        ///// </summary>
        ///// <param name="masterPagePath"></param>
        ///// <param name="templateUrl"></param>
        //public void Render(string masterPagePath, string templateUrl)
        //{
        //    if (this.MasterPage.GetType() == typeof(object))
        //    {
        //        this.Write("MasterPage必须为非dynamic以及object类型");
        //    }
        //    else
        //    {
        //        this.MasterPage.ViewData = new ViewData(templateUrl, this.ViewBag);
        //        this.RenderModel(masterPagePath,this.MasterPage);
        //    }
        //}

        
        #endregion
    }
}