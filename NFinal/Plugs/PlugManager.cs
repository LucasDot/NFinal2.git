﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : PlugManager.cs
//        Description :插件管理
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace NFinal.Plugs
{
    /// <summary>
    /// 插件管理
    /// </summary>
    public class PlugManager
    {
        private NFinal.Logs.ILogger logger;
        public PlugManager(NFinal.Logs.ILogger logger)
        {
            this.logger = logger;
            if (!isInit)
            {
                isInit = true;
                Init();
            }
        }
        /// <summary>
        /// 插件列表
        /// </summary>
        public static List<NFinal.Plugs.PlugInfo> plugInfoList =null;
        /// <summary>
        /// 插件是否加载
        /// </summary>
        public static bool isInit = false;
        /// <summary>
        /// 获取程序集
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Assembly> GetAssemblys()
        {
            foreach (var plug in plugInfoList)
            {
                if (plug.enable && plug.loadSuccess)
                {
                    yield return plug.assembly;
                }
            }
        }
        /// <summary>
        /// 加载插件
        /// </summary>
        public void Init()
        {
            if (plugInfoList == null)
            {
                plugInfoList = new List<PlugInfo>();
                NFinal.Plugs.Loader.IAssemblyLoader assemblyLoader = new NFinal.Plugs.Loader.AssemblyLoader();
                NFinal.Plugs.PlugInfo plugInfo = null;
                bool loadSuccess = false;
                string assemblyFilePath = null;
                foreach (var plug in Application.plugConfigDictionary)
                {
                    assemblyFilePath = NFinal.IO.Path.GetApplicationPath(plug.Value.plug.assemblyPath);
                    plugInfo = new PlugInfo();
                    plugInfo.config = Application.plugConfigDictionary[plug.Value.plug.name];
                    plugInfo.loadSuccess = false;
                    plugInfo.assembly = null;
                    plugInfo.assemblyFullPath = assemblyFilePath;
                    //plugInfo.configPath = plug.configPath;
                    plugInfo.description = plug.Value.plug.description;
                    plugInfo.enable = plug.Value.plug.enable;
                    plugInfo.name = plug.Value.plug.name;
                    plugInfo.urlPrefix = plug.Value.plug.urlPrefix;
                    plugInfoList.Add(plugInfo);
                }
                logger.Info("使用priority优先集对插件进行排序.");
                plugInfoList.Sort((x, y)=>{
                    return x.priority - y.priority;
                });
                foreach (var plug in plugInfoList)
                {
                    loadSuccess = false;
                    if (plug.enable)
                    {
                        if (File.Exists(plug.assemblyFullPath))
                        {
                            try
                            {
                                logger.Info("开始加载插件"+plug.name+":"+plug.assemblyFullPath);
                                assemblyLoader.Load(plug.assemblyFullPath);
                                loadSuccess = true;
                                logger.Info("加载插件"+plug.name+"成功");
                            }
                            catch (Exception ex)
                            {
                                logger.Error("加载插件"+plug.name+"出错:"+ex.Message);
                                loadSuccess = false;
                                //throw ex;
                            }
                        }
                        else
                        {
                            throw new NFinal.Exceptions.PlugAssemblyNotFoundException(plug.assemblyFullPath);
                        }
                    }
                    if (loadSuccess)
                    {
                        plug.assembly = assemblyLoader.assemblyDictionary[plug.assemblyFullPath];
                    }
                    else
                    {
                        plug.assembly = null;
                    }
                    plug.loadSuccess = loadSuccess;
                }
            }
        }
    }
}
