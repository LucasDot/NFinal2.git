﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.Action;

namespace NFinal.Filter
{
    public class ExceptionFilterAttribute : System.Exception,IExceptionFilter
    {
        public Exception ExceptionHandler { get ; set; }
        public IActionResult ActionResult { get; set; }

        public ExceptionFilterAttribute()
        {

        }

        public void ExceptionFilter<TContext, TRequest>(AbstractAction<TContext, TRequest> action)
        {
            
        }
    }
}
