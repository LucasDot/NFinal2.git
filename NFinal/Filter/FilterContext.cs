﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Filter
{
    public abstract class FilterContext<TContext,TRequest> : NFinal.Action.AbstractAction<TContext, TRequest>
    {
        public IActionResult Result { get; set; }
    }
}
