using System.Threading.Tasks;

namespace NFinal.Filter
{
    public interface IAsyncAuthorizationFilter : IFilterMetadata
    {

        Task OnAuthorizationAsync<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context);
    }
}
