﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Filter
{
    public interface IParameterFilter
    {
        IActionResult ActionResult { get; set; }
        bool ParameterFilter<TContext>(TContext context,NameValueCollection parameters);
    }
}
