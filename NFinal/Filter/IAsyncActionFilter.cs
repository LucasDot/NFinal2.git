using System.Threading.Tasks;

namespace NFinal.Filter
{
    public interface IAsyncActionFilter : IFilterMetadata
    {
        Task OnActionExecutionAsync<TContext, TRequest>(Action.AbstractAction<TContext, TRequest> context);
    }
}
