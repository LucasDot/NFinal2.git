using System.Threading.Tasks;

namespace NFinal.Filter
{
    public interface IAsyncResultFilter : IFilterMetadata
    {
        Task OnResultExecutionAsync<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context);
    }
}
