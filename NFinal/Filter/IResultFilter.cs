﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Filter
{
    public interface IResultFilter : IFilterMetadata
    {
        void OnResultExecuting<TContext, TRequest>(Filter.FilterContext<TContext, TRequest> context);
        void OnResultExecuted<TContext, TRequest>(Filter.FilterContext<TContext, TRequest> context);
    }
}