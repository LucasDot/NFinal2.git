﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Filter
{
    public interface IConnectionFilter
    {
        bool ConnectionFilter<TContext>(TContext context,Http.HttpConnection connection);
    }
}
