using System.Threading.Tasks;

namespace NFinal.Filter
{

    public interface IAsyncExceptionFilter : IFilterMetadata
    {
        Task OnExceptionAsync<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> context);
    }
}
