namespace NFinal.Filter
{
    public interface IActionFilter : IFilterMetadata
    {
        void OnActionExecuting<TContext, TRequest>(Filter.FilterContext<TContext,TRequest> context);

        void OnActionExecuted<TContext, TRequest>(Filter.FilterContext<TContext, TRequest> context);
    }
}