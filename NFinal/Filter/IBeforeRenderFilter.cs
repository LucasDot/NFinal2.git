﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Filter
{
    public interface IBeforeRenderFilter
    {
        IActionResult ActionResult { get; set; }
        bool RenderFilter<TContext, TRequest>(NFinal.Action.AbstractAction<TContext, TRequest> action);
    }
}
