﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Http
{
    public class HttpConnection
    {
        public string ConnectionId { get; set; }
        public string RemoteIpAddress { get; set; }
        public bool IsLocal { get; set; }
        public Func<string, string[]> GetHeader { get; set; }
    }
}
