﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
using System.Reflection;

namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_View_DotLiquid_Extension
    {
        /// <summary>
        /// 设置二进制序列化
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void SetViewDotLiquid(this IServiceCollection serviceCollection, NFinal.Logs.ILogger logger)
        {
            serviceCollection.SetService<NFinal.View.ITemplate, string, object>(typeof(NFinal.View.DotLiquid.DotLiquidTempalte)).Configure(logger);
        }
    }
}
