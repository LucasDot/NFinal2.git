﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using System.IO;
using DotLiquidEx = DotLiquid;
using NFinal.IO;
using System.Threading.Tasks;

namespace NFinal.View.DotLiquid
{
    public class DotLiquidTempalte:NFinal.View.ITemplate
    {
        public static NFinal.Logs.ILogger logger;
        public static void Configure(NFinal.Logs.ILogger logger)
        {
            DotLiquidTempalte.logger = logger;
        }
        public string RootPath { get; set; }
        public string Extension { get { return ".htm"; } }

        public Writer writer { get; set; }

        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            string path = controllerFullName.Substring(assemblyName.Length);
            path = path.Replace('.', '/');
            if (string.IsNullOrEmpty(skinName))
            {
                path = "/" + assemblyName + "/" + path + "/" + methodName + Extension;
            }
            else
            {
                path = skinName + "/" + path + "/" + methodName + Extension;
            }
            return path;
        }
        /// <summary>
        /// 获取绝对路径
        /// </summary>
        /// <param name="absoluteDirectory">绝对根目录</param>
        /// <param name="relativePath">相对路径</param>
        /// <returns></returns>
        public static string GetAbsolutePath(string absoluteDirectory, string relativePath)
        {
            //relativePath= relativePath.Trim('/');
            string[] relativeDirectories = relativePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string absolutePath = absoluteDirectory;
            for (int i = 0; i < relativeDirectories.Length; i++)
            {
                if (relativeDirectories[i] == "..")
                {
                    absolutePath = System.IO.Path.GetDirectoryName(absolutePath);
                }
                else if (relativeDirectories[i] == ".")
                {
                    absolutePath = absoluteDirectory;
                }
                else if (relativeDirectories[i] == "")
                {
                    absolutePath = absoluteDirectory;
                }
                else
                {
                    absolutePath = System.IO.Path.Combine(absolutePath, relativeDirectories[i]);
                }
            }
            return absolutePath;
        }
        public string Render(Writer writer, string relativePath, object model)
        {
            Type type = model.GetType();
            string filePath = GetAbsolutePath(RootPath, relativePath);
            StreamReader reader= File.OpenText(filePath);
            try
            {
                string content = reader.ReadToEnd();
                DotLiquidEx.Template template = DotLiquidEx.Template.Parse(content);
                DotLiquidEx.Hash hash = new DotLiquidEx.Hash();
                GetVariableScope(hash, model);
                return template.Render(hash);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader?.Dispose();
            }
        }
        public static NFinal.Collections.FastDictionary<RuntimeTypeHandle, Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash>>
            getVarDelegate = new Collections.FastDictionary<RuntimeTypeHandle, Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash>>();
        public DotLiquidEx.Hash GetVariableScope(object model)
        {
            DotLiquidEx.Hash variableScope = new DotLiquidEx.Hash();
            return GetVariableScope(variableScope, model);
        }
        public DotLiquidEx.Hash GetVariableScope(DotLiquidEx.Hash variableScope, object model)
        {
            Type type = model.GetType();
            RuntimeTypeHandle runtimeTypeHandle = type.TypeHandle;
            if (!getVarDelegate.TryGetValue(runtimeTypeHandle, out Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash> GetVariableScopeDelegate))
            {
                getVarDelegate[runtimeTypeHandle] = GetVariableScopeFunc(model);
            }
            return GetVariableScopeDelegate(variableScope, model);
        }
        public static MethodInfo pushMethodInfo = typeof(DotLiquidEx.Hash)
            .GetMethod("Add", new Type[] { typeof(string), typeof(object) });
        public Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash> GetVariableScopeFunc(object model)
        {
            Type type = model.GetType();
            DynamicMethod dynamicMethod = new DynamicMethod("DotLiquid", typeof(DotLiquidEx.Hash)
                , new Type[] { typeof(DotLiquidEx.Hash), type });
            var iLGenerator = dynamicMethod.GetILGenerator();
            var variableScope = iLGenerator.DeclareLocal(typeof(DotLiquidEx.Hash));

            FieldInfo[] fields = type.GetFields();
            //List<KeyValuePair<string, FieldInfo>> fieldList = new List<KeyValuePair<string, FieldInfo>>();
            for (int i = 0; i < fields.Length; i++)
            {
                iLGenerator.Emit(OpCodes.Ldarg_0);
                iLGenerator.Emit(OpCodes.Ldstr, fields[i].Name);
                iLGenerator.Emit(OpCodes.Ldarg_1);
                iLGenerator.Emit(OpCodes.Ldfld, fields[i]);
                if (fields[i].FieldType.GetTypeInfo().IsValueType)
                {
                    iLGenerator.Emit(OpCodes.Box, fields[i].FieldType);
                }
                iLGenerator.Emit(OpCodes.Callvirt, pushMethodInfo);
            }
            PropertyInfo[] propertys = type.GetProperties();
            for (int i = 0; i < propertys.Length; i++)
            {
                iLGenerator.Emit(OpCodes.Ldarg_0);
                iLGenerator.Emit(OpCodes.Ldstr, propertys[i].Name);
                iLGenerator.Emit(OpCodes.Ldarg_1);
                iLGenerator.Emit(OpCodes.Callvirt, propertys[i].GetGetMethod());
                if (propertys[i].PropertyType.GetTypeInfo().IsValueType)
                {
                    iLGenerator.Emit(OpCodes.Box, propertys[i].PropertyType);
                }
                iLGenerator.Emit(OpCodes.Callvirt, pushMethodInfo);
            }
            iLGenerator.Emit(OpCodes.Ldloc, variableScope);
            iLGenerator.Emit(OpCodes.Ret);
            Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash> variableScopeFunc
                = (Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash>)
                dynamicMethod.CreateDelegate(typeof(Func<DotLiquidEx.Hash, object, DotLiquidEx.Hash>));
            return variableScopeFunc;
        }

        public DotLiquidTempalte(string rootPath, object options)
        {
            this.RootPath = rootPath;
        }

        public Task RenderAsync(Writer writer, string url, object model)
        {
            string content = Render(writer,url, model);
            return this.writer.WriteAsync(content);
        }
    }
}
