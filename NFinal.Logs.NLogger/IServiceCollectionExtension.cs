﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Logs_NLogger_Extension
    {
        public static void SetLoggersNlogger(this IServiceCollection serviceCollection)
        {
            serviceCollection.SetService<NFinal.Logs.ILogger, string>(typeof(NFinal.Logs.NLogger.NLogger));
        }
    }
}
