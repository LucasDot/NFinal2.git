﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.IO;
#if !NET20
using System.Threading.Tasks;
#endif
using NustacheEx = Nustache.Core;
namespace NFinal.View.Nustache
{
    public class NustacheTemplate : NFinal.View.ITemplate
    {
        public static NFinal.Logs.ILogger logger;
        public static void Configure(NFinal.Logs.ILogger logger)
        {
            NustacheTemplate.logger = logger;
        }
        public string RootPath { get; set; }
        public string Extension { get { return ".htm"; } }
        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            string path = controllerFullName.Substring(assemblyName.Length);
            path = path.Replace('.', '/');
            if (string.IsNullOrEmpty(skinName))
            {
                path = "/" + assemblyName + "/" + path + "/" + methodName + Extension;
            }
            else
            {
                path = skinName + "/" + path + "/" + methodName + Extension;
            }
            return path;
        }
        /// <summary>
        /// 获取绝对路径
        /// </summary>
        /// <param name="absoluteDirectory">绝对根目录</param>
        /// <param name="relativePath">相对路径</param>
        /// <returns></returns>
        public static string GetAbsolutePath(string absoluteDirectory, string relativePath)
        {
            //relativePath= relativePath.Trim('/');
            string[] relativeDirectories = relativePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string absolutePath = absoluteDirectory;
            for (int i = 0; i < relativeDirectories.Length; i++)
            {
                if (relativeDirectories[i] == "..")
                {
                    absolutePath = System.IO.Path.GetDirectoryName(absolutePath);
                }
                else if (relativeDirectories[i] == ".")
                {
                    absolutePath = absoluteDirectory;
                }
                else if (relativeDirectories[i] == "")
                {
                    absolutePath = absoluteDirectory;
                }
                else
                {
                    absolutePath = System.IO.Path.Combine(absolutePath, relativeDirectories[i]);
                }
            }
            return absolutePath;
        }
#if !NET20
        public Task RenderAsync(NFinal.IO.Writer writer,string relativePath,object model)
        {
            string content= Render(writer,relativePath,model);
            return NFinal.DependencyInjection.TaskHelper.CompletedTask;
        }
#endif
        public string Render(NFinal.IO.Writer writer,string relativePath, object model)
        {
            string filePath = GetAbsolutePath(RootPath, relativePath);
            return NustacheEx.Render.FileToString(filePath, model);
        }
        public NustacheTemplate( string rootPath, object options)
        {
            this.RootPath = rootPath;
        }
    }
}
