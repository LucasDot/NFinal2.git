﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Validation.Attribute
{
    public interface IValidation
    {
        bool OnValidate();
    }
}
