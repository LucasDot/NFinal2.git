﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFinal;
//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinalServerTest.Controllers.IndexController_Model
{
	public class Default
	{
		[ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public string imageServerUrl;
		[NFinal.ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public NFinal.Config.Plug.PlugConfig config;
		[NFinal.ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public NFinal.NameValueCollection parameters;
		public string Message;
		public string Title;
	}
	public class Ajax
	{
		[ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public string imageServerUrl;
		[NFinal.ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public NFinal.Config.Plug.PlugConfig config;
		[NFinal.ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public NFinal.NameValueCollection parameters;
		public string Message;
		public int id;
		public System.DateTime time;
	}
}
