﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NFinal;

namespace NFinalServerTest.Controllers
{
    public class IndexController:BaseController
    {
        public ActionResult Default()
        {
            this.ViewBag.Message = "Hello World!";
            this.ViewBag.Title = "Title";
            return View();
        }
        public ActionResult Ajax()
        {
            this.ViewBag.Message = "Hello Json!";
            this.ViewBag.id = 2;
            
            this.ViewBag.time = DateTime.Now;
            return Json();
        }
    }
}
