﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using NFinal;

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinalServerTest.Views.Index
{
    /// <summary>
    /// 模板类
    /// </summary>
    [View("/NFinalServerTest/Views/Index/Default.cshtml")]
    public class Default_cshtml : NFinal.View.RazorView<NFinalServerTest.Controllers.IndexController_Model.Default>
    {
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">数据</param>
        public Default_cshtml(NFinal.IO.Writer writer, NFinalServerTest.Controllers.IndexController_Model.Default Model) : base(writer, Model)
        {
        }
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            this.ExecuteAsync().Wait();
        }
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
        {
            await writer.WriteAsync("");

            Layout = "/NFinalServerTest/Views/Shared/_Layout.cshtml";
            await writer.WriteAsync("\r\n<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n    <title>33</title>\r\n\t<meta http-equiv=\"Cache-Control\" content=\"no-cache\">\r\n</head>\r\n<body>\r\n");
            await writer.WriteAsync("    ");
            await writer.WriteAsync(RenderPage("/NFinalServerTest/Views/Shared/HeadContent.cshtml"));
            await writer.WriteAsync("\r\n    <h2>Message:");
            await writer.WriteAsync(Model.Message);
            await writer.WriteAsync("</h2>\r\n    <h2>siteName：");
            await writer.WriteAsync(Model.config.keyValueCache["siteName"].value);
            await writer.WriteAsync("</h2>\r\n    <h2>mobile:");
            await writer.WriteAsync(Model.config.keyValueCache["mobile"].value);
            await writer.WriteAsync("</h2>\r\n\r\n</body>\r\n</html> ");
        }
    }
}
