﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using NFinal;

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinalServerTest.Views.Shared
{
    /// <summary>
    /// 模板类
    /// </summary>
    [View("/NFinalServerTest/Views/Shared/HeadContent.cshtml")]
    public class HeadContent_cshtml : NFinal.View.RazorView<string>
    {
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">任意对象</param>
        public HeadContent_cshtml(NFinal.IO.Writer writer, object Model) : base(writer, Model)
        {

        }
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">实体数据</param>
        public HeadContent_cshtml(NFinal.IO.Writer writer, string Model) : base(writer, Model)
        {

        }
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            this.ExecuteAsync().Wait();
        }
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
        {
            writer.Write("<link rel=\"stylesheet\" href=\"/Content/lib/bootstrap/dist/css/bootstrap.css\" />\r\n<link rel=\"stylesheet\" href=\"/Content/css/site.css\" />\r\n<script src=\"/Content/lib/jquery/dist/jquery.js\"></script>\r\n<script src=\"/Content/lib/bootstrap/dist/js/bootstrap.js\"></script>\r\n<script src=\"/Content/js/site.js\"></script>");
        }
    }
}
