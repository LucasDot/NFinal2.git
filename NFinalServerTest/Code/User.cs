﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace NFinalServerTest.Code
{
    /// <summary>
    /// 用于缓存的用户数据
    /// </summary>
    [ProtoContract]
    public class User
    {
        [ProtoMember(1)]
        public  string Id { get; set; }
        [ProtoMember(2)]
        public  string Name { get; set; }
        [ProtoMember(3)]
        public  string Password { get; set; }
        [ProtoMember(4)]
        public  string Account { get; set; }
    }
}
