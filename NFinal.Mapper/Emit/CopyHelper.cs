﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : CopyHelper.cs
//        Description :两个不同类型中相同名称的字段及属性值的复制帮助类
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace NFinal.Emit
{
    public class Tuple<T1, T2>
    {
        public Tuple(T1 t1, T2 t2)
        {
            this.t1 = t1;
            this.t2 = t2;
        }
        public T1 t1;
        public T2 t2;
    }
    /// <summary>
    /// 两个不同类型中相同名称的字段及属性值的复制帮助类
    /// </summary>
    public class CopyHelper
    {
        /// <summary>
        /// 复制代理类
        /// </summary>
        public static NFinal.Collections.FastAsyncDictionary<Tuple<RuntimeTypeHandle,RuntimeTypeHandle>, Delegate> CopyDic =
            new NFinal.Collections.FastAsyncDictionary<Tuple<RuntimeTypeHandle, RuntimeTypeHandle>, Delegate>();
        /// <summary>
        /// 复制代码声明
        /// </summary>
        /// <typeparam name="From">要复制的类型</typeparam>
        /// <typeparam name="To">复制到的类型</typeparam>
        /// <param name="f">复制对象</param>
        /// <param name="t">复制到的对象</param>
        /// <returns></returns>
        public delegate To CopyDelegate<From, To>(From f, To t);
        public delegate To GetModelDelegate<To>(object obj);
        public struct PropertyAndField
        {
            public PropertyAndField(PropertyInfo propertyInfo)
            {
                this.isPropertyOrField = true;
                this.name = propertyInfo.Name;
                this.type = propertyInfo.PropertyType;
                this.propertyInfo = propertyInfo;
                this.fieldInfo = null;
            }
            public PropertyAndField(FieldInfo fieldInfo)
            {
                this.isPropertyOrField = false;
                this.name = fieldInfo.Name;
                this.type = fieldInfo.FieldType;
                this.propertyInfo = null;
                this.fieldInfo = fieldInfo;
            }
            public string name;
            public Type type;
            public bool isPropertyOrField;
            public FieldInfo fieldInfo;
            public PropertyInfo propertyInfo;
        }
        public static To GetModel<To>(object obj)
        {
            Type fromType = obj.GetType();
            System.RuntimeTypeHandle fromTypeHandle = fromType.TypeHandle;
            Type toType = typeof(To);
            GetModelDelegate<To> copyDelegate;
            Tuple<RuntimeTypeHandle, RuntimeTypeHandle> keyDelegate = new Tuple<RuntimeTypeHandle, RuntimeTypeHandle>(fromType.TypeHandle, toType.TypeHandle);
            //添加生成锁,在高并发时，生成代理需要很长时间，这时可能有几百个线程同时都在生成代理。加锁后，生成代理的线程只能有一个，其它线程等待生成结束。
            Delegate delegatgeTemp = CopyDic.GetOrAddValue<Tuple<Type, Type>>(keyDelegate, (x) => {
                DynamicMethod CopyMethod = new DynamicMethod("Copy_" + fromType.Name + toType.Name, typeof(To), new Type[] { typeof(object) }, true);
                ILGenerator methodIL = CopyMethod.GetILGenerator();
                List<PropertyAndField> fromPropertyAndFieldArray = new List<PropertyAndField>();
                List<PropertyAndField> toPropertyAndFieldArray = new List<PropertyAndField>();
                var localFrom= methodIL.DeclareLocal(fromType);
                var localTo = methodIL.DeclareLocal(toType);

                methodIL.Emit(OpCodes.Ldarg_0);
                methodIL.Emit(OpCodes.Castclass,fromType);
                methodIL.Emit(OpCodes.Stloc, localFrom);

                methodIL.Emit(OpCodes.Newobj, toType.GetConstructor(Type.EmptyTypes));
                methodIL.Emit(OpCodes.Stloc, localTo);

                PropertyInfo[] fromTypePropertyInfoArray = fromType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] toTypePropertyInfoArray = toType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var fromTypePropertyInfo in fromTypePropertyInfoArray)
                {
                    fromPropertyAndFieldArray.Add(new PropertyAndField(fromTypePropertyInfo));
                }
                foreach (var toTypePropertyInfo in toTypePropertyInfoArray)
                {
                    toPropertyAndFieldArray.Add(new PropertyAndField(toTypePropertyInfo));
                }

                FieldInfo[] fromTypeFieldInfoArray = fromType.GetFields(BindingFlags.Public | BindingFlags.Instance);
                FieldInfo[] toTypeFieldInfoArray = toType.GetFields(BindingFlags.Public | BindingFlags.Instance);
                foreach (var fromTypeFieldInfo in fromTypeFieldInfoArray)
                {
                    fromPropertyAndFieldArray.Add(new PropertyAndField(fromTypeFieldInfo));
                }
                foreach (var toTypeFieldInfo in toTypeFieldInfoArray)
                {
                    toPropertyAndFieldArray.Add(new PropertyAndField(toTypeFieldInfo));
                }
                foreach (var fromPropertyAndField in fromPropertyAndFieldArray)
                {
                    foreach (var toPropertyAndField in toPropertyAndFieldArray)
                    {
                        if ((fromPropertyAndField.name == toPropertyAndField.name)
                            && (fromPropertyAndField.type == toPropertyAndField.type))
                        {
                            methodIL.Emit(OpCodes.Ldloc, localTo);
                            methodIL.Emit(OpCodes.Ldloc, localFrom);
                            if (fromPropertyAndField.isPropertyOrField)
                            {

                                methodIL.Emit(OpCodes.Callvirt, fromPropertyAndField.propertyInfo.GetGetMethod());

                            }
                            else
                            {
                                methodIL.Emit(OpCodes.Ldfld, fromPropertyAndField.fieldInfo);
                            }
                            if (toPropertyAndField.isPropertyOrField)
                            {
                                methodIL.Emit(OpCodes.Callvirt, toPropertyAndField.propertyInfo.GetSetMethod());
                            }
                            else
                            {
                                methodIL.Emit(OpCodes.Stfld, toPropertyAndField.fieldInfo);
                            }
                        }
                    }
                }
                methodIL.Emit(OpCodes.Ldloc,localTo);
                methodIL.Emit(OpCodes.Ret);
                return CopyMethod.CreateDelegate(typeof(GetModelDelegate<To>));

            }, new Tuple<Type,Type>(fromType, toType));
            copyDelegate = (GetModelDelegate<To>)delegatgeTemp;
            return copyDelegate(obj);
        }
        /// <summary>
        /// 复制
        /// </summary>
        /// <typeparam name="From">要复制的类型</typeparam>
        /// <typeparam name="To">复制到的类型</typeparam>
        /// <param name="f">复制对象</param>
        /// <param name="t">复制到的对象</param>
        /// <returns></returns>
        public static To CopyEmit<From, To>(From f, To t)
        {
            Type fromType = typeof(From);
            Type toType = typeof(To);
            CopyDelegate<From, To> copyDelegate;
            Tuple<RuntimeTypeHandle, RuntimeTypeHandle> keyDelegate = new Tuple<RuntimeTypeHandle, RuntimeTypeHandle>(fromType.TypeHandle, toType.TypeHandle);
            //添加生成锁,在高并发时，生成代理需要很长时间，这时可能有几百个线程同时都在生成代理。加锁后，生成代理的线程只能有一个，其它线程等待生成结束。
            Delegate delegatgeTemp = CopyDic.GetOrAddValue<Tuple<From, To>>(keyDelegate, (x) => {
                DynamicMethod CopyMethod = new DynamicMethod("Copy_" + fromType.Name + toType.Name, typeof(To), new Type[] { typeof(From), typeof(To) }, true);
                ILGenerator methodIL = CopyMethod.GetILGenerator();
                List<PropertyAndField> fromPropertyAndFieldArray = new List<PropertyAndField>();
                List<PropertyAndField> toPropertyAndFieldArray = new List<PropertyAndField>();

                PropertyInfo[] fromTypePropertyInfoArray = fromType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] toTypePropertyInfoArray = toType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var fromTypePropertyInfo in fromTypePropertyInfoArray)
                {
                    fromPropertyAndFieldArray.Add(new PropertyAndField(fromTypePropertyInfo));
                }
                foreach (var toTypePropertyInfo in toTypePropertyInfoArray)
                {
                    toPropertyAndFieldArray.Add(new PropertyAndField(toTypePropertyInfo));
                }
                
                FieldInfo[] fromTypeFieldInfoArray = fromType.GetFields(BindingFlags.Public | BindingFlags.Instance);
                FieldInfo[] toTypeFieldInfoArray = toType.GetFields(BindingFlags.Public | BindingFlags.Instance);
                foreach (var fromTypeFieldInfo in fromTypeFieldInfoArray)
                {
                    fromPropertyAndFieldArray.Add(new PropertyAndField(fromTypeFieldInfo));
                }
                foreach (var toTypeFieldInfo in toTypeFieldInfoArray)
                {
                    toPropertyAndFieldArray.Add(new PropertyAndField(toTypeFieldInfo));
                }
                foreach (var fromPropertyAndField in fromPropertyAndFieldArray)
                {
                    foreach (var toPropertyAndField in toPropertyAndFieldArray)
                    {
                        if ((fromPropertyAndField.name == toPropertyAndField.name)
                            && (fromPropertyAndField.type == toPropertyAndField.type))
                        {
                            methodIL.Emit(OpCodes.Ldarg_1);
                            methodIL.Emit(OpCodes.Ldarg_0);
                            if (fromPropertyAndField.isPropertyOrField)
                            {

                                methodIL.Emit(OpCodes.Callvirt, fromPropertyAndField.propertyInfo.GetGetMethod());

                            }
                            else
                            {
                                methodIL.Emit(OpCodes.Ldfld, fromPropertyAndField.fieldInfo);
                            }
                            if (toPropertyAndField.isPropertyOrField)
                            {
                                methodIL.Emit(OpCodes.Callvirt, toPropertyAndField.propertyInfo.GetSetMethod());
                            }
                            else
                            {
                                methodIL.Emit(OpCodes.Stfld, toPropertyAndField.fieldInfo);
                            }
                        }
                    }
                }
                methodIL.Emit(OpCodes.Ldarg_1);
                methodIL.Emit(OpCodes.Ret);
                return CopyMethod.CreateDelegate(typeof(CopyDelegate<From, To>));

            }, new Tuple<From, To>(f, t));
            copyDelegate = (CopyDelegate<From, To>)delegatgeTemp;
            return copyDelegate(f, t);
        }
        public class From
        {
            public int valueProperty { get; set; }
            public int valueField;
            public string referenceProperty { get; set; }
            public string referenceField;
        }
        public class To
        {
            public int valueProperty { get; set; }
            public int valueField;
            public string referenceProperty { get; set; }
            public string referenceField;
        }
        public void Run()
        {
            From from = new From();
            from.valueProperty = 1;
            from.valueField = 2;
            from.referenceProperty = "3";
            from.referenceField = "4";
            To to = new To();
        }
        public void Copy(From from,To to)
        {
            string commit = "";
            commit = "值类型属性复制到值类型属性";
            to.valueProperty = from.valueProperty;
            commit = "值类型字段复制到值类型字段";
            to.valueField = from.valueField;
            commit = "引用类型属性复制到引用类型属性";
            to.referenceProperty = from.referenceProperty;
            commit = "引用类型字段复制到引用类型字段";
            to.referenceField = from.referenceField;
            commit = "值类型属性复制到值类型字段";
            to.valueField = from.valueProperty;
            commit = "值类型字段复制到值类型属性";
            to.valueProperty = from.valueField;
            commit = "引用类型属性复制到引用类型字段";
            to.referenceField = from.referenceProperty;
            commit = "引用类型字段复制到引用类型属性";
            to.referenceProperty = from.referenceField;
        }
        public void Copy(string[] from,To to)
        {
            bool success = int.TryParse(from[0], out int valueProperty);
            if (success)
            {
                to.valueProperty = valueProperty;
            }
            success = int.TryParse(from[1], out int valueField);
            if (success)
            {
                to.valueField = valueField;
            }
            to.referenceProperty = from[2];
            to.referenceField = from[3];
        }
        public void Copy(From from, string[] to)
        {
            to[0] = from.valueProperty.ToString();
            to[1] = from.valueField.ToString();
            to[2] = from.referenceProperty;
            to[3] = from.referenceField;
        }
    }
}
