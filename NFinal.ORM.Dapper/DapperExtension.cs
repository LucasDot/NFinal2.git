﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DapperEx = Dapper;
namespace NFinal.ORM.Dapper
{
    public static class DapperExtension
    {

        //
        // 摘要:
        //     Execute parameterized SQL
        //
        // 返回结果:
        //     Number of rows affected
        public static int Execute(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            DapperEx.SqlMapper.Execute(cnn, sql, param, transaction, commandTimeout, commandType);
        }
        //
        // 摘要:
        //     Execute parameterized SQL
        //
        // 返回结果:
        //     Number of rows affected
        public static int Execute(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a command asynchronously using .NET 4.5 Task.
        public static Task<int> ExecuteAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a command asynchronously using .NET 4.5 Task.
        public static Task<int> ExecuteAsync(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute parameterized SQL and return an System.Data.IDataReader
        //
        // 返回结果:
        //     An System.Data.IDataReader that can be used to iterate over the results of the
        //     SQL query.
        //
        // 备注:
        //     This is typically used when the results of a query are not processed by Dapper,
        //     for example, used to fill a System.Data.DataTable or DataSet.
        public static IDataReader ExecuteReader(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute parameterized SQL and return an System.Data.IDataReader
        //
        // 返回结果:
        //     An System.Data.IDataReader that can be used to iterate over the results of the
        //     SQL query.
        //
        // 备注:
        //     This is typically used when the results of a query are not processed by Dapper,
        //     for example, used to fill a System.Data.DataTable or DataSet.
        public static IDataReader ExecuteReader(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute parameterized SQL and return an System.Data.IDataReader
        //
        // 返回结果:
        //     An System.Data.IDataReader that can be used to iterate over the results of the
        //     SQL query.
        //
        // 备注:
        //     This is typically used when the results of a query are not processed by Dapper,
        //     for example, used to fill a System.Data.DataTable or DataSet.
        public static IDataReader ExecuteReader(this IDbConnection cnn, CommandDefinition command, CommandBehavior commandBehavior);
        //
        // 摘要:
        //     Execute parameterized SQL and return an System.Data.IDataReader
        //
        // 返回结果:
        //     An System.Data.IDataReader that can be used to iterate over the results of the
        //     SQL query.
        //
        // 备注:
        //     This is typically used when the results of a query are not processed by Dapper,
        //     for example, used to fill a System.Data.DataTable or DataSet.
        public static Task<IDataReader> ExecuteReaderAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute parameterized SQL and return an System.Data.IDataReader
        //
        // 返回结果:
        //     An System.Data.IDataReader that can be used to iterate over the results of the
        //     SQL query.
        //
        // 备注:
        //     This is typically used when the results of a query are not processed by Dapper,
        //     for example, used to fill a System.Data.DataTable or DataSet.
        public static Task<IDataReader> ExecuteReaderAsync(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static object ExecuteScalar(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static T ExecuteScalar<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static object ExecuteScalar(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static T ExecuteScalar<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static Task<T> ExecuteScalarAsync<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static Task<object> ExecuteScalarAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static Task<T> ExecuteScalarAsync<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute parameterized SQL that selects a single value
        //
        // 返回结果:
        //     The first cell selected
        public static Task<object> ExecuteScalarAsync(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Internal use only
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", true)]
        public static IDbDataParameter FindOrAddParameter(IDataParameterCollection parameters, IDbCommand command, string name);
        //
        // 摘要:
        //     Convert numeric values to their string form for SQL literal purposes
        [Obsolete("This method is for internal use only")]
        public static string Format(object value);
        //
        // 摘要:
        //     Return a list of all the queries cached by dapper
        //
        // 参数:
        //   ignoreHitCountAbove:
        public static IEnumerable<Tuple<string, string, int>> GetCachedSQL(int ignoreHitCountAbove = int.MaxValue);
        //
        // 摘要:
        //     Return a count of all the cached queries by dapper
        public static int GetCachedSQLCount();
        //
        // 摘要:
        //     Get the DbType that maps to a given value
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", false)]
        public static DbType GetDbType(object value);
        //
        // 摘要:
        //     Deep diagnostics only: find any hash collisions in the cache
        public static IEnumerable<Tuple<int, int>> GetHashCollissions();
        //
        // 摘要:
        //     Gets the row parser for a specific row on a data reader. This allows for type
        //     switching every row based on, for example, a TypeId column. You could return
        //     a collection of the base type but have each more specific.
        //
        // 参数:
        //   reader:
        //     The data reader to get the parser for the current row from
        //
        //   type:
        //     The type to get the parser for
        //
        //   startIndex:
        //     The start column index of the object (default 0)
        //
        //   length:
        //     The length of columns to read (default -1 = all fields following startIndex)
        //
        //   returnNullIfFirstMissing:
        //     Return null if we can't find the first column? (default false)
        //
        // 返回结果:
        //     A parser for this specific object from this row.
        public static Func<IDataReader, object> GetRowParser(this IDataReader reader, Type type, int startIndex = 0, int length = -1, bool returnNullIfFirstMissing = false);
        //
        // 摘要:
        //     Gets the row parser for a specific row on a data reader. This allows for type
        //     switching every row based on, for example, a TypeId column. You could return
        //     a collection of the base type but have each more specific.
        //
        // 参数:
        //   reader:
        //     The data reader to get the parser for the current row from
        //
        //   concreteType:
        //     The type to get the parser for
        //
        //   startIndex:
        //     The start column index of the object (default 0)
        //
        //   length:
        //     The length of columns to read (default -1 = all fields following startIndex)
        //
        //   returnNullIfFirstMissing:
        //     Return null if we can't find the first column? (default false)
        //
        // 返回结果:
        //     A parser for this specific object from this row.
        public static Func<IDataReader, T> GetRowParser<T>(this IDataReader reader, Type concreteType = null, int startIndex = 0, int length = -1, bool returnNullIfFirstMissing = false);
        //
        // 摘要:
        //     Internal use only
        //
        // 参数:
        //   type:
        //
        //   reader:
        //
        //   startBound:
        //
        //   length:
        //
        //   returnNullIfFirstMissing:
        public static Func<IDataReader, object> GetTypeDeserializer(Type type, IDataReader reader, int startBound = 0, int length = -1, bool returnNullIfFirstMissing = false);
        //
        // 摘要:
        //     Gets type-map for the given type
        //
        // 返回结果:
        //     Type map implementation, DefaultTypeMap instance if no override present
        public static ITypeMap GetTypeMap(Type type);
        //
        // 摘要:
        //     OBSOLETE: For internal usage only. Lookup the DbType and handler for a given
        //     Type and member
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", false)]
        public static DbType LookupDbType(Type type, string name, bool demand, out ITypeHandler handler);
        //
        // 摘要:
        //     Internal use only
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", false)]
        public static void PackListParameters(IDbCommand command, string namePrefix, object value);
        //
        // 摘要:
        //     Parses a data reader to a sequence of data of the supplied type. Used for deserializing
        //     a reader without a connection, etc.
        [IteratorStateMachine(typeof(SqlMapper.< Parse > d__218<>))]
        public static IEnumerable<T> Parse<T>(this IDataReader reader);
        //
        // 摘要:
        //     Parses a data reader to a sequence of dynamic. Used for deserializing a reader
        //     without a connection, etc.
        [IteratorStateMachine(typeof(< Parse > d__220))]
        [return: Dynamic(new[] { false, true })]
        public static IEnumerable<dynamic> Parse(this IDataReader reader);
        //
        // 摘要:
        //     Parses a data reader to a sequence of data of the supplied type (as object).
        //     Used for deserializing a reader without a connection, etc.
        [IteratorStateMachine(typeof(< Parse > d__219))]
        public static IEnumerable<object> Parse(this IDataReader reader, Type type);
        //
        // 摘要:
        //     Purge the query cache
        public static void PurgeQueryCache();
        //
        // 摘要:
        //     Perform a multi mapping query with 6 input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //
        //   commandTimeout:
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TFourth:
        //
        //   TFifth:
        //
        //   TSixth:
        //
        //   TReturn:
        public static IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Perform a multi mapping query with 5 input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //
        //   commandTimeout:
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TFourth:
        //
        //   TFifth:
        //
        //   TReturn:
        public static IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Perform a multi mapping query with 4 input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //
        //   commandTimeout:
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TFourth:
        //
        //   TReturn:
        public static IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Maps a query to objects
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //     The Field we should split and read the second object from (default: id)
        //
        //   commandTimeout:
        //     Number of seconds before command execution timeout
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TReturn:
        public static IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Return a sequence of dynamic objects with properties matching the columns
        //
        // 备注:
        //     Note: each row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, true })]
        public static IEnumerable<dynamic> Query(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Maps a query to objects
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //     The Field we should split and read the second object from (default: id)
        //
        //   commandTimeout:
        //     Number of seconds before command execution timeout
        //
        //   commandType:
        //     Is it a stored proc or a batch?
        //
        // 类型参数:
        //   TFirst:
        //     The first type in the record set
        //
        //   TSecond:
        //     The second type in the record set
        //
        //   TReturn:
        //     The return type
        public static IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a query, returning the data typed as per T
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        //
        // 备注:
        //     the dynamic param may seem a bit odd, but this works around a major usability
        //     issue in vs, if it is Object vs completion gets annoying. Eg type new [space]
        //     get new object
        public static IEnumerable<T> Query<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Perform a multi mapping query with arbitrary input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   types:
        //     array of types in the record set
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //     The Field we should split and read the second object from (default: id)
        //
        //   commandTimeout:
        //     Number of seconds before command execution timeout
        //
        //   commandType:
        //     Is it a stored proc or a batch?
        //
        // 类型参数:
        //   TReturn:
        //     The return type
        public static IEnumerable<TReturn> Query<TReturn>(this IDbConnection cnn, string sql, Type[] types, Func<object[], TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a query, returning the data typed as per T
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static IEnumerable<T> Query<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Perform a multi mapping query with 7 input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //
        //   commandTimeout:
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TFourth:
        //
        //   TFifth:
        //
        //   TSixth:
        //
        //   TSeventh:
        //
        //   TReturn:
        public static IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per the Type suggested
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static IEnumerable<object> Query(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a query asynchronously using .NET 4.5 Task.
        //
        // 备注:
        //     Note: each row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, false, true })]
        public static Task<IEnumerable<dynamic>> QueryAsync(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Maps a query to objects
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //     The field we should split and read the second object from (default: id)
        //
        //   commandTimeout:
        //     Number of seconds before command execution timeout
        //
        //   commandType:
        //     Is it a stored proc or a batch?
        //
        // 类型参数:
        //   TFirst:
        //     The first type in the recordset
        //
        //   TSecond:
        //     The second type in the recordset
        //
        //   TReturn:
        //     The return type
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Maps a query to objects
        //
        // 参数:
        //   cnn:
        //
        //   splitOn:
        //     The field we should split and read the second object from (default: id)
        //
        //   command:
        //     The command to execute
        //
        //   map:
        //
        // 类型参数:
        //   TFirst:
        //     The first type in the recordset
        //
        //   TSecond:
        //     The second type in the recordset
        //
        //   TReturn:
        //     The return type
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(this IDbConnection cnn, CommandDefinition command, Func<TFirst, TSecond, TReturn> map, string splitOn = "Id");
        //
        // 摘要:
        //     Maps a query to objects
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //     The Field we should split and read the second object from (default: id)
        //
        //   commandTimeout:
        //     Number of seconds before command execution timeout
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TReturn:
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Maps a query to objects
        //
        // 参数:
        //   cnn:
        //
        //   splitOn:
        //     The field we should split and read the second object from (default: id)
        //
        //   command:
        //     The command to execute
        //
        //   map:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TReturn:
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(this IDbConnection cnn, CommandDefinition command, Func<TFirst, TSecond, TThird, TReturn> map, string splitOn = "Id");
        //
        // 摘要:
        //     Perform a multi mapping query with 4 input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //
        //   commandTimeout:
        //
        //   commandType:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TFourth:
        //
        //   TReturn:
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Perform a multi mapping query with 4 input parameters
        //
        // 参数:
        //   cnn:
        //
        //   splitOn:
        //     The field we should split and read the second object from (default: id)
        //
        //   command:
        //     The command to execute
        //
        //   map:
        //
        // 类型参数:
        //   TFirst:
        //
        //   TSecond:
        //
        //   TThird:
        //
        //   TFourth:
        //
        //   TReturn:
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(this IDbConnection cnn, CommandDefinition command, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, string splitOn = "Id");
        //
        // 摘要:
        //     Execute a query asynchronously using .NET 4.5 Task.
        //
        // 备注:
        //     Note: each row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, false, true })]
        public static Task<IEnumerable<dynamic>> QueryAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Perform a multi mapping query with 5 input parameters
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(this IDbConnection cnn, CommandDefinition command, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, string splitOn = "Id");
        //
        // 摘要:
        //     Perform a multi mapping query with 5 input parameters
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Perform a multi mapping query with 6 input parameters
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(this IDbConnection cnn, CommandDefinition command, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, string splitOn = "Id");
        //
        // 摘要:
        //     Perform a multi mapping query with 7 input parameters
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Perform a multi mapping query with 7 input parameters
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(this IDbConnection cnn, CommandDefinition command, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, string splitOn = "Id");
        //
        // 摘要:
        //     Perform a multi mapping query with arbitrary input parameters
        //
        // 参数:
        //   cnn:
        //
        //   sql:
        //
        //   types:
        //     array of types in the recordset
        //
        //   map:
        //
        //   param:
        //
        //   transaction:
        //
        //   buffered:
        //
        //   splitOn:
        //     The Field we should split and read the second object from (default: id)
        //
        //   commandTimeout:
        //     Number of seconds before command execution timeout
        //
        //   commandType:
        //     Is it a stored proc or a batch?
        //
        // 类型参数:
        //   TReturn:
        //     The return type
        public static Task<IEnumerable<TReturn>> QueryAsync<TReturn>(this IDbConnection cnn, string sql, Type[] types, Func<object[], TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a query asynchronously using .NET 4.5 Task.
        public static Task<IEnumerable<object>> QueryAsync(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a query asynchronously using .NET 4.5 Task.
        public static Task<IEnumerable<object>> QueryAsync(this IDbConnection cnn, Type type, CommandDefinition command);
        //
        // 摘要:
        //     Execute a query asynchronously using .NET 4.5 Task.
        public static Task<IEnumerable<T>> QueryAsync<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Perform a multi mapping query with 6 input parameters
        public static Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(this IDbConnection cnn, string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a query asynchronously using .NET 4.5 Task.
        public static Task<IEnumerable<T>> QueryAsync<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Return a dynamic object with properties matching the columns
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic]
        public static dynamic QueryFirst(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a query, returning the data typed as per T
        //
        // 返回结果:
        //     A single instance or null of the supplied type; if a basic type (int, string,
        //     etc) is queried then the data from the first column in assumed, otherwise an
        //     instance is created per row, and a direct column-name===member-name mapping is
        //     assumed (case insensitive).
        //
        // 备注:
        //     the dynamic param may seem a bit odd, but this works around a major usability
        //     issue in vs, if it is Object vs completion gets annoying. Eg type new [space]
        //     get new object
        public static T QueryFirst<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per the Type suggested
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static object QueryFirst(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per T
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static T QueryFirst<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, true })]
        public static Task<dynamic> QueryFirstAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QueryFirstAsync(this IDbConnection cnn, Type type, CommandDefinition command);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QueryFirstAsync(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<T> QueryFirstAsync<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per T
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static T QueryFirstOrDefault<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per the Type suggested
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static object QueryFirstOrDefault(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a query, returning the data typed as per T
        //
        // 返回结果:
        //     A single or null instance of the supplied type; if a basic type (int, string,
        //     etc) is queried then the data from the first column in assumed, otherwise an
        //     instance is created per row, and a direct column-name===member-name mapping is
        //     assumed (case insensitive).
        //
        // 备注:
        //     the dynamic param may seem a bit odd, but this works around a major usability
        //     issue in vs, if it is Object vs completion gets annoying. Eg type new [space]
        //     get new object
        public static T QueryFirstOrDefault<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Return a dynamic object with properties matching the columns
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic]
        public static dynamic QueryFirstOrDefault(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QueryFirstOrDefaultAsync(this IDbConnection cnn, Type type, CommandDefinition command);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QueryFirstOrDefaultAsync(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<T> QueryFirstOrDefaultAsync<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, true })]
        public static Task<dynamic> QueryFirstOrDefaultAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a command that returns multiple result sets, and access each in turn
        public static GridReader QueryMultiple(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a command that returns multiple result sets, and access each in turn
        public static GridReader QueryMultiple(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a command that returns multiple result sets, and access each in turn
        [AsyncStateMachine(typeof(< QueryMultipleAsync > d__47))]
        public static Task<GridReader> QueryMultipleAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a command that returns multiple result sets, and access each in turn
        public static Task<GridReader> QueryMultipleAsync(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a query, returning the data typed as per T
        //
        // 返回结果:
        //     A single instance of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        //
        // 备注:
        //     the dynamic param may seem a bit odd, but this works around a major usability
        //     issue in vs, if it is Object vs completion gets annoying. Eg type new [space]
        //     get new object
        public static T QuerySingle<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Return a dynamic object with properties matching the columns
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic]
        public static dynamic QuerySingle(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per the Type suggested
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static object QuerySingle(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per T
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static T QuerySingle<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, true })]
        public static Task<dynamic> QuerySingleAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<T> QuerySingleAsync<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QuerySingleAsync(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QuerySingleAsync(this IDbConnection cnn, Type type, CommandDefinition command);
        //
        // 摘要:
        //     Executes a query, returning the data typed as per T
        //
        // 返回结果:
        //     A single instance of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        //
        // 备注:
        //     the dynamic param may seem a bit odd, but this works around a major usability
        //     issue in vs, if it is Object vs completion gets annoying. Eg type new [space]
        //     get new object
        public static T QuerySingleOrDefault<T>(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per T
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static T QuerySingleOrDefault<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Return a dynamic object with properties matching the columns
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic]
        public static dynamic QuerySingleOrDefault(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Executes a single-row query, returning the data typed as per the Type suggested
        //
        // 返回结果:
        //     A sequence of data of the supplied type; if a basic type (int, string, etc) is
        //     queried then the data from the first column in assumed, otherwise an instance
        //     is created per row, and a direct column-name===member-name mapping is assumed
        //     (case insensitive).
        public static object QuerySingleOrDefault(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QuerySingleOrDefaultAsync(this IDbConnection cnn, Type type, CommandDefinition command);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        //
        // 备注:
        //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
        [return: Dynamic(new[] { false, true })]
        public static Task<dynamic> QuerySingleOrDefaultAsync(this IDbConnection cnn, CommandDefinition command);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<T> QuerySingleOrDefaultAsync<T>(this IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Execute a single-row query asynchronously using .NET 4.5 Task.
        public static Task<object> QuerySingleOrDefaultAsync(this IDbConnection cnn, Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null);
        //
        // 摘要:
        //     Internal use only
        //
        // 参数:
        //   value:
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", false)]
        public static char ReadChar(object value);
        //
        // 摘要:
        //     Internal use only
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", false)]
        public static char? ReadNullableChar(object value);
        //
        // 摘要:
        //     Replace all literal tokens with their text form
        public static void ReplaceLiterals(this IParameterLookup parameters, IDbCommand command);
        //
        // 摘要:
        //     Clear the registered type handlers
        public static void ResetTypeHandlers();
        //
        // 摘要:
        //     OBSOLETE: For internal usage only. Sanitizes the paramter value with proper type
        //     casting.
        [Obsolete("This method is for internal use only", false)]
        public static object SanitizeParameterValue(object value);
        //
        // 摘要:
        //     Set custom mapping for type deserializers
        //
        // 参数:
        //   type:
        //     Entity type to override
        //
        //   map:
        //     Mapping rules impementation, null to remove custom map
        public static void SetTypeMap(Type type, ITypeMap map);
        //
        // 摘要:
        //     Throws a data exception, only used internally
        [Obsolete("This method is for internal use only", false)]
        public static void ThrowDataException(Exception ex, int index, IDataReader reader, object value);

        //
        // 摘要:
        //     Implement this interface to change default mapping of reader columns to type
        //     members
        public interface ITypeMap
        {
            //
            // 摘要:
            //     Finds best constructor
            //
            // 参数:
            //   names:
            //     DataReader column names
            //
            //   types:
            //     DataReader column types
            //
            // 返回结果:
            //     Matching constructor or default one
            ConstructorInfo FindConstructor(string[] names, Type[] types);
            //
            // 摘要:
            //     Returns a constructor which should *always* be used. Parameters will be default
            //     values, nulls for reference types and zero'd for value types. Use this class
            //     to force object creation away from parameterless constructors you don't control.
            ConstructorInfo FindExplicitConstructor();
            //
            // 摘要:
            //     Gets mapping for constructor parameter
            //
            // 参数:
            //   constructor:
            //     Constructor to resolve
            //
            //   columnName:
            //     DataReader column name
            //
            // 返回结果:
            //     Mapping implementation
            IMemberMap GetConstructorParameter(ConstructorInfo constructor, string columnName);
            //
            // 摘要:
            //     Gets member mapping for column
            //
            // 参数:
            //   columnName:
            //     DataReader column name
            //
            // 返回结果:
            //     Mapping implementation
            IMemberMap GetMember(string columnName);
        }
        //
        // 摘要:
        //     Implement this interface to perform custom type-based parameter handling and
        //     value parsing
        public interface ITypeHandler
        {
            //
            // 摘要:
            //     Parse a database value back to a typed value
            //
            // 参数:
            //   value:
            //     The value from the database
            //
            //   destinationType:
            //     The type to parse to
            //
            // 返回结果:
            //     The typed value
            object Parse(Type destinationType, object value);
            //
            // 摘要:
            //     Assign the value of a parameter before a command executes
            //
            // 参数:
            //   parameter:
            //     The parameter to configure
            //
            //   value:
            //     Parameter value
            void SetValue(IDbDataParameter parameter, object value);
        }
        //
        // 摘要:
        //     Extends IDynamicParameters providing by-name lookup of parameter values
        [DefaultMember("Item")]
        public interface IParameterLookup : IDynamicParameters
        {
            //
            // 摘要:
            //     Get the value of the specified parameter (return null if not found)
            object this[string name] { get; }
        }
        //
        // 摘要:
        //     Extends IDynamicParameters with facilities for executing callbacks after commands
        //     have completed
        public interface IParameterCallbacks : IDynamicParameters
        {
            //
            // 摘要:
            //     Invoked when the command has executed
            void OnCompleted();
        }
        //
        // 摘要:
        //     Implement this interface to pass an arbitrary db specific parameter to Dapper
        public interface ICustomQueryParameter
        {
            //
            // 摘要:
            //     Add the parameter needed to the command before it executes
            //
            // 参数:
            //   command:
            //     The raw command prior to execution
            //
            //   name:
            //     Parameter name
            void AddParameter(IDbCommand command, string name);
        }
        //
        // 摘要:
        //     Implement this interface to pass an arbitrary db specific set of parameters to
        //     Dapper
        public interface IDynamicParameters
        {
            //
            // 摘要:
            //     Add all the parameters needed to the command just before it executes
            //
            // 参数:
            //   command:
            //     The raw command prior to execution
            //
            //   identity:
            //     Information about the query
            void AddParameters(IDbCommand command, Identity identity);
        }
        //
        // 摘要:
        //     Implements this interface to provide custom member mapping
        public interface IMemberMap
        {
            //
            // 摘要:
            //     Source DataReader column name
            string ColumnName { get; }
            //
            // 摘要:
            //     Target member type
            Type MemberType { get; }
            //
            // 摘要:
            //     Target property
            PropertyInfo Property { get; }
            //
            // 摘要:
            //     Target field
            FieldInfo Field { get; }
            //
            // 摘要:
            //     Target constructor parameter
            ParameterInfo Parameter { get; }
        }

        //
        // 摘要:
        //     Permits specifying certain SqlMapper values globally.
        public static class Settings
        {
            //
            // 摘要:
            //     Specifies the default Command Timeout for all Queries
            public static int? CommandTimeout { get; set; }
            //
            // 摘要:
            //     Indicates whether nulls in data are silently ignored (default) vs actively applied
            //     and assigned to members
            public static bool ApplyNullValues { get; set; }
            //
            // 摘要:
            //     Should list expansions be padded with null-valued parameters, to prevent query-plan
            //     saturation? For example, an 'in @foo' expansion with 7, 8 or 9 values will be
            //     sent as a list of 10 values, with 3, 2 or 1 of them null. The padding size is
            //     relative to the size of the list; "next 10" under 150, "next 50" under 500, "next
            //     100" under 1500, etc.
            //
            // 备注:
            //     Caution: this should be treated with care if your DB provider (or the specific
            //     configuration) allows for null equality (aka "ansi nulls off"), as this may change
            //     the intent of your query; as such, this is disabled by default and must be enabled.
            public static bool PadListExpansions { get; set; }
            //
            // 摘要:
            //     If set (non-negative), when performing in-list expansions of integer types ("where
            //     id in @ids", etc), switch to a string_split based operation if there are more
            //     than this many elements. Note that this feautre requires SQL Server 2016 / compatibility
            //     level 130 (or above).
            public static int InListStringSplitCount { get; set; }

            //
            // 摘要:
            //     Resets all Settings to their default values
            public static void SetDefaults();
        }
        //
        // 摘要:
        //     Not intended for direct usage
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("This method is for internal use only", false)]
        public static class TypeHandlerCache<T>
        {
            //
            // 摘要:
            //     Not intended for direct usage
            [Obsolete("This method is for internal use only", true)]
            public static T Parse(object value);
            //
            // 摘要:
            //     Not intended for direct usage
            [Obsolete("This method is for internal use only", true)]
            public static void SetValue(IDbDataParameter parameter, object value);
        }
        //
        // 摘要:
        //     Identity of a cached query in Dapper, used for extensibility
        public class Identity : IEquatable<Identity>
        {
            //
            // 摘要:
            //     The sql
            public readonly string sql;
            //
            // 摘要:
            //     The command type
            public readonly CommandType? commandType;
            //
            public readonly int hashCode;
            //
            public readonly int gridIndex;
            //
            public readonly Type type;
            //
            public readonly string connectionString;
            //
            public readonly Type parametersType;

            //
            // 参数:
            //   obj:
            public override bool Equals(object obj);
            //
            // 摘要:
            //     Compare 2 Identity objects
            //
            // 参数:
            //   other:
            public bool Equals(Identity other);
            //
            // 摘要:
            //     Create an identity for use with DynamicParameters, internal use only
            //
            // 参数:
            //   type:
            public Identity ForDynamicParameters(Type type);
            //
            public override int GetHashCode();
        }
        //
        // 摘要:
        //     The grid reader provides interfaces for reading multiple result sets from a Dapper
        //     query
        public class GridReader : IDisposable
        {
            //
            // 摘要:
            //     The command associated with the reader
            public IDbCommand Command { get; set; }
            //
            // 摘要:
            //     Has the underlying reader been consumed?
            public bool IsConsumed { get; }

            //
            // 摘要:
            //     Dispose the grid, closing and disposing both the underlying reader and command.
            public void Dispose();
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TReturn>(Type[] types, Func<object[], TReturn> map, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> func, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> func, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> func, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TReturn> func, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TReturn>(Func<TFirst, TSecond, TThird, TReturn> func, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read multiple objects from a single record set on the grid
            public IEnumerable<TReturn> Read<TFirst, TSecond, TReturn>(Func<TFirst, TSecond, TReturn> func, string splitOn = "id", bool buffered = true);
            //
            // 摘要:
            //     Read the next grid of results
            public IEnumerable<object> Read(Type type, bool buffered = true);
            //
            // 摘要:
            //     Read the next grid of results
            public IEnumerable<T> Read<T>(bool buffered = true);
            //
            // 摘要:
            //     Read the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: each row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic(new[] { false, true })]
            public IEnumerable<dynamic> Read(bool buffered = true);
            //
            // 摘要:
            //     Read the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: each row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic(new[] { false, false, true })]
            public Task<IEnumerable<dynamic>> ReadAsync(bool buffered = true);
            //
            // 摘要:
            //     Read the next grid of results
            public Task<IEnumerable<object>> ReadAsync(Type type, bool buffered = true);
            //
            // 摘要:
            //     Read the next grid of results
            public Task<IEnumerable<T>> ReadAsync<T>(bool buffered = true);
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public T ReadFirst<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public object ReadFirst(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic]
            public dynamic ReadFirst();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<T> ReadFirstAsync<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<object> ReadFirstAsync(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic(new[] { false, true })]
            public Task<dynamic> ReadFirstAsync();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public T ReadFirstOrDefault<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public object ReadFirstOrDefault(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic]
            public dynamic ReadFirstOrDefault();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<T> ReadFirstOrDefaultAsync<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic(new[] { false, true })]
            public Task<dynamic> ReadFirstOrDefaultAsync();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<object> ReadFirstOrDefaultAsync(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic]
            public dynamic ReadSingle();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public object ReadSingle(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public T ReadSingle<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<T> ReadSingleAsync<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic(new[] { false, true })]
            public Task<dynamic> ReadSingleAsync();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<object> ReadSingleAsync(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic]
            public dynamic ReadSingleOrDefault();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public object ReadSingleOrDefault(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public T ReadSingleOrDefault<T>();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<object> ReadSingleOrDefaultAsync(Type type);
            //
            // 摘要:
            //     Read an individual row of the next grid of results, returned as a dynamic object
            //
            // 备注:
            //     Note: the row can be accessed via "dynamic", or by casting to an IDictionary<string,object>
            [return: Dynamic(new[] { false, true })]
            public Task<dynamic> ReadSingleOrDefaultAsync();
            //
            // 摘要:
            //     Read an individual row of the next grid of results
            public Task<T> ReadSingleOrDefaultAsync<T>();
        }
        //
        // 摘要:
        //     Base-class for simple type-handlers that are based around strings
        public abstract class StringTypeHandler<T> : TypeHandler<T>
        {
            protected StringTypeHandler();

            //
            // 摘要:
            //     Parse a database value back to a typed value
            //
            // 参数:
            //   value:
            //     The value from the database
            //
            // 返回结果:
            //     The typed value
            public override T Parse(object value);
            //
            // 摘要:
            //     Assign the value of a parameter before a command executes
            //
            // 参数:
            //   parameter:
            //     The parameter to configure
            //
            //   value:
            //     Parameter value
            public override void SetValue(IDbDataParameter parameter, T value);
            //
            // 摘要:
            //     Format an instace into a string (the instance will never be null)
            protected abstract string Format(T xml);
            //
            // 摘要:
            //     Parse a string into the expected type (the string will never be null)
            protected abstract T Parse(string xml);
        }
        //
        // 摘要:
        //     Base-class for simple type-handlers
        public abstract class TypeHandler<T> : ITypeHandler
        {
            protected TypeHandler();

            //
            // 摘要:
            //     Parse a database value back to a typed value
            //
            // 参数:
            //   value:
            //     The value from the database
            //
            // 返回结果:
            //     The typed value
            public abstract T Parse(object value);
            //
            // 摘要:
            //     Assign the value of a parameter before a command executes
            //
            // 参数:
            //   parameter:
            //     The parameter to configure
            //
            //   value:
            //     Parameter value
            public abstract void SetValue(IDbDataParameter parameter, T value);
        }
    }
}
