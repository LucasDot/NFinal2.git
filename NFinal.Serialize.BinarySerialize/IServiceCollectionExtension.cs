﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;

namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Serialize_BinarySerialize_Extension
    {
        /// <summary>
        /// 设置二进制序列化
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void SetSerializeBinary(this IServiceCollection serviceCollection)
        {
#if (NET40 || NET451 || NET461)
            serviceCollection.SetService<NFinal.Serialize.ISerializable>(typeof(NFinal.Serialize.BinarySerialize.BinarySerialize));
#endif
        }
    }
}
