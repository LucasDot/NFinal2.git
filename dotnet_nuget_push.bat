@echo off
set k=4eeeecaa-98da-4e88-9114-8c9b795078fd
set s=https://api.nuget.org/v3/index.json
set c=Debug
echo ��ʼ
dotnet nuget push NFinal/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Cache.RedisCache/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Cache.SimpleCache/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Collections/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Common/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Data/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.DependencyInjection/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Http.Session/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Json.Jil/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Json.NetJSON/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Json.Newtonsoft/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Json.SimpleJson/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Json.TinyJson/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Logs.NLogger/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Mapper/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.NET20Extension/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.OpenApi/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.ORM/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.ORM.Dapper/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Serialize.BinarySerialize/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Serialize.NetJSON/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Serialize.ProtobufSerialize/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Serialize.WireSerializable/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.Validation/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.View.DotLiquid/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.View.JNTemplate/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.View.Markdown/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.View.Nustache/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.View.Razor/bin/%c%/ -k %k% -s %s%
dotnet nuget push NFinal.View.Spark/bin/%c%/ -k %k% -s %s%
echo ����
pause