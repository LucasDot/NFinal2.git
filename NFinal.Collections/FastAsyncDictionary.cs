﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace NFinal.Collections
{
    /// <summary>
    /// 获取字典的值的代理
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TParam"></typeparam>
    /// <param name="param"></param>
    /// <returns></returns>
    public delegate TValue GetDictionaryValue<TValue, TParam>(TParam param);
     
    /// <summary>
    /// 多线程的快速字典类
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class FastAsyncDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        private FastDictionary<TKey, TValue> innerDictionary = null;
#if !NET20
        private ReaderWriterLockSlim readWriteLock;
#endif
        private const int timeout = 1000;
        /// <summary>
        /// 多线程的获取和生成value值,适用于生成value值比较耗时的操作
        /// </summary>
        /// <typeparam name="TParam"></typeparam>
        /// <param name="key"></param>
        /// <param name="getValueDelegate"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public TValue GetOrAddValue<TParam>(TKey key, GetDictionaryValue<TValue, TParam> getValueDelegate,TParam param)
        {
            TValue value = default(TValue);
#if NET20
            lock (innerDictionary)
            {
                if (!innerDictionary.TryGetValue(key, out value))
                {
                    value = getValueDelegate(param);
                    innerDictionary[key] = value;
                }
            }
#else
            if (!innerDictionary.TryGetValue(key, out value))
            {
                value = getValueDelegate(param);
                readWriteLock.EnterWriteLock();
                try
                {
                    innerDictionary[key] = value;
                }
                finally
                {
                    readWriteLock.ExitWriteLock();
                }
            }
#endif
            return value;
        }
        /// <summary>
        /// 元素访问
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[TKey key]
        {
            get {
#if NET20
                lock (innerDictionary)
                {
                    TValue val = innerDictionary[key];
                    return val;
                }
#else
                if (readWriteLock.TryEnterReadLock(timeout))
                {
                    TValue val= innerDictionary[key];
                    readWriteLock.ExitReadLock();
                    return val;
                }
                return default(TValue);
#endif
            }
            set
            {
#if NET20
                lock (innerDictionary)
                {
                    innerDictionary[key] = value;
                }
#else
                if (readWriteLock.TryEnterWriteLock(timeout))
                {
                    innerDictionary[key] = value;
                    readWriteLock.ExitWriteLock();
                }
#endif
            }
        }
        public FastAsyncDictionary(IEqualityComparer<TKey> comparer)
        {
            this.innerDictionary = new FastDictionary<TKey, TValue>(comparer);
#if !NET20
            this.readWriteLock = new ReaderWriterLockSlim();
#endif
        }
        /// <summary>
        /// 字典初始化
        /// </summary>
        public FastAsyncDictionary()
        {
            this.innerDictionary = new FastDictionary<TKey, TValue>();
#if !NET20
            this.readWriteLock = new ReaderWriterLockSlim();
#endif
        }
        /// <summary>
        /// 添加元素
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value)
        {
#if NET20
            lock (innerDictionary)
            {
                this.innerDictionary[key] = value;
            }
#else
            if (readWriteLock.TryEnterWriteLock(timeout))
            {
                this.innerDictionary[key] = value;
                readWriteLock.ExitWriteLock();
            }
#endif
        }
        /// <summary>
        /// 清除所有元素
        /// </summary>
        public void Clear()
        {
#if NET20
            lock (innerDictionary)
            {
                innerDictionary.Clear();
            }
#else
            if (readWriteLock.TryEnterWriteLock(timeout))
            {
                this.innerDictionary.Clear();
                readWriteLock.ExitWriteLock();
            }
#endif
        }
        /// <summary>
        /// 查找元素
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Contains(TKey key)
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.Contains(key);
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                bool result= this.innerDictionary.Contains(key);
                readWriteLock.ExitReadLock();
                return result;
            }
            return false;
#endif
        }
        /// <summary>
        /// 查找元素
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.ContainsKey(key);
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                bool result= this.innerDictionary.Contains(key);
                readWriteLock.ExitReadLock();
                return result;
            }
            return false;
#endif
        }
        /// <summary>
        /// 查找元素
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ContainsValue(TValue value)
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.ContainsValue(value);
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                bool result = this.innerDictionary.ContainsValue(value);
                readWriteLock.ExitReadLock();
                return result;
            }
            return false;
#endif
        }
        /// <summary>
        /// 获取元素
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.TryGetValue(key, out value);
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                bool result = this.innerDictionary.TryGetValue(key, out value);
                readWriteLock.ExitReadLock();
                return result;
            }
            value = default(TValue);
            return false;
#endif
        }
        /// <summary>
        /// 元素数量
        /// </summary>
        public int Count
        {
            get
            {
#if NET20
                lock (innerDictionary)
                {
                    return innerDictionary.Count;
                }
#else
                if (readWriteLock.TryEnterReadLock(timeout))
                {
                    int count = this.innerDictionary.Count;
                    readWriteLock.ExitReadLock();
                    return count;
                }
                return -1;
#endif
            }
        }
        /// <summary>
        /// 移除元素
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(TKey key)
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.Remove(key);
            }
#else
            if (readWriteLock.TryEnterWriteLock(timeout))
            {
                bool result = this.innerDictionary.Remove(key);
                readWriteLock.ExitWriteLock();
                return result;
            }
            return false;
#endif
        }
        /// <summary>
        /// 复制元素
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
        {
#if NET20
            lock (innerDictionary)
            {
                innerDictionary.CopyTo(array, index);
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                this.innerDictionary.CopyTo(array, index);
                readWriteLock.ExitReadLock();
            }
#endif
        }
        public int Capacity
        {
            get
            {
                return this.innerDictionary.Capacity;
            }
        }
        public IEqualityComparer<TKey> Comparer
        {
            get
            {
                return this.innerDictionary.Comparer;
            }
        }
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.GetEnumerator();
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                IEnumerator<KeyValuePair<TKey, TValue>> enumerator= this.innerDictionary.GetEnumerator();
                readWriteLock.ExitReadLock();
                return enumerator;
            }
            return null;
#endif
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
#if NET20
            lock (innerDictionary)
            {
                return innerDictionary.GetEnumerator();
            }
#else
            if (readWriteLock.TryEnterReadLock(timeout))
            {
                IEnumerator enumerator = this.innerDictionary.GetEnumerator();
                readWriteLock.ExitReadLock();
                return enumerator;
            }
            return null;
#endif
        }
        /// <summary>
        /// 析构释放对象时，把读写锁释放掉
        /// </summary>
        ~FastAsyncDictionary()
        {
#if !NET20
            if (readWriteLock != null) readWriteLock.Dispose();
#endif
        }
    }
}
