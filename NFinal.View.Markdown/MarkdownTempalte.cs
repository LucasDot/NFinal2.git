﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NFinal.IO;
using MarkdownSharpEx = HeyRed.MarkdownSharp;
namespace NFinal.View.Markdown
{
    public class MarkdownTempalte:NFinal.View.ITemplate
    {
        public static NFinal.Logs.ILogger logger;
        public static void Configure(NFinal.Logs.ILogger logger)
        {
            MarkdownTempalte.logger = logger;
        }
        public string RootPath { get; set; }
        private MarkdownSharpEx.Markdown markdown;
        private MarkdownSharpEx.MarkdownOptions options;
        public string Extension { get { return ".htm"; } }
        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            string path = controllerFullName.Substring(assemblyName.Length);
            path = path.Replace('.', '/');
            if (string.IsNullOrEmpty(skinName))
            {
                path = "/" + assemblyName + "/" + path + "/" + methodName + Extension;
            }
            else
            {
                path = skinName + "/" + path + "/" + methodName + Extension;
            }
            return path;
        }
        public string Render(Writer writer, string content, object model=null)
        {
            MarkdownSharpEx.Markdown markdonw = new MarkdownSharpEx.Markdown();
            return markdonw.Transform(content);
        }
        public MarkdownTempalte(string rootPath, object options)
        {
            this.options = (MarkdownSharpEx.MarkdownOptions)options;
            this.markdown = new MarkdownSharpEx.Markdown(this.options);
            this.RootPath = rootPath;
        }

        public Task RenderAsync(Writer writer, string url, object model)
        {
            throw new NotImplementedException();
        }
    }
}
