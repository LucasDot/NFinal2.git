﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using NFinal;

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinalPlugTest.Views
{
    /// <summary>
    /// 模板类
    /// </summary>
    [View("/NFinalPlugTest/Views/Index.cshtml")]
    public class Index_cshtml : NFinal.View.RazorView<NFinalPlugTest.Controllers.IndexController_Model.Show>
    {
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">数据</param>
        public Index_cshtml(NFinal.IO.Writer writer, object model) : base(writer, model)
        {
        }
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            this.ExecuteAsync().Wait();
        }
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
        {
            await writer.WriteAsync("");
            await writer.WriteAsync("<!DOCTYPE html>\r\n<html>\r\n<head>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n    <title></title>\r\n\t<meta http-equiv=\"Cache-Control\" content=\"no-cache\">\r\n</head>\r\n<body>\r\n    ");
            await writer.WriteAsync(Model.a);
            await writer.WriteAsync("\r\n    ");
            await writer.WriteAsync(Model.cc2);
            await writer.WriteAsync("\r\n    你在哪？ffffdddddd\r\n</body>\r\n</html>");
        }
    }
}
