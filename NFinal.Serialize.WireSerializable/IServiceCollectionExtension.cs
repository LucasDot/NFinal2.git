﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Serialize_WireSerializable_Extension
    {
        /// <summary>
        /// 设置ProtobufSerialize
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void SetSerializeWireSerializable(this IServiceCollection serviceCollection)
        {
            serviceCollection.SetService<NFinal.Serialize.ISerializable>(typeof(NFinal.Serialize.WireSerializable.WireSerializable));
        }
    }
}
