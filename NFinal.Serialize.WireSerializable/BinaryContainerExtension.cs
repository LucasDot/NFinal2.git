﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Extension
{
    /// <summary>
    /// 字符串转任意基础类型
    /// </summary>
    public static class BinaryContainerExtension
    {
        /// <summary>
        /// 把任意对象转为BinaryContainer
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static BinaryContainer AsBytes(this object obj)
        {
            byte[] buffer = null;
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                string str= NetJSON.NetJSON.Serialize(obj);
                buffer = Application.encoding.GetBytes(str);
            }
            return new BinaryContainer(buffer);
        }
        /// <summary>
        /// 把BinaryContainer转为任意对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="BinaryContainer"></param>
        /// <returns></returns>
        public static T As<T>(this BinaryContainer BinaryContainer)
        {
            T t = default(T);
            if (BinaryContainer.value != null)
            {
                byte[] buffer = BinaryContainer.value;
                using (System.IO.Stream stream = buffer.ToStream())
                {
                    string str = Application.encoding.GetString(buffer);
                    object obj = NetJSON.NetJSON.Deserialize<T>(str);
                    t = (T)obj;
                    return t;
                }
            }
            return t;
        }
        /// <summary>
        /// 把任意对象转为BinaryContainer
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
		public static BinaryContainer AsBinaryContainer(this object obj)
        {
            Wire.Serializer serializer = new Wire.Serializer(new Wire.SerializerOptions(true, true));
            byte[] buffer = null;
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                serializer.Serialize(obj, stream);
                buffer = stream.ToArray();
            }
            return new BinaryContainer(buffer);
        }
        /// <summary>
        /// 把BinaryContainer转为任意对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="BinaryContainer"></param>
        /// <returns></returns>
        public static T As<T>(this BinaryContainer BinaryContainer) where T : class
        {
            if (BinaryContainer.value != null)
            {
                Wire.Serializer serializer = new Wire.Serializer(new Wire.SerializerOptions(true, true));
                byte[] buffer = BinaryContainer.value;
                T t = null;
                using (System.IO.Stream stream = buffer.ToStream())
                {
                    object obj = serializer.Deserialize(stream);
                    t = (T)obj;
                    return t;
                }
            }
            return null;
        }
    }
}
