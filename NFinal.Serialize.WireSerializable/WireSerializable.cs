﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Serialize.WireSerializable
{
#if !NET40
    public class WireSerializable : ISerializable
    {
        public T Deserialize<T>(byte[] content)
        {
            Wire.Serializer serializer = new Wire.Serializer(new Wire.SerializerOptions(true, true));
            T t = default(T);
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                t= serializer.Deserialize<T>(stream);
            }
            return t;
        }

        public byte[] Serialize<T>(T t)
        {
            Wire.Serializer serializer = new Wire.Serializer(new Wire.SerializerOptions(true, true));
            byte[] buffer = null;
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                serializer.Serialize(t, stream);
                buffer = stream.ToArray();
            }
            return buffer;
        }
    }
#endif
}
