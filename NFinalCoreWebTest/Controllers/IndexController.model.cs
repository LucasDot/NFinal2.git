﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NFinal;
//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinalCoreWebTest.Controllers.IndexController_Model
{
	public class Index
	{
		[NFinal.ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public NFinal.Config.Plug.PlugConfig config;
		[NFinal.ViewBagMember]
		[Newtonsoft.Json.JsonIgnore]
		public NFinal.NameValueCollection parameters;
	}
}
