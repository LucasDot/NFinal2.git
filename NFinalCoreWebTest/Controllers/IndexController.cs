﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NFinal;

namespace NFinalCoreWebTest.Controllers
{
    public class IndexController:BaseController
    {
        public async Task<ActionResult> Index()
        {
            this.Write("Hello World Async!");
            return Json();
        }
    }
}
 