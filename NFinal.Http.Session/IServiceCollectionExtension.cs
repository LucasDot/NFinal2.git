﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Http_Session_Extension
    {
        public static void SetSession(this IServiceCollection serviceCollection,string userSessionKey,bool rewrite=true)
        {
            serviceCollection.SetService<NFinal.Http.ISession, string, NFinal.Cache.ICache<string>>
                (typeof(NFinal.Http.Session.Session),rewrite).Configure(userSessionKey);
        }
        private static Func<string, NFinal.Cache.ICache<string>, NFinal.Http.ISession> sessionOrigionConstructor;
        public static Func<string, NFinal.Http.ISession> sessionConstructor;
        public static NFinal.Cache.ICache<string> cache;
        public static Func<string,NFinal.Http.ISession> GetGetSessionMethod(this IServiceCollection serviceCollection)
        {
            sessionOrigionConstructor = serviceCollection.GetServiceConstructor<NFinal.Http.ISession, string, NFinal.Cache.ICache<string>, NFinal.Http.ISession>();
            var serialize=  serviceCollection.GetService<NFinal.Serialize.ISerializable>();
            cache = serviceCollection.GetService<NFinal.Cache.ICache<string>,NFinal.Serialize.ISerializable>("session", serialize);
            return GetSession;
        }
        public static NFinal.Http.ISession GetSession(string key)
        {
            return sessionOrigionConstructor(key, cache);
        }
    }
}
