﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Serialize_NetJSON_Extension
    {
        /// <summary>
        /// 设置ProtobufSerialize
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void SetSerializeNetJSON(this IServiceCollection serviceCollection)
        {
            serviceCollection.SetService<NFinal.Serialize.ISerializable>(typeof(NFinal.Serialize.NetJSON.NetJSONSerialize));
        }
    }
}
