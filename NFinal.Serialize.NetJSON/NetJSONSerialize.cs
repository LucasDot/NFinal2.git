﻿using System;
using System.Collections.Generic;
using System.Text;
using NetJSONEx = NetJSON;
namespace NFinal.Serialize.NetJSON
{
#if !NET40
    public class NetJSONSerialize : ISerializable
    {
        public static System.Text.Encoding encoding = new System.Text.UTF8Encoding(false);
        public T Deserialize<T>(byte[] content)
        {
            T t = default(T);
            string str = encoding.GetString(content);
            t = NetJSONEx.NetJSON.Deserialize<T>(str);
            return t;
        }

        public byte[] Serialize<T>(T t)
        {
            byte[] buffer = null;
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                string str = NetJSONEx.NetJSON.Serialize(t);
                buffer = encoding.GetBytes(str);
            }
            return buffer;
        }
    }
#endif
}
