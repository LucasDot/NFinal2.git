﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Data
{
    public interface IDataProvider
    {
        object GetData(params NFinal.StringContainer[] options);
    }
    public class ProviderExecutor
    {
        public string providerPath;
        public NFinal.StringContainer[] parameters;
        public Type resultType;
        public Func<NFinal.StringContainer[], object> executeDelegate;
    }
    public class DataResult
    {
        public object instance;
        public Type type;
    }
    public class BaseDataProvider :IDataProvider
    {

        public dynamic ViewBag = null;

        public object GetData(params NFinal.StringContainer[] options)
        {
            int a= options[0];
            string b = options[1];
            //this.ViewBag.Id = 1;
            //this.ViewBag.Name = 2;
            //return ViewBag;
            return null;
        }
    }
}
