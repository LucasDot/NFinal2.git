﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SparkEx = Spark;
using NFinal.IO;
using System.Threading.Tasks;

namespace NFinal.View.Spark
{
    public class SparkTemplate : NFinal.View.ITemplate
    {
        public static NFinal.Logs.ILogger logger;
        public static void Configure(NFinal.Logs.ILogger logger)
        {
            SparkTemplate.logger = logger;
        }
        public string Extension { get { return ".spark"; } }
        public string RootPath { get; set; }
        private SparkEx.SparkSettings options;
        public SparkTemplate(string rootPath, object options)
        {
            this.RootPath = rootPath;
            this.options = (SparkEx.SparkSettings)options;
            this.options.SetPageBaseType(typeof(TemplateBase));
        }

        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            string path = controllerFullName.Substring(assemblyName.Length);
            path = path.Replace('.', '/');
            if (string.IsNullOrEmpty(skinName))
            {
                path = "/" + assemblyName + "/" + path + "/" + methodName + Extension;
            }
            else
            {
                path = skinName + "/" + path + "/" + methodName + Extension;
            }
            return path;
        }
        /// <summary>
        /// 获取绝对路径
        /// </summary>
        /// <param name="absoluteDirectory">绝对根目录</param>
        /// <param name="relativePath">相对路径</param>
        /// <returns></returns>
        public static string GetAbsolutePath(string absoluteDirectory, string relativePath)
        {
            //relativePath= relativePath.Trim('/');
            string[] relativeDirectories = relativePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string absolutePath = absoluteDirectory;
            for (int i = 0; i < relativeDirectories.Length; i++)
            {
                if (relativeDirectories[i] == "..")
                {
                    absolutePath = System.IO.Path.GetDirectoryName(absolutePath);
                }
                else if (relativeDirectories[i] == ".")
                {
                    absolutePath = absoluteDirectory;
                }
                else if (relativeDirectories[i] == "")
                {
                    absolutePath = absoluteDirectory;
                }
                else
                {
                    absolutePath = System.IO.Path.Combine(absolutePath, relativeDirectories[i]);
                }
            }
            return absolutePath;
        }

        public Task RenderAsync(Writer writer, string url, object model)
        {

            string content= Render(writer,url,model);
            return writer.WriteAsync(content);
        }

        public string Render(Writer writer, string url, object model)
        {
            SparkEx.SparkViewEngine engine = new SparkEx.SparkViewEngine(options);
            string fileName = null;
            var descriptor = new SparkEx.SparkViewDescriptor().AddTemplate(fileName);
            var view = (TemplateBase)engine.CreateInstance(descriptor);
            StringWriter sw = new StringWriter();
            try
            {
                view.ViewBag = model;
                view.RenderView(sw);
                return sw.ToString();
            }
            finally
            {
                engine.ReleaseInstance(view);
                sw.Dispose();
            }
        }
    }
    public abstract class TemplateBase : SparkEx.AbstractSparkView
    {
        public dynamic ViewBag { get; set; }
    }

}
