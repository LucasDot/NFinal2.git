@echo off
call Build.bat
echo 开始复制.net core 1.1生成文件
copy /y NFinal\bin\Debug\netstandard1.6\NFinal.dll Nuget\lib\netcoreapp1.1
copy /y NFinal\bin\Debug\netstandard1.6\NFinal.xml Nuget\lib\netcoreapp1.1
echo ======================================================================
echo 开始复制.net standard 1.6生成文件
copy /y NFinal\bin\Debug\netstandard1.6\NFinal.dll Nuget\lib\netstandard1.6
copy /y NFinal\bin\Debug\netstandard1.6\NFinal.xml Nuget\lib\netstandard1.6
echo ======================================================================
echo 开始复制.net framework 4.0生成文件
copy /y NFinal4\bin\Debug\NFinal.dll Nuget\lib\net40-client
copy /y NFinal4\bin\Debug\NFinal.xml Nuget\lib\net40-client
echo ======================================================================
echo 开始复制.net framework 4.5生成文件
copy /y NFinal45\bin\Debug\NFinal.dll Nuget\lib\net45
copy /y NFinal45\bin\Debug\NFinal.xml Nuget\lib\net45
echo ======================================================================
echo 开始复制.net framework 4.6生成文件
copy /y NFinal46\bin\Debug\NFinal.dll Nuget\lib\net46
copy /y NFinal46\bin\Debug\NFinal.xml Nuget\lib\net46
echo ======================================================================
echo 开始nuget打包
Nuget pack Nuget\NFinal2.nuspec
pause
exit