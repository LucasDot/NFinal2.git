﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;

namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Json_Jil_Extension
    {
        public static void SetJsonJil(this IServiceCollection serviceCollection)
        {
            serviceCollection.SetService<NFinal.Json.IJsonSerialize>(typeof(NFinal.Json.Jil.Jil));
        }
    }
}
