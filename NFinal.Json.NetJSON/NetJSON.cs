﻿using System;
using System.Collections.Generic;
using System.Text;
using NetJSONEx = NetJSON;
namespace NFinal.Json.NetJSON
{
    /// <summary>
    /// 基于Newtonsoft的Json序列化类
    /// </summary>
    public class NetJSON : IJsonSerialize
    {
        /// <summary>
        /// 把Json字符串转为object
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public object DeserializeObject(string json)
        {
            return NetJSONEx.NetJSON.DeserializeObject(json);
        }
        /// <summary>
        /// 把Json字符串转为自定义类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public T DeserializeObject<T>(string json)
        {
            return NetJSONEx.NetJSON.Deserialize<T>(json);
        }
        /// <summary>
        /// 把object类型转为json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string SerializeObject(object obj)
        {
            return NetJSONEx.NetJSON.Serialize(obj);
        }
    }
}
