﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Json.TinyJson
{
    /// <summary>
    /// 基于Newtonsoft的Json序列化类
    /// </summary>
    public class TinyJson : IJsonSerialize
    {
        /// <summary>
        /// 把Json字符串转为object
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public object DeserializeObject(string json)
        {
            return JSONParser.ParseValue(typeof(object),json);
        }
        /// <summary>
        /// 把Json字符串转为自定义类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public T DeserializeObject<T>(string json)
        {
            return (T)JSONParser.ParseValue(typeof(T),json);
        }
        /// <summary>
        /// 把object类型转为json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string SerializeObject(object obj)
        {
            return JSONWriter.ToJson(obj);
        }
    }
}
