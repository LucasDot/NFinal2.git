﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.UI
{
    public interface IControl
    {
        NFinal.View.IView Render();
        NFinal.View.IView Render<T>(string ViewPath, Type inputType, T ViewBag);
    }
}
