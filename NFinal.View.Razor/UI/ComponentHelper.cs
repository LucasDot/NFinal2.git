﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;

namespace NFinal.UI
{
    public delegate NFinal.View.IView InvokeComponentDelegate(string url, object model);
    public class ComponentInvokeData
    {
        public RuntimeTypeHandle componentType;
        public string compnoentFullName;
        public InvokeComponentDelegate invokeComponentDelegate;
    }
    public class ComponentHelper
    {
        public static NFinal.Collections.FastDictionary<string, ComponentInvokeData> componentInvokeDelegateDictionary = new Collections.FastDictionary<string, ComponentInvokeData>();
        public void Init(Assembly[] assemblys)
        {
            foreach (var assembly in assemblys)
            {
                Type[] types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.FullName.EndsWith("Component"))
                    {
                        if (typeof(NFinal.UI.IComponent).IsAssignableFrom(type))
                        {
                            NFinal.UI.IComponent component = null;
                            
                        }
                    }
                }
            }
        }

        public static InvokeComponentDelegate GenerateGetInvokeComponentDelegate(string url, object model)
        {
            //DynamicMethod dynamicMethod = new DynamicMethod("InvokeComponentX", typeof(NFinal.UI.IComponent), typeof(NFinal.IO.Writer), typeof(string), typeof(model))
            return null;
        }
       
    }
}
