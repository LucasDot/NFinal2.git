﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.UI
{
    public interface IComponent
    {
        void Load();
        NFinal.View.IView Invoke(object model = null);
    }
}
