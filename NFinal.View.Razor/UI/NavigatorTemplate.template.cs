﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using NFinal;
#if !NET20
using System.Threading.Tasks;
#endif

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinal.UI
{
    [View("/NFinal/UI/NavigatorTemplate.cshtml")]
    public class NavigatorTemplate : NFinal.View.RazorView<NFinal.UI.Navigator>
    {
        public NavigatorTemplate(NFinal.IO.Writer writer, NFinal.UI.Navigator Model) : base(writer, Model)
        {
        }

        public override void Execute()
        {
            
        }
#if !NET20
        //如果此处报错，请添加NFinal引用
        //PMC命令为：Install-Package NFinal2
        public override async Task ExecuteAsync()
        {
            writer.WriteAsync("");
            writer.WriteAsync("<link href=\"<%=ViewBag.bucketImageUrl%>/App/Content/css/navigator.css\" rel=\"stylesheet\" />\r\n<div class=\"pageDiv\">\r\n    <a href=\"");
            writer.WriteAsync(Model.GetUrlFunction(1));
            writer.WriteAsync("\" class=\"currentpage\">首页</a>\r\n");
            bool isFirst = Model.index <= 1; writer.Write("\r\n");

            string UrlPre = isFirst ? " #" : Model.GetUrlFunction(Model.index - 1);
            writer.WriteAsync("\r\n    \r\n");
            if (isFirst)
            {
                writer.WriteAsync("        <a href=\"");
                writer.WriteAsync(UrlPre);
                writer.WriteAsync("\" class=\"currentpage\">上一页</a>\r\n");
            }
            else
            {
                writer.WriteAsync("        <a href=\"");
                writer.WriteAsync(UrlPre);
                writer.WriteAsync("\" class=\"currentpage\">上一页</a>\r\n");
            }
            writer.WriteAsync("    <a href=\"");
            writer.WriteAsync(UrlPre);
            writer.WriteAsync("\" style=\"");
            writer.WriteAsync(Model.index);
            writer.WriteAsync(" <= 1 ? \"display: inline-block;padding: 4px 8px;margin: 0 2px;color: #bfbfbf;background: #f2f2f2;border: 1px solid #ddd;vertical-align: middle;cursor:default;\":\"\"%>\" class=\"currentpage\">上一页</a>\r\n");
            for (int i = ((Model.index - 1) / Model.navigatorSize) * Model.navigatorSize + 1; i <= Model.count && i <= ((Model.index - 1) / Model.navigatorSize + 1) * Model.navigatorSize; i++)
            {
                if (i == Model.index)
                {
                    writer.WriteAsync("            <span class=\"current\" style=\"width:30px;\"> ");
                    writer.WriteAsync(i);
                    writer.WriteAsync("</span>\r\n");
                }
                else
                {
                    writer.Write("            <a href=\"");
                    writer.Write(Model.GetUrlFunction(i));
                    writer.Write("\" class=\"currentpage\" style=\"width:30px;\">");
                    writer.Write(i);
                    writer.Write("</a>\r\n");
                }
            }
            writer.Write("    ");
            bool isLast = Model.index >= Model.count; writer.Write("\r\n");
            string UrlNext = isLast ? " #" : Model.GetUrlFunction(Model.index + 1); writer.Write("\r\n   \r\n");
            if (isLast)
            {
                writer.Write("        <a href=\"");
                writer.Write(UrlNext);
                writer.Write("\" class=\"currentpage\">下一页</a>\r\n");
            }
            else
            {
                writer.Write("        <a href=\"");
                writer.Write(UrlNext);
                writer.Write("\" class=\"currentpage\">下一页</a>\r\n");
            }
            writer.Write("    <a href=\"");
            writer.Write(Model.GetUrlFunction(Model.count));
            writer.Write("\" class=\"currentpage\">末页</a>\r\n    <span class=\"pagecount\">共@Model.count页</span>\r\n</div>");
        }
#endif
    }
}
