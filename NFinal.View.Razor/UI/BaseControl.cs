﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : BaseControl.cs
//        Description :基础控件类,自定义控件需要继承该类，并新建一个以Template.cshtml结尾以该自定义类为视图数据的Razor视图。
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;

namespace NFinal.UI
{
    /// <summary>
    /// 基础控件类
    /// </summary>
    public class BaseControl:IControl
    {
        public NFinal.Collections.FastDictionary<string, object> ViewData { get; set; }
        public NFinal.Collections.FastDictionary<string, NFinal.UI.BaseControl> Controls { get; set; }
        protected NFinal.IO.Writer writer;
        public NFinal.DependencyInjection.IServiceCollection serviceCollection { get; set; }
        public NFinal.UI.BaseControl GetControl(string fullName, params NFinal.StringContainer[] parameters)
        {
            NFinal.UI.BaseControl control = null;
            try
            {
                control = serviceCollection.GetService<NFinal.UI.BaseControl, NFinal.StringContainer[]>(fullName, parameters);
                if (control != null)
                {
                    return control;
                }
                else
                {
                    return new NFinal.UI.Common.NotFoundControl(writer, fullName);
                }
            }
            catch (Exception exception)
            {
                return new NFinal.UI.Common.ExceptionControl(writer, exception);
            }
        }
        /// <summary>
        /// 控件初始化
        /// </summary>
        /// <param name="writer"></param>
        public BaseControl(NFinal.IO.Writer writer)
        {
            Controls = new Collections.FastDictionary<string, BaseControl>();
            this.writer = writer;
        }
        /// <summary>
        /// 按照默认路径渲染
        /// </summary>
        public NFinal.View.IView Render()
        {
            Type t= this.GetType();
            string ViewPath= '/' + t.Namespace.Replace('.', '/') + '/' + t.Name + ".cshtml";
            return this.Render(ViewPath);
        }
        /// <summary>
        /// 按照拽认路径渲染
        /// </summary>
        /// <param name="ViewPath"></param>
        public NFinal.View.IView Render(string ViewPath)
        {
            return Render(ViewPath,this.GetType(), this);
        }
        public NFinal.View.IView Render<T>(string ViewPath,Type inputType, T t)
        {
            NFinal.ViewDelegateData dele;
            if (NFinal.ViewDelegate.viewFastDic != null)
            {
                if (NFinal.ViewDelegate.viewFastDic.TryGetValue(ViewPath, out dele))
                {
                    if (dele.renderMethod == null)
                    {
                        dele.renderMethod = NFinal.ViewDelegate.GetRenderDelegate<object>(ViewPath,inputType,Type.GetTypeFromHandle(dele.viewTypeHandle),false);
                        NFinal.ViewDelegate.viewFastDic[ViewPath] = dele;
                    }
                    var render =(RenderMethod<object>)dele.renderMethod;
                    var view= render(writer, t);
                    if (view.Layout != null)
                    {
                        var layoutView = Render<object>(view.Layout, inputType, t);
                        layoutView.View = view;
                        return layoutView;
                    }
                    else
                    {
                        return view;
                    }
                }
                else
                {
                    //模板未找到异常
                    throw new NFinal.Exceptions.ViewNotFoundException(ViewPath);
                }
            }
            else
            {
                throw new NFinal.Exceptions.ViewNotFoundException(ViewPath);
            }
        }
    }
}