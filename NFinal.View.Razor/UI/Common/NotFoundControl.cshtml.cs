﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
#if !NET20
using System.Threading.Tasks;
#endif
using NFinal;

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinal.UI.Common
{
    /// <summary>
    /// 模板类
    /// </summary>
    [View("/NFinal/UI/Common/NotFoundControl.cshtml")]
    public class NotFoundControl_cshtml : NFinal.View.RazorView<NFinal.UI.Common.NotFoundControl>
    {
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">数据</param>
        public NotFoundControl_cshtml(NFinal.IO.Writer writer, NFinal.UI.Common.NotFoundControl Model) : base(writer, Model)
        {
        }
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            writer.Write("");
            writer.Write("<div>\r\n    控件未找到.名称为:");
            writer.Write(Model.Name);
            writer.Write("\r\n</div>\r\n");
            writer.Write("");
        }
#if !NET20
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
        {
            await writer.WriteAsync("");
            await writer.WriteAsync("<div>\r\n    控件未找到.名称为:");
            await writer.WriteAsync(Model.Name);
            await writer.WriteAsync("\r\n</div>\r\n");
            await writer.WriteAsync("");
        }
#endif
    }
}
