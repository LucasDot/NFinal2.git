﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
#if !NET20
using System.Threading.Tasks;
#endif
using NFinal;

//此代码由NFinalCompiler生成。
//http://bbs.nfinal.com
namespace NFinal.UI.Common
{
    /// <summary>
    /// 模板类
    /// </summary>
    [View("/NFinal/UI/Common/ExceptionControl.cshtml")]
    public class ExceptionControl_cshtml : NFinal.View.RazorView<NFinal.UI.Common.ExceptionControl>
    {
        /// <summary>
        /// 模板类初始化函数
        /// </summary>
        /// <param name="writer">写对象</param>
        /// <param name="Model">数据</param>
        public ExceptionControl_cshtml(NFinal.IO.Writer writer, NFinal.UI.Common.ExceptionControl Model) : base(writer, Model)
        {
        }
        /// <summary>
        /// 输出模板内容
        /// </summary>
        public override void Execute()
        {
            writer.Write("");
            writer.Write("<div>\r\n    Message:");
            writer.Write(Model.Exception.Message);
            writer.Write("\r\n    Source:");
            writer.Write(Model.Exception.Source);
            writer.Write("\r\n    Source:");
            writer.Write(Model.Exception.StackTrace);
            writer.Write(";\r\n</div>");
        }
#if !NET20
        /// <summary>
        /// 输出模板内容(异步)
        /// </summary>
        public override async Task ExecuteAsync()
        {
            await writer.WriteAsync("");
            await writer.WriteAsync("<div>\r\n    Message:");
            await writer.WriteAsync(Model.Exception.Message);
            await writer.WriteAsync("\r\n    Source:");
            await writer.WriteAsync(Model.Exception.Source);
            await writer.WriteAsync("\r\n    Source:");
            await writer.WriteAsync(Model.Exception.StackTrace);
            await writer.WriteAsync(";\r\n</div>");
        }
#endif
    }
}
