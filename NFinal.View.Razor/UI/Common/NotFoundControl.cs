﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.UI.Common
{
    public class NotFoundControl : BaseControl
    {
        public string Name { get; set; }
        public NotFoundControl(NFinal.IO.Writer writer,string name):base(writer)
        {
            this.Name = name;
        }
    }
}
