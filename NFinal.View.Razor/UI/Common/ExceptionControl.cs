﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.UI.Common
{
    public class ExceptionControl : NFinal.UI.BaseControl
    {
        public System.Exception Exception { get; set; }
        public ExceptionControl(NFinal.IO.Writer writer,System.Exception exception):base(writer)
        {
            this.Exception = exception;
        }
    }
}
