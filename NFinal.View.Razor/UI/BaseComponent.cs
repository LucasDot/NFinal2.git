﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.UI
{
    
    public class BaseComponent: IComponent
    {
        public NFinal.IO.Writer writer;
        [FromModel]
        public NFinal.Collections.FastDictionary<string, object> ViewData;
        [FromModel]
        public NFinal.NameValueCollection parameters;
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="model"></param>
        public BaseComponent(NFinal.IO.Writer writer)
        {
            this.writer = writer;
        }
        /// <summary>
        /// 加载处理
        /// </summary>
        public virtual void Load()
        {
            
        }
        /// <summary>
        /// 执行函数
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public NFinal.View.IView Invoke(object model=null)
        {
            if (model == null)
            {
                model = this;
            }
            Type modelType = model.GetType();
            Type componentType = this.GetType();
            ComponentInvokeData componentInvokeData;
            string url = componentType.FullName;
            if (NFinal.UI.ComponentHelper.componentInvokeDelegateDictionary.TryGetValue(url, out componentInvokeData))
            {
                if (componentInvokeData.invokeComponentDelegate == null)
                {
                    componentInvokeData.invokeComponentDelegate = ComponentHelper.GenerateGetInvokeComponentDelegate(url, model);
                }
                NFinal.View.IView view= componentInvokeData.invokeComponentDelegate(url, model);
                return view;
            }
            else
            {
                throw new NFinal.Exceptions.ComponentNoFoundException(url);
            }
        }
    }   
}
