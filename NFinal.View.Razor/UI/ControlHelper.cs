﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace NFinal.UI
{
    public class ControlHelper
    {
        public static NFinal.DependencyInjection.IServiceCollection controlServiceCollection = new NFinal.DependencyInjection.ServiceCollection();
        public void Init(Assembly[] assemblys)
        {
            foreach (var assembly in assemblys)
            {
                Type[] types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.FullName.EndsWith("Control"))
                    {
                        if (typeof(NFinal.UI.BaseControl).IsAssignableFrom(type))
                        {
                            controlServiceCollection.SetService<NFinal.UI.IControl, NFinal.IO.Writer>(type.FullName, type);

                        }
                    }
                }
            }

        }
        public void GenerateInvokeControlDelegate(BaseControl control)
        {
           
        }
    }
}
