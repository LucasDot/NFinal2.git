﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
#if !NET20
using System.Threading.Tasks;
#endif
namespace NFinal.View.Razor
{
    public class RazorSimpleTemplate : ITemplate
    {
        public string RootPath { get; set; }
        public string Extension { get { return ".cshtml"; } }
        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            
            string controllerFullNameWithOutController=null;
            if (controllerFullName.EndsWith("Controller"))
            {
                controllerFullNameWithOutController = controllerFullName.Substring(0, controllerFullName.Length - "Controller".Length);
            }
            string[] nameSpace = controllerFullNameWithOutController.Split('.');
            string path = null;
            using (StringWriter sw = new StringWriter())
            {
                sw.Write("/");
                if (!string.IsNullOrEmpty(skinName))
                {
                    sw.Write(skinName);
                }
                else
                {
                    sw.Write(nameSpace[0]);
                }
                for (int i = 1; i < nameSpace.Length; i++)
                {
                    sw.Write("/");
                    if (nameSpace[i] == "Controllers")
                    {
                        sw.Write("Views");
                    }
                    else
                    {
                        sw.Write(nameSpace[i]);
                    }
                }
                sw.Write("/");
                sw.Write(methodName);
                sw.Write(Extension);
                path = sw.ToString();
            }
            return path;  
        }
        public static void Configure(NFinal.Logs.ILogger logger,Assembly[] assemblys)
        {
            ViewHelper viewHelper = new ViewHelper(logger, assemblys);
        }
        public object options;
        public RazorSimpleTemplate(string rootPath,object options)
        {
            this.options = options;
            this.RootPath = RootPath;
        }
#if !NET20
        public Task RenderAsync(NFinal.IO.Writer writer, string relativePath, object model)
        {
            return RenderModel(writer, relativePath, model).ExecuteAsync();
            //string content= Render(relativePath,model);
            //return writer.WriteAsync(content);
        }
#endif
        public string Render(NFinal.IO.Writer writer, string relativePath, object model)
        {
            RenderModel(writer,relativePath, model).Execute();
            return writer.ToString();
        }
        /// <summary>
        /// 模板渲染函数
        /// </summary>
        /// <typeparam name="T">视图数据类型</typeparam>
        /// <param name="url">视图URL路径</param>
        /// <param name="t">视图数据，即ViewBag</param>
        public IView RenderModel<T>(NFinal.IO.Writer writer,string url, T t)
        {
            
            if (NFinal.ViewDelegate.viewFastDic != null)
            {
                NFinal.ViewDelegateData dele;
                if (NFinal.ViewDelegate.viewFastDic.TryGetValue(url, out dele))
                {
                    Type inputType = t.GetType();
                    ViewDelegate.SetViewDataDelegate setViewDataDelegate = ViewDelegate.GetSetViewDataDelegate(t, inputType,Type.GetTypeFromHandle(dele.viewTypeHandle));
                    if (dele.renderMethod == null)
                    {
                        dele.renderMethod = NFinal.ViewDelegate.GetRenderDelegate<T>(url, t.GetType(),Type.GetTypeFromHandle(dele.viewTypeHandle),false);
                        NFinal.ViewDelegate.viewFastDic[url] = dele;
                    }
                    var render = (NFinal.RenderMethod<T>)dele.renderMethod;
                    var view = render(writer, t);
                    view = setViewDataDelegate(t, view);
                    if (view.Layout != null)
                    {
                        var layoutView = RenderModel<T>(writer, view.Layout, t);
                        layoutView.View = view;
                        return layoutView;
                    }
                    else
                    {
                        return view;
                    }
                }
                else
                {
                    //模板未找到异常
                    throw new NFinal.Exceptions.ViewNotFoundException(url);
                }
            }
            else
            {
                throw new NFinal.Exceptions.ViewNotFoundException(url);
            }
        }
    }
}
