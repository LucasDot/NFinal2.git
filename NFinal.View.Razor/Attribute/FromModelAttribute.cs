﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    /// <summary>
    /// 表示该字段或属性数据将从传入的数据中自动获取
    /// </summary>
    [AttributeUsage(AttributeTargets.Property|AttributeTargets.Field)]
    public class FromModelAttribute : System.Attribute
    {
        public string Name;
        /// <summary>
        /// 取出Model里中与该名称一致的属性或字段数据放入该属性中
        /// </summary>
        public FromModelAttribute()
        { }
        /// <summary>
        /// 取出Model里的相应名称的属性或字段放入该属性中
        /// </summary>
        /// <param name="name">Model中属性或字段的名称</param>
        public FromModelAttribute(string name)
        {
            this.Name = name;
        }
    }
}
