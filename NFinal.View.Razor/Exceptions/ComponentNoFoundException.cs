﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Exceptions
{
    public class ComponentNoFoundException : System.Exception
    {
        public ComponentNoFoundException(string url)
            :base($"组件未找到！请确认路径：\"{url}\"是否正确。")
        {
            
        }
    }
}
