﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Exceptions
{
    public class SectionNotFoundException : KeyNotFoundException
    {
        public string Url { get; set; }
        public string SectionName { get; set; }
        public SectionNotFoundException(string sectionName,string url) : base($"找不到名为{sectionName}的节")
        {
            this.SectionName = sectionName;
            this.Url = url;
        }
    }
}
