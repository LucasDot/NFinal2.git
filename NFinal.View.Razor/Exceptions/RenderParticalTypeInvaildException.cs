﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.Exceptions
{
    public class RenderParticalTypeInvaildException : InvalidCastException
    {
        public RenderParticalTypeInvaildException(string url,Type inputType,Type modelType) : base($"在调用RenderPartical(\"{url}\")时出现类型不匹配错误:" +
                                        $"输入类型为:{inputType.FullName},模板所需类型为:{modelType.FullName}," +
                                        $"请更改{url}模板中的类型声明为{inputType.FullName}")
        {

        }
    }
}
