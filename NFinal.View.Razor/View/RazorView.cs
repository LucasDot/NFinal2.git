﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : RazorView.cs
//        Description :视图基础类
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
#if !NET20
using System.Threading.Tasks;
#endif
namespace NFinal.View
{
    /// <summary>
    /// cshtml基础视图
    /// </summary>
    /// <typeparam name="T">强类型视图数据，类型为object或dynamic时即为模板页</typeparam>
    public abstract class RazorView<T> : NFinal.DependencyInjection.ServiceCollectionHandler, IView
    {
        /// <summary>
        /// 生成Url的对象
        /// </summary>
        public NFinal.IUrlHelper UrlHelper { get; set; }
        /// <summary>
        /// 视图数据
        /// </summary>
        [FromModel]
        public NFinal.Collections.FastDictionary<string, object> ViewData { get; set; }
        /// <summary>
        /// 子视图
        /// </summary>
        [FromModel]
        public NFinal.Collections.FastDictionary<string, IView> Views { get; set; }
        /// <summary>
        /// 布局页路径
        /// </summary>
        [FromModel]
        public string Layout { get; set; }
        /// <summary>
        /// 当前页路径
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 布局页中需要渲染的页面
        /// </summary>
        public IView View { get; set; }
        /// <summary>
        /// 参数
        /// </summary>
        [FromModel]
        public NFinal.NameValueCollection parameters { get; set; }
#if NET20
        public NFinal.Collections.FastDictionary<string, NFinal.IO.WriteDelegate> Sections { get; set; }
#else
        public NFinal.Collections.FastDictionary<string, NFinal.IO.WriteAsyncDelegate> Sections { get; set; }
#endif
        /// <summary>
        /// 渲染页面
        /// </summary>
        /// <returns></returns>
        public IView RenderBody()
        {
            return View;
        }
        public IView InvokeComponent(string controlFullName)
        {
            //参数.1.控件名称，2.writer对象,3.Model数据及类型4.控件类型

            //初始化
            var control = NFinal.UI.ControlHelper.controlServiceCollection.GetService<NFinal.UI.IControl, NFinal.IO.Writer>(writer);
            //复制数据
            NFinal.Emit.CopyHelper.CopyEmit(this.Model, control);
            //调用自动初始化函数

            //调用模板函数
            return control.Render();
        }
        /// <summary>
        /// 用当前Model渲染Page
        /// </summary>
        /// <param name="url"></param>
        /// <param name="model">数据</param>
        /// <returns></returns>
        public IView RenderPage<TModel>(string url, TModel model = null) where TModel : class, new()
        {
            return Render(url, model, Razor.RenderType.RenderPage);
        }
        /// <summary>
        /// 渲染局部页
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public IView RenderPartical(string url)
        {
            return Render(url, Model, Razor.RenderType.RenderPartical);
        }
#if NET20
        /// <summary>
        /// 在布局页中，将呈现指定部分的内容。
        /// </summary>
        /// <param name="sectionName">节名称</param>
        /// <param name="required">是否需要</param>
        /// <returns></returns>
        public NFinal.IO.WriteDelegate RenderSection(string sectionName,bool required=true)
        {
            if (View.Sections.TryGetValue(sectionName, out NFinal.IO.WriteDelegate render))
            {
                return render;
            }
            else
            {
                if (required)
                {
                    throw new NFinal.Exceptions.SectionNotFoundException(sectionName, Url);
                }
                else
                {
                    return ()=> { };
                }
            }
        }
#else
        /// <summary>
        /// 在布局页中，将呈现指定部分的内容。
        /// </summary>
        /// <param name="sectionName">节名称</param>
        /// <param name="required">是否需要</param>
        /// <returns></returns>
        public NFinal.IO.WriteAsyncDelegate RenderSection(string sectionName, bool required = true)
        {
            if (View.Sections.TryGetValue(sectionName, out NFinal.IO.WriteAsyncDelegate render))
            {
                return render;
            }
            else
            {
                if (required)
                {
                    throw new NFinal.Exceptions.SectionNotFoundException(sectionName, Url);
                }
                else
                {
                    return () => { return NFinal.DependencyInjection.TaskHelper.CompletedTask; };
                }
            }
        }
        /// <summary>
        /// 在布局页中，将呈现指定部分的内容。
        /// </summary>
        /// <typeparam name="TProperty">属性值类型</typeparam>
        /// <param name="url">Razor模板特性路径</param>
        /// <param name="propertyName">属性名称</param>
        /// <returns></returns>
        //public Task RenderSectionAsync(string sectionName, bool required = true)
        //{
        //    if (View.Sections.TryGetValue(sectionName, out Func<Task> render))
        //    {
        //        return render();
        //    }
        //    else
        //    {
        //        if (required)
        //        {
        //            throw new NFinal.Exceptions.SectionNotFoundException(sectionName, Url);
        //        }
        //        else {
        //            return NFinal.DependencyInjection.TaskHelper.CompletedTask;
        //        }
        //    }
        //}
#endif
        ///// <summary>
        ///// 生成Html辅助对象
        ///// </summary>
        //public NFinal.View.HtmlHelper HtmlHelper { get; set; }
        public class ViewKey
        {
            public string url { get; set; }
            public Type modelType { get; set; }
        }
        //url+type=key,这个地方一直没有修正好,是因为url对应多个输出函数.
        public NFinal.View.IView Render<TModel>(string url, TModel t)
        {
            ViewKey viewKey = new ViewKey();
            viewKey.url = url;
            viewKey.modelType = typeof(TModel);
            if (NFinal.ViewDelegate.viewFastDic.TryGetValue(url, out dele))
            {

            }
        }
        /// <summary>
        /// 模板渲染函数
        /// </summary>
        /// <typeparam name="TModel">视图数据类型</typeparam>
        /// <param name="url">视图URL路径</param>
        /// <param name="t">视图数据，即ViewBag</param>
        /// <param name="renderType">渲染类型</param>
        public NFinal.View.IView Render<TModel>(string url, TModel t,NFinal.View.Razor.RenderType renderType=Razor.RenderType.Render)
        {
            if (NFinal.ViewDelegate.viewFastDic != null)
            {
                NFinal.ViewDelegateData dele;
                if (NFinal.ViewDelegate.viewFastDic.TryGetValue(url, out dele))
                {
                    Type inputType;
                    Type renderMethodViewType = Type.GetTypeFromHandle(dele.viewTypeHandle);
                    Type modelType = renderMethodViewType.GetProperty("Model").PropertyType;
                    ViewDelegate.SetViewDataDelegate setViewDataDelegate = null;
                    if (t != null)
                    {
                        inputType = t.GetType();
                        setViewDataDelegate = ViewDelegate.GetSetViewDataDelegate(t, inputType, renderMethodViewType);
                    }
                    else
                    {
                        inputType = null;
                    }
                   
                    if (dele.renderMethod == null)
                    {
                        
                        if (renderType == Razor.RenderType.RenderPartical)
                        {
                            if (inputType != modelType)
                            {
                                if (!inputType.IsAssignableFrom(modelType))
                                {
                                    throw new Exceptions.RenderParticalTypeInvaildException(url, inputType, modelType);
                                }
                            }
                        }
                        if (renderType == Razor.RenderType.RenderPage)
                        {

                        }
                        dele.renderMethod = NFinal.ViewDelegate.GetRenderDelegate<TModel>(url, inputType, renderMethodViewType, false);
                        NFinal.ViewDelegate.viewFastDic[url] = dele;
                    }
                    var render = (NFinal.RenderMethod<TModel>)dele.renderMethod;
                    var view = render(writer, t);
                    if (inputType != null)
                    {
                        view = setViewDataDelegate(t, view);
                    }
                    if (view.Layout != null)
                    {
                        var layoutView = Render<TModel>(view.Layout, t);
                        layoutView.View = view;
                        return layoutView;
                    }
                    else
                    {
                        return view;
                    }
                }
                else
                {
                    //模板未找到异常
                    throw new NFinal.Exceptions.ViewNotFoundException(url);
                }
            }
            else
            {
                throw new NFinal.Exceptions.ViewNotFoundException(url);
            }
        }
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="writer">输出对象</param>
        /// <param name="Model">实体数据</param>
        public RazorView(NFinal.IO.Writer writer,T Model)
        {
            this.UrlHelper = ServiceCollection.GetService<NFinal.IUrlHelper>();
            //this.parameters = parameters;
            //this.HtmlHelper = new HtmlHelper();
            this.writer = writer;
            this.Model = Model;
        }
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="writer">输出对象</param>
        /// <param name="model">任意对象</param>
        public RazorView(NFinal.IO.Writer writer, object model)
        {
            this.UrlHelper = ServiceCollection.GetService<NFinal.IUrlHelper>();
            this.writer = writer;
            if (model != null)
            {
                this.Model = NFinal.Emit.CopyHelper.GetModel<T>(model);
            }
        }
        //public Task ComponentInvokeAsync(string componentName)
        //{
        //    var component = NFinal.Application.serviceCollection
        //        .GetService<NFinal.UI.BaseComponent, string, NFinal.NameValueCollection>(componentName, parameters);
        //    return component.Render().ExecuteAsync();
        //}
        //public Task ControlInvokeAsync(string controlName,object model)
        //{
        //    var control= NFinal.Application.serviceCollection.GetService<NFinal.UI.BaseControl,object>(controlName, model);
        //    return control.Render().ExecuteAsync();
        //}
        /// <summary>
        /// 输出对象
        /// </summary>
        public NFinal.IO.Writer writer { get; set; }
        /// <summary>
        /// 模板实体类
        /// </summary>
        public T Model { get; set; }
        /// <summary>
        /// 输出模板
        /// </summary>
        public abstract void Execute();
#if !NET20
        /// <summary>
        /// 输出模板(异步)
        /// </summary>
        public abstract Task ExecuteAsync();
#endif
    }
}