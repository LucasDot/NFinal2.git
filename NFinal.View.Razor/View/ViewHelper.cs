﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : ViewHelper.cs
//        Description :视图初始化以及执行帮助类
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
#if !NET20
using System.Linq;
#endif
using System.Reflection;
using System.Reflection.Emit;

namespace NFinal
{
    /// <summary>
    /// 视图初始化以及执行帮助类
    /// </summary>
    public class ViewHelper
    {
        private NFinal.Logs.ILogger logger;
        public ViewHelper(NFinal.Logs.ILogger logger,Assembly[] assemblys)
        {
            this.logger = logger;
            if (!isInit)
            {
                isInit = true;
                Init(assemblys);
            }
        }
        /// <summary>
        /// 视图是否初始化
        /// </summary>
        public static bool isInit = false;
        //public static Dictionary<string, NFinal.ViewDelegateData>  dicViews = new Dictionary<string, NFinal.ViewDelegateData>();
        /// <summary>
        /// 视图执行代理缓存
        /// </summary>
        /// <summary>
        /// 视图初始化
        /// </summary>
        /// <param name="globalConfig"></param>
        public void Init(Assembly[] assemblys)
        {
            Assembly assembly = null;
            Module[] modules= null;
            NFinal.ViewDelegateData dele;
            ViewAttribute viewAttr;
            NFinal.Collections.FastDictionary<string, ViewDelegateData> viewDataDictionary = new NFinal.Collections.FastDictionary<string, NFinal.ViewDelegateData>();
            for (int i = 0; i < assemblys.Length; i++)
            {
                assembly = assemblys[i];
                Type[] types;
                try
                {
                    types = assembly.GetTypes();
                }
                catch (System.Reflection.ReflectionTypeLoadException loadException)
                {
                    logger.Fatal("dll加载错误!");
                    foreach (var loaderException in loadException.LoaderExceptions)
                    {
                        logger.Fatal(loaderException.Message);
                        logger.Fatal(loaderException.Source);
                        logger.Fatal(loaderException.StackTrace);
                    }
                    throw loadException;
                }
                for (int k = 0; k < types.Length; k++)
                {
                    if (!types[k].Name.EndsWith("_cshtml"))
                    {
                        continue;
                    }
#if (NET20 || NET40 || NET451 || NET461)
                    var attrs =   types[k].GetCustomAttributes(typeof(ViewAttribute), true);
                    if (attrs.Length > 0)
                    {
                        viewAttr = (ViewAttribute)attrs[0];
#endif 
#if NETCORE
                    var attrs = types[k].GetTypeInfo().GetCustomAttributes(typeof(ViewAttribute), false);
                    if (attrs.Count() > 0)
                    {
                        viewAttr = (ViewAttribute)attrs.First();
#endif
                        if (string.IsNullOrEmpty(viewAttr.viewUrl))
                        {
                            viewAttr.viewUrl = types[k].FullName.Replace('.', '/');
                        }
                        dele = new ViewDelegateData();
                        dele.viewTypeHandle = types[k].TypeHandle;
                        dele.renderMethod = null;//GetRenderDelegate(dele.renderMethodInfo);
                        dele.viewClassName = types[k].FullName;
                        //dicViews.Add(viewAttr.viewUrl, dele);
                        if (viewDataDictionary.ContainsKey(viewAttr.viewUrl))
                        {
                            var oldViewDelegateData = viewDataDictionary[viewAttr.viewUrl];
                            throw new NFinal.Exceptions.DuplicateViewUrlException(oldViewDelegateData.viewClassName, dele.viewClassName);
                        }
                        else
                        {
                            viewDataDictionary.Add(viewAttr.viewUrl, dele);
                        }
                    }
                }
                
            }
            ViewDelegate.viewFastDic = viewDataDictionary;
        }
        
    }
}
