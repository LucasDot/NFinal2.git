﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;

namespace NFinal
{
    /// <summary>
    /// 视渲染函数代理
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="writer"></param>
    /// <param name="t"></param>
    public delegate NFinal.View.IView RenderMethod<T>(NFinal.IO.Writer writer, T t);
    //public delegate void GetRenderMethod<T>(T t);
    /// <summary>
    /// 视图渲染信息
    /// </summary>
    public struct ViewDelegateData
    {
        /// <summary>
        /// 视图类名
        /// </summary>
        public string viewClassName;
        /// <summary>
        /// 视图类型
        /// </summary>
        public RuntimeTypeHandle viewTypeHandle;
        /// <summary>
        /// 视图渲染函数
        /// </summary>
        public Delegate renderMethod;
    }
    public static class ViewDelegate
    {
        public static NFinal.Collections.FastDictionary<string, NFinal.ViewDelegateData> viewFastDic = null;
        public static NFinal.Collections.FastDictionary<Tuple<string, Type>, NFinal.ViewDelegateData> viewModelFastDic = null;
        /// <summary>
        /// 视图泛型代理
        /// </summary>
        public static Delegate renderMethodDelegate = null;
        /// <summary>
        /// 组装并返回视图泛型函数代理
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="viewType"></param>
        /// <returns></returns>
        public static Delegate GetRenderDelegate<T>(string url, Type viewType)
        {
#if !NFinalIO
            PropertyInfo modelProperty = viewType.GetProperty("Model");
            Type modelType = modelProperty.PropertyType;
            if (typeof(T) != modelType)
            {
                throw new Exceptions.ViewModelTypeUnMatchedException(url, typeof(T), modelType);
            }
            DynamicMethod method = new DynamicMethod("RenderX", typeof(NFinal.View.IView), new Type[] { typeof(NFinal.IO.Writer), modelType });
            ILGenerator methodIL = method.GetILGenerator();
            var model = methodIL.DeclareLocal(modelType);
            methodIL.Emit(OpCodes.Nop);
            //methodIL.Emit(OpCodes.Ldarg_1);
            //methodIL.Emit(OpCodes.Castclass, modelType);
            //methodIL.Emit(OpCodes.Stloc,model);
            //methodIL.Emit(OpCodes.Ldarg_0);
            //methodIL.Emit(OpCodes.Ldloc,model);
            methodIL.Emit(OpCodes.Ldarg_0);
            methodIL.Emit(OpCodes.Ldarg_1);
            methodIL.Emit(OpCodes.Newobj, viewType.GetConstructor(new Type[] { typeof(NFinal.IO.Writer), modelType }));
            //methodIL.Emit(OpCodes.Callvirt, viewType.GetMethod("Execute", Type.EmptyTypes));
            methodIL.Emit(OpCodes.Ret);
            renderMethodDelegate = method.CreateDelegate(typeof(NFinal.RenderMethod<>).MakeGenericType(modelType));
            return renderMethodDelegate;
#else
            return null;
#endif
        }
        public delegate NFinal.View.IView SetViewDataDelegate(object input, NFinal.View.IView view);
        public static Dictionary<Tuple<RuntimeTypeHandle,RuntimeTypeHandle>, SetViewDataDelegate> dictionarySetViewDataDelegate = new Dictionary<Tuple<RuntimeTypeHandle,RuntimeTypeHandle>, SetViewDataDelegate>();
        /// <summary>
        /// 生成自动复制视图数据到视图属性或字段的代理
        /// </summary>
        /// <param name="inputType"></param>
        /// <param name="viewType"></param>
        /// <returns></returns>
        public static SetViewDataDelegate GenerateGetSetViewDataDelegate(Type inputType,Type viewType)
        {
            //viewType = typeof(NFinal.View.IView);
            DynamicMethod method = new DynamicMethod("SetDataX", typeof(NFinal.View.IView), new Type[] { typeof(object), typeof(NFinal.View.IView) },true);
            ILGenerator methodIL = method.GetILGenerator();
            //查找出视图类中有哪些字段
            FieldInfo[] fields = viewType.GetFields();
            //表示需要复制的属性或字段
            FromModelAttribute fromModelAttribute;
            var inputModel = methodIL.DeclareLocal(inputType);
            var viewCshtml = methodIL.DeclareLocal(viewType);
            methodIL.Emit(OpCodes.Ldarg_0);
            methodIL.Emit(OpCodes.Castclass, inputType);
            methodIL.Emit(OpCodes.Stloc,inputModel);
            methodIL.Emit(OpCodes.Ldarg_1);
            methodIL.Emit(OpCodes.Castclass,viewType);
            methodIL.Emit(OpCodes.Stloc,viewCshtml);
            string name;
            foreach (var fieldt in fields)
            {
                //获取所有的特性
                var attrs= fieldt.GetCustomAttributes(typeof(FromModelAttribute),false);
                foreach (var attr in attrs)
                {
                    fromModelAttribute = (FromModelAttribute)attr;
                    //如果定义了名称
                    if (fromModelAttribute.Name == null)
                    {
                        name = fieldt.Name;
                    }
                    else
                    {
                        name = fromModelAttribute.Name;
                    }
                    //复制数据
                    AddCopyData(methodIL, inputType,inputModel,viewType,viewCshtml, name, fieldt.FieldType, null, fieldt, false);
                    break;
                }
            }
            Dictionary<string, PropertyInfo> propertyInfoDictionary = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] propertys = viewType.GetProperties();
            foreach (var propertyt in propertys)
            {
                var attrs = propertyt.GetCustomAttributes(typeof(FromModelAttribute),false);
                foreach (var attr in attrs)
                {
                    fromModelAttribute =(FromModelAttribute) attr;
                    if (fromModelAttribute.Name == null)
                    {
                        name = propertyt.Name;
                    }
                    else
                    {
                        name = fromModelAttribute.Name;
                    }
                    AddCopyData(methodIL, inputType,inputModel,viewType,viewCshtml, name,propertyt.PropertyType, propertyt, null, true);
                    break;
                }
                
            }
            methodIL.Emit(OpCodes.Ldarg_1);
            methodIL.Emit(OpCodes.Ret);
            var setViewDataDelegate = (SetViewDataDelegate)method.CreateDelegate(typeof(SetViewDataDelegate));
            return setViewDataDelegate;
        }
        /// <summary>
        /// 复制数据
        /// </summary>
        /// <param name="methodIL">IL生成器</param>
        /// <param name="inputType">输入类型</param>
        /// <param name="name">查找的字段或属性名称</param>
        /// <param name="memberType">该字段或属性的类型</param>
        /// <param name="viewProperty">视图中的属性</param>
        /// <param name="viewField">视图中的字段</param>
        /// <param name="isPropertyOrField">添加的是属性还是字段</param>
        public static void AddCopyData(ILGenerator methodIL,Type inputType,LocalBuilder inputModel,Type viewType,LocalBuilder viewCshtml, string name, Type memberType,PropertyInfo viewProperty,FieldInfo viewField,bool isPropertyOrField)
        {
            PropertyInfo property;
            var hasProperty = inputType.HasProperty(name, memberType, true, false, out property);
            FieldInfo field;
            var hasField = inputType.HasField(name, memberType, out field);
            if (hasProperty || hasField)
            {
                methodIL.Emit(OpCodes.Ldloc, viewCshtml);
                methodIL.Emit(OpCodes.Ldloc,inputModel);
                //object类型转换为强类型，否则在取字段或属性时会报错。
                if (hasProperty)
                {
                    //model.ViewData;
                    methodIL.Emit(OpCodes.Callvirt, property.GetGetMethod());
                }
                if (hasField)
                {
                    //model.ViewData;
                    methodIL.Emit(OpCodes.Ldfld, field);
                }
                if (isPropertyOrField)
                {
                    //viewInstance.ViewData=model.ViewData;
                    methodIL.Emit(OpCodes.Callvirt, viewProperty.GetSetMethod());
                }
                else
                {
                    methodIL.Emit(OpCodes.Stfld,viewField);
                }
            }
        }
        public static SetViewDataDelegate GetSetViewDataDelegate(object model,Type inputType,Type viewType)
        {
            SetViewDataDelegate setViewDataDelegate=null;
            var key = new Tuple<RuntimeTypeHandle, RuntimeTypeHandle>(inputType.TypeHandle, viewType.TypeHandle);
            if (!dictionarySetViewDataDelegate.TryGetValue(key, out setViewDataDelegate))
            {
                setViewDataDelegate = GenerateGetSetViewDataDelegate(inputType,viewType);
                dictionarySetViewDataDelegate.Add(key, setViewDataDelegate);
            }
            return setViewDataDelegate;
        }
        public static bool HasField(this Type inputType,string name,Type fieldType,out FieldInfo field)
        {
            field = inputType.GetField(name);
            if (field == null || field.FieldType != fieldType || field.IsPublic==false)
            {
                return false;
            }
            return true;
        }
        public static bool HasProperty(this Type inputType,string name, Type propertyType, bool needGet, bool needSet,out PropertyInfo property)
        {
            property = inputType.GetProperty(name);
            if (property == null
                || property.PropertyType != propertyType)
            {
                return false;
            }
            else
            {
                if (needGet && property.GetGetMethod()==null)
                {
                    return false;
                }
                if (needSet && property.GetSetMethod() == null)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 组装并返回视图泛型函数代理
        /// </summary>
        /// <param name="url"></param>
        /// <param name="inputType"></param>
        /// <param name="viewType"></param>
        /// <returns></returns>
        public static Delegate GetRenderDelegate<T>(string url, Type inputType, Type viewType, bool isControl)
        {
#if !NFinalIO
            Type modelType = null;
            if (isControl)
            {
                modelType = viewType;
            }
            else
            {
                PropertyInfo modelProperty = viewType.GetProperty("Model");
                modelType = modelProperty.PropertyType;
            }
            Type methodModelType;
            bool isAnonymousType = false;
            //throw new Exceptions.ViewModelTypeUnMatchedException(url, inputType, modelType);
            if (inputType == null)
            {
                methodModelType = modelType;
            }
            //如果输入类型与输出类型一致,且都不为object类型
            else if (modelType == inputType && modelType != typeof(object))
            {
                methodModelType = modelType;
            }
            //如果输入类型为object类型
            else if (inputType == typeof(object))
            {
                //System.Runtime.CompilerServices.CompilerGeneratedAttribute
                methodModelType = typeof(object);

#if NETSTANDARD1_3
                var compilerGeneratedAttrs = inputType.GetTypeInfo().GetCustomAttributes(
                    typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute));
                if (compilerGeneratedAttrs.Count() > 0)
                {
                    isAnonymousType = true;
                }
#else
                var compilerGeneratedAttrs =  inputType.GetCustomAttributes(
                typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), false);
                if(compilerGeneratedAttrs.Length>0)
                {
                    isAnonymousType=true;
                }
#endif
            }
            else
            {
                methodModelType = typeof(object);
            }
            DynamicMethod method = new DynamicMethod("RenderX", typeof(NFinal.View.IView), new Type[] { typeof(NFinal.IO.Writer), typeof(T) }, true);
            ILGenerator methodIL = method.GetILGenerator();
            //var model = methodIL.DeclareLocal(modelType);
            var viewInstance = methodIL.DeclareLocal(typeof(NFinal.View.IView));
            methodIL.Emit(OpCodes.Nop);
            methodIL.Emit(OpCodes.Ldarg_0);
            if (inputType != null)
            {
                methodIL.Emit(OpCodes.Ldarg_1);
                if (methodModelType != typeof(object) && typeof(T) == typeof(object))
                {
                    methodIL.Emit(OpCodes.Castclass, methodModelType);
                }
            }
            else
            {
                methodIL.Emit(OpCodes.Ldnull);
            }
            methodIL.Emit(OpCodes.Newobj, viewType.GetConstructor(new Type[] { typeof(NFinal.IO.Writer), methodModelType }));
                #region
            //var ViewDataProperty = inputType.GetProperty("ViewData");

            //if (ViewDataProperty != null && ViewDataProperty.PropertyType == typeof(NFinal.Collections.FastDictionary<string, object>)
            //    && ViewDataProperty.GetGetMethod() != null)
            //{
            //    //viewinstance=view;
            //    methodIL.Emit(OpCodes.Stloc, viewInstance);
            //    //viewinstance
            //    methodIL.Emit(OpCodes.Ldloc, viewInstance);
            //    //model
            //    methodIL.Emit(OpCodes.Ldarg_1);
            //    if (typeof(T) == typeof(object))
            //    {
            //        methodIL.Emit(OpCodes.Castclass, inputType);
            //    }
            //    //model.ViewData;
            //    methodIL.Emit(OpCodes.Callvirt, ViewDataProperty.GetGetMethod());
            //    //viewInstance.ViewData=model.ViewData;
            //    methodIL.Emit(OpCodes.Callvirt, typeof(NFinal.View.IView).GetProperty("ViewData").GetSetMethod());
            //    methodIL.Emit(OpCodes.Ldloc, viewInstance);
            //}
            //else
            //{
            //    var ViewDataField = inputType.GetField("ViewData");
            //    if (ViewDataField != null && ViewDataField.FieldType == typeof(NFinal.Collections.FastDictionary<string, object>))
            //    {
            //        //viewinstance=view;
            //        methodIL.Emit(OpCodes.Stloc, viewInstance);
            //        //model
            //        methodIL.Emit(OpCodes.Ldloc, viewInstance);
            //        methodIL.Emit(OpCodes.Ldarg_1);
            //        if (typeof(T)==typeof(object))
            //        {
            //            methodIL.Emit(OpCodes.Castclass, inputType);
            //        }
            //        //model.ViewData;
            //        methodIL.Emit(OpCodes.Ldfld, ViewDataField);
            //        //viewInstance.ViewData=model.ViewData;
            //        methodIL.Emit(OpCodes.Callvirt, typeof(NFinal.View.IView).GetProperty("ViewData").GetSetMethod());
            //        methodIL.Emit(OpCodes.Ldloc, viewInstance);
            //    }
            //}
                #endregion
            //methodIL.Emit(OpCodes.Callvirt, viewType.GetMethod("Execute", Type.EmptyTypes));
            methodIL.Emit(OpCodes.Ret);
            renderMethodDelegate = method.CreateDelegate(typeof(NFinal.RenderMethod<T>));;
            return renderMethodDelegate;
#else
            return null;
#endif
            }
    }
}