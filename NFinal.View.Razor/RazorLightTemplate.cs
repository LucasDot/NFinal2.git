﻿using System;
using System.Collections.Generic;
using System.Text;
using RazorLightEx = RazorLight;

namespace NFinal.View.Razor
{
    public class RazorLightTemplate : ITemplate
    {
        public string RootPath { get; set; }
        public string Extension { get { return ".htm"; } }
        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            string path = controllerFullName.Substring(assemblyName.Length);
            path = path.Replace('.', '/');
            if (string.IsNullOrEmpty(skinName))
            {
                path = "/" + assemblyName + "/" + path + "/" + methodName + Extension;
            }
            else
            {
                path ="/"+assemblyName+"/"+ skinName + "/" + path + "/" + methodName + Extension;
            }
            return path;
        }
        public object options;
        private RazorLightEx.IRazorLightEngine engine;
        public RazorLightTemplate(string rootPath, object options)
        {
            this.engine = RazorLightEx.EngineFactory.CreatePhysical(rootPath);
            this.options = options;
            this.RootPath = RootPath;
        }

        public string Render(string relativePath, object model)
        {
            string content= this.engine.Parse(relativePath, model);
            return content;
        }
    }
}
