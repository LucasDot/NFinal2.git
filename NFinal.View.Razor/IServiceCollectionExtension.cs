﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;
using System.Reflection;

namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_View_Razor_Extension
    {
        /// <summary>
        /// 设置二进制序列化
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void SetViewRazor(this IServiceCollection serviceCollection, NFinal.Logs.ILogger logger, Assembly[] assemblys,bool rewrite=true)
        {
            serviceCollection.SetService<NFinal.View.ITemplate, string, object>(typeof(NFinal.View.Razor.RazorSimpleTemplate),rewrite).Configure(logger,assemblys);
        }
    }
}
