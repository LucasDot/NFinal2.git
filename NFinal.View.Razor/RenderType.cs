﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.View.Razor
{
    /// <summary>
    /// Razor引擎的渲染类型
    /// </summary>
    public enum RenderType
    {
        /// <summary>
        /// 基础渲染
        /// </summary>
        Render=0,
        /// <summary>
        /// 渲染子页面
        /// </summary>
        RenderBody =1,
        /// <summary>
        /// 渲染子页面的节
        /// </summary>
        RenderSection=2,
        /// <summary>
        ///  渲染其它页面
        /// </summary>
        RenderPage=3,
        /// <summary>
        /// 渲染部分页面
        /// </summary>
        RenderPartical=4,
        /// <summary>
        /// 渲染并调用组件
        /// </summary>
        InvokeComponent=5,
        /// <summary>
        /// 渲染布局页面
        /// </summary>
        RenderLayOut=6
    }
}
