﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.DependencyInjection
{

    /// <summary>
    /// 依赖注入接口
    /// </summary>
    public interface IServiceCollection
    {
        Func<TResult> GetServiceConstructor<TInterface, TResult>();
        Func<T, TResult> GetServiceConstructor<TInterface, T, TResult>();
        Func<T1, T2, TResult> GetServiceConstructor<TInterface, T1, T2, TResult>();
        Func<T1, T2, T3, TResult> GetServiceConstructor<TInterface, T1, T2, T3, TResult>();
        Func<T1, T2, T3, T4, TResult> GetServiceConstructor<TInterface, T1, T2, T3, T4, TResult>();

        #region 全局注入对象
        /// <summary>
        /// 模板引擎
        /// </summary>
        Func<string, object, NFinal.View.ITemplate> templateConstructor { get; set; }
        /// <summary>
        /// 全局缓存对象
        /// </summary>
        NFinal.Cache.ICache<string> cache { get; set; }
        /// <summary>
        /// 全局序列化对象
        /// </summary>
        NFinal.Serialize.ISerializable serializable { get; set; }
        /// <summary>
        /// 获取返回全局Session缓存对象的代理
        /// </summary>
        Func<string, NFinal.Http.ISession> sessionConstructor { get; set; }
        /// <summary>
        /// 全局Json序列化对象
        /// </summary>
        NFinal.Json.IJsonSerialize jsonSerialize { get; set; }
        /// <summary>
        /// 全局日志对象
        /// </summary>
        NFinal.Logs.ILogger logger { get; set; }
        #endregion

        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface>();
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface>(string name);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <param name="t1">参数1</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1>(T1 t1);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="t1">参数1</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1>(string name, T1 t1);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <param name="t1">参数1</param>
        /// <param name="t2">参数2</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1, T2>(T1 t1, T2 t2);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="t1">参数1</param>
        /// <param name="t2">参数2</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1, T2>(string name, T1 t1, T2 t2);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <param name="t1">参数1</param>
        /// <param name="t2">参数2</param>
        /// <param name="t3">参数3</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1, T2, T3>(T1 t1, T2 t2, T3 t3);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="t1">参数1</param>
        /// <param name="t2">参数2</param>
        /// <param name="t3">参数3</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1, T2, T3>(string name, T1 t1, T2 t2, T3 t3);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <typeparam name="T4">参数4类型</typeparam>
        /// <param name="t1">参数1</param>
        /// <param name="t2">参数2</param>
        /// <param name="t3">参数3</param>
        /// <param name="t4">参数4</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1, T2, T3, T4>(T1 t1, T2 t2, T3 t3, T4 t4);
        /// <summary>
        /// 获取服务对象
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <typeparam name="T4">参数4类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="t1">参数1</param>
        /// <param name="t2">参数2</param>
        /// <param name="t3">参数3</param>
        /// <param name="t4">参数4</param>
        /// <returns>接口对象</returns>
        TInterface GetService<TInterface, T1, T2, T3, T4>(string name, T1 t1, T2 t2, T3 t3, T4 t4);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <param name="rewrite">是否重写</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface>(Type ImplementationType,bool rewrite=true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="rewrite">如果已经注册服务，是否需要重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface>(string name, Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <param name="rewrite">是否重写</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1>(Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="rewrite">如果已经注册服务，是否需要重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1>(string name, Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <param name="rewrite">是否重写</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1, T2>(Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="rewrite">如果已经注册服务，是否需要重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1, T2>(string name, Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <param name="rewrite">是否重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1, T2, T3>(Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="rewrite">如果已经注册服务，是否需要重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1, T2, T3>(string name, Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <typeparam name="T4">参数4类型</typeparam>
        /// <param name="rewrite">如果已经注册服务，是否需要重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1, T2, T3, T4>(Type ImplementationType, bool rewrite = true);
        /// <summary>
        /// 设置服务
        /// </summary>
        /// <typeparam name="TInterface">接口</typeparam>
        /// <typeparam name="T1">参数1类型</typeparam>
        /// <typeparam name="T2">参数2类型</typeparam>
        /// <typeparam name="T3">参数3类型</typeparam>
        /// <typeparam name="T4">参数4类型</typeparam>
        /// <param name="name">注入服务名称</param>
        /// <param name="rewrite">如果已经注册服务，是否需要重写</param>
        /// <param name="ImplementationType">实现接口的类型</param>
        /// <returns>返回服务数据</returns>
        ITypeHandler SetService<TInterface, T1, T2, T3, T4>(string name, Type ImplementationType, bool rewrite = true);
    }
}
