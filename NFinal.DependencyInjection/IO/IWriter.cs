﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : IWriter.cs
//        Description :输出流接口
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Text;
#if !NET20
using System.Threading.Tasks;
#endif
namespace NFinal.IO
{
    /// <summary>
    /// 输出流接口
    /// </summary>
    public interface IWriter
    {
        /// <summary>
        /// 输出字节流
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        void Write(byte[] buffer, int offset, int count);
#if !NET20
        /// <summary>
        /// 输出字节流（异步）
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        Task WriteAsync(byte[] buffer,int offset,int count);
#endif
        /// <summary>
        /// 输出字符串
        /// </summary>
        /// <param name="value">字符串</param>
        void Write(string value);
#if !NET20
        /// <summary>
        /// 输出字符串（异步）
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        Task WriteAsync(string value);
#endif
        /// <summary>
        /// 输出视图
        /// </summary>
        /// <param name="view"></param>
        void Write(NFinal.View.IView view);
#if !NET20
        /// <summary>
        /// 输出视图(异步)
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        Task WriteAsync(NFinal.View.IView view);
#endif
    }
}
