﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : Writer.cs
//        Description :输出类
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Text;
#if !NET20
using System.Threading.Tasks;
#endif
using NFinal.View;

namespace NFinal.IO
{

#if NET20
    public delegate void WriteDelegate();
#else
    public delegate Task WriteAsyncDelegate();
#endif
    /// <summary>
    /// 
    /// </summary>
    public abstract class Writer :NFinal.DependencyInjection.ServiceCollectionHandler, IWriter
    {
        /// <summary>
        /// 输出反转义的Json字符串
        /// </summary>
        /// <param name="value"></param>
        //public void WriteJsonReverseString(string value)
        //{
        //    byte[] buffer = value.JsonEncodeBytes();
        //    Write(buffer, 0, buffer.Length);
        //}
        /// <summary>
        /// 输出字节
        /// </summary>
        /// <param name="buffer"></param>
        public void Write(byte[] buffer)
        {
            Write(buffer, 0, buffer.Length);
        }
        /// <summary>
        /// 输出Object.ToString();
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public void Write<T>(T value)
        {
            if (value != null)
            {
                Write(value.ToString());
            }
        }
#if !NET20
        public Task WriteAsync<T>(T value)
        {
            if (value != null)
            {
                return WriteAsync(value.ToString());
            }
            return NFinal.DependencyInjection.TaskHelper.CompletedTask;
        }
#endif
#if !NET20
        public Task WriteAsync(WriteAsyncDelegate writeAsync)
        {
            return writeAsync();
        }
#else
        public void Write(WriteDelegate write)
        {
            write();
        }
#endif
        /// <summary>
        /// 输出String容器类
        /// </summary>
        /// <param name="value"></param>
        public void Write(StringContainer value)
        {
            Write(value.value);
        }
#if !NET20
        public Task WriteAsync(StringContainer value)
        {
            return this.WriteAsync(value.value);
        }
#endif
        /// <summary>
        /// 输出字节流
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public abstract void Write(byte[] buffer, int offset, int count);
        /// <summary>
        /// 输出字符串
        /// </summary>
        /// <param name="value"></param>
#if !NET20
        public abstract Task WriteAsync(string value);
#endif

        public void Write(IView view)
        {
            view.Execute();
        }
#if !NET20
        public abstract Task WriteAsync(byte[] buffer, int offset, int count);

        public Task WriteAsync(IView view)
        {
            return view.ExecuteAsync();
        }
#endif

        public abstract void Write(string value);
    }
}