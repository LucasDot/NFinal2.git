﻿using System;
using System.Collections.Generic;
using System.Text;
#if !NET20
using System.Threading.Tasks;
#endif
namespace NFinal.DependencyInjection
{
    public static class TaskHelper
    {
#if !NET20
        private static Task _completedTask;
        /// <summary>
        /// 已经完成的任务
        /// </summary>
        public static Task CompletedTask
        {
            get
            {
                if (_completedTask == null)
                {
                    var tcs = new TaskCompletionSource<int>();
                    tcs.SetResult(0);
                    _completedTask = tcs.Task;
                }
                return _completedTask;
            }
        }
#endif
    }
}
