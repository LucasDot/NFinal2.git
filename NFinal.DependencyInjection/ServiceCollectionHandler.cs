﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.DependencyInjection
{
    /// <summary>
    /// 全局依赖注入类，只要继承该类，子类即可使用依赖注入功能
    /// </summary>
    public class ServiceCollectionHandler
    {
        private static NFinal.DependencyInjection.IServiceCollection _serviceCollection
            = new NFinal.DependencyInjection.ServiceCollection();
        /// <summary>
        /// 全局依赖注入对象
        /// </summary>
        public static NFinal.DependencyInjection.IServiceCollection ServiceCollection

        {
            get
            {
                return _serviceCollection;
            }
            set
            {
                _serviceCollection = value;
            }
        }
    }
}
