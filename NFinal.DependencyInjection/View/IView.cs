﻿using System;
using System.Collections.Generic;
using System.Text;
#if !NET20
using System.Threading.Tasks;
#endif
namespace NFinal.View
{

    /// <summary>
    /// 视图接口
    /// </summary>
    public interface IView
    {
        string Layout { get; set; }
        IView View { get; set; }
        NFinal.Collections.FastDictionary<string, object> ViewData { get; set; }
#if NET20
        NFinal.Collections.FastDictionary<string, NFinal.IO.WriteDelegate> Sections {get;set;}
#else
        NFinal.Collections.FastDictionary<string, NFinal.IO.WriteAsyncDelegate> Sections { get; set; }
#endif

#if NET20
        NFinal.IO.WriteDelegate RenderSection(string sectionName, bool required = true);
#else
        NFinal.IO.WriteAsyncDelegate RenderSection(string sectionName, bool required = true);
#endif
        /// <summary>
        /// 输入对象
        /// </summary>
        NFinal.IO.Writer writer { get; set; }
        /// <summary>
        /// 输出模板
        /// </summary>
        void Execute();
#if !NET20
        Task ExecuteAsync();
#endif
    }
}
