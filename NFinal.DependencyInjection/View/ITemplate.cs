﻿using System;
using System.Collections.Generic;
using System.Text;
#if !NET20
using System.Threading.Tasks;
#endif
namespace NFinal.View
{
    public interface ITemplate
    {
        string Extension { get; }
        string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName);
        string RootPath { get; set; }
#if !NET20
        Task RenderAsync(NFinal.IO.Writer writer, string url, object model);
#endif
        string Render(NFinal.IO.Writer writer, string url, object model);
    }
}
