﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    public interface IUrlHelper
    {
        string GetUrl<TController>(string methodName, params StringContainer[] parameters);
        string Content(string path);
    }
}
