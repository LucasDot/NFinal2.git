﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename : StringContainer.cs
//        Description :字符串容器类，用于基本类型的自动转换。
//
//        created by Lucas at  2015-5-31
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;

namespace NFinal
{
	/// <summary>
    /// 字符串容器类，用于基本类型的自动转换。
    /// </summary>
    public struct BinaryContainer
    {
		/// <summary>
		/// 空容器类，用于判断初始化等。
		/// </summary>
        public static readonly byte[] Empty=new BinaryContainer();
		/// <summary>
		/// bytes数组
		/// </summary>
        public byte[] value;
		/// <summary>
		/// bytes数组数据起始位置
		/// </summary>
		public int index;
		/// <summary>
		/// bytes数组数据的长度
		/// </summary>
		public int length;
		/// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="value"></param>
        internal BinaryContainer(byte[] value)
        {
            this.value = value;
			this.index=0;
			this.length=0;
        }
		/// <summary>
        /// 比较判断
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
		public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// 相等判断
        /// </summary>
        /// <param name="container"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
		public static bool operator ==(BinaryContainer container, byte[] bytes)
        {
            return container.value == bytes;
        }
        /// <summary>
        /// 不等判断
        /// </summary>
        /// <param name="container"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static bool operator !=(BinaryContainer container, byte[] bytes)
        {
            return container.value != bytes;
        }
		/// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
		public static implicit operator BinaryContainer(byte[] bytes)
		{
			return new BinaryContainer(bytes);
		}
		///// <summary>
  //      /// 把任意对象转为BinaryContainer
  //      /// </summary>
  //      /// <param name="bytes"></param>
  //      /// <returns></returns>
		//public static BinaryContainer AsBinaryContainer(this object obj)
		//{
  //          Wire.Serializer serializer = new Wire.Serializer(new Wire.SerializerOptions(true, true));
  //          byte[] buffer = null;
  //          using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
  //          {
  //              serializer.Serialize(obj, stream);
  //              buffer = stream.ToArray();
  //          }
		//	return new BinaryContainer(buffer);
		//}
  //      /// <summary>
  //      /// 把BinaryContainer转为任意对象
  //      /// </summary>
  //      /// <typeparam name="T"></typeparam>
  //      /// <param name="BinaryContainer"></param>
  //      /// <returns></returns>
  //      public static T As<T>(this BinaryContainer BinaryContainer) where T : class
  //      {
  //          if(BinaryContainer.value!=null)
		//	{
  //              Wire.Serializer serializer = new Wire.Serializer(new Wire.SerializerOptions(true, true));
  //              byte[] buffer = BinaryContainer.value;
  //              T t = null;
  //              using (System.IO.Stream stream = buffer.ToStream())
  //              {
  //                  object obj = serializer.Deserialize(stream);
  //                  t = (T)obj;
  //                  return t;
  //              }
		//	}
		//	return null;
  //      }
		/// <summary>
        /// 把SByte类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">SByte类型</param>
        public static implicit operator BinaryContainer(SByte obj)
		{
			return new BinaryContainer(new byte[] { (byte)obj });
		}
		/// <summary>
        /// 把Byte类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Byte类型</param>
        public static implicit operator BinaryContainer(Byte obj)
		{
			return new BinaryContainer(new byte[] { obj });
		}
		/// <summary>
        /// 把Int16类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Int16类型</param>
        public static implicit operator BinaryContainer(Int16 obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把UInt16类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">UInt16类型</param>
        public static implicit operator BinaryContainer(UInt16 obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把Int32类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Int32类型</param>
        public static implicit operator BinaryContainer(Int32 obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把UInt32类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">UInt32类型</param>
        public static implicit operator BinaryContainer(UInt32 obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把Int64类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Int64类型</param>
        public static implicit operator BinaryContainer(Int64 obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把UInt64类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">UInt64类型</param>
        public static implicit operator BinaryContainer(UInt64 obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把Boolean类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Boolean类型</param>
        public static implicit operator BinaryContainer(Boolean obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把Char类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Char类型</param>
        public static implicit operator BinaryContainer(Char obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把Decimal类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Decimal类型</param>
        public static implicit operator BinaryContainer(Decimal obj)
		{
			return new BinaryContainer(BitConverterExt.GetBytes(obj));
		}
		/// <summary>
        /// 把Double类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Double类型</param>
        public static implicit operator BinaryContainer(Double obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把Single类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">Single类型</param>
        public static implicit operator BinaryContainer(Single obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj));
		}
		/// <summary>
        /// 把DateTime类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">DateTime类型</param>
        public static implicit operator BinaryContainer(DateTime obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj.Ticks));
		}
		/// <summary>
        /// 把DateTimeOffset类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">DateTimeOffset类型</param>
        public static implicit operator BinaryContainer(DateTimeOffset obj)
		{
			return new BinaryContainer(BitConverter.GetBytes(obj.Ticks));
		}
		/// <summary>
        /// 把SByte类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的SByte类型</param>
        public static implicit operator BinaryContainer(SByte? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Byte类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Byte类型</param>
        public static implicit operator BinaryContainer(Byte? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Int16类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Int16类型</param>
        public static implicit operator BinaryContainer(Int16? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把UInt16类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的UInt16类型</param>
        public static implicit operator BinaryContainer(UInt16? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Int32类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Int32类型</param>
        public static implicit operator BinaryContainer(Int32? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把UInt32类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的UInt32类型</param>
        public static implicit operator BinaryContainer(UInt32? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Int64类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Int64类型</param>
        public static implicit operator BinaryContainer(Int64? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把UInt64类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的UInt64类型</param>
        public static implicit operator BinaryContainer(UInt64? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Boolean类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Boolean类型</param>
        public static implicit operator BinaryContainer(Boolean? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Char类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Char类型</param>
        public static implicit operator BinaryContainer(Char? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Decimal类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Decimal类型</param>
        public static implicit operator BinaryContainer(Decimal? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverterExt.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Double类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Double类型</param>
        public static implicit operator BinaryContainer(Double? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把Single类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的Single类型</param>
        public static implicit operator BinaryContainer(Single? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把DateTime类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的DateTime类型</param>
        public static implicit operator BinaryContainer(DateTime? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value.Ticks));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 把DateTimeOffset类型转为字节容器类，即字节数组
        /// </summary>
        /// <param name="obj">可以为null的DateTimeOffset类型</param>
        public static implicit operator BinaryContainer(DateTimeOffset? obj)
		{
            if(obj!=null)
            {
				return new BinaryContainer(BitConverter.GetBytes(obj.Value.Ticks));
            }
			return BinaryContainer.Empty;
		}
		/// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="BinaryContainer">字节容器类</param>
		public static implicit operator byte[](BinaryContainer BinaryContainer)
		{
			return BinaryContainer.value;
		}
		/// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="value">字符串</param>
		public static implicit operator BinaryContainer(string value)
        {
            return new BinaryContainer(System.Text.Encoding.UTF8.GetBytes(value));
        }
		/// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="BinaryContainer">字节容器类</param>
		public static implicit operator string(BinaryContainer BinaryContainer)
		{
			if (BinaryContainer.index == 0 && BinaryContainer.length==0)
            {
                return System.Text.Encoding.UTF8.GetString(BinaryContainer.value);
            }
            else
            {
                return System.Text.Encoding.UTF8.GetString(BinaryContainer.value,BinaryContainer.index,BinaryContainer.length);
            }
		}
		/// <summary>
        /// 把字节容器类转换为SByte类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator SByte(BinaryContainer BinaryContainer)
        {
			return (SByte)BinaryContainer.value[0];
        }
		/// <summary>
        /// 把字节容器类转换为Byte类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Byte(BinaryContainer BinaryContainer)
        {
			return BinaryContainer.value[0];
        }
		/// <summary>
        /// 把字节容器类转换为Int16类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Int16(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToInt16(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为UInt16类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator UInt16(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToUInt16(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Int32类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Int32(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToInt32(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为UInt32类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator UInt32(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToUInt32(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Int64类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Int64(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToInt64(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为UInt64类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator UInt64(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToUInt64(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Boolean类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Boolean(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToBoolean(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Char类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Char(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToChar(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Decimal类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Decimal(BinaryContainer BinaryContainer)
        {
			return BitConverterExt.ToDecimal(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Double类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Double(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToDouble(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为Single类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Single(BinaryContainer BinaryContainer)
        {
			return BitConverter.ToSingle(BinaryContainer.value,BinaryContainer.index);
        }
		/// <summary>
        /// 把字节容器类转换为DateTime类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator DateTime(BinaryContainer BinaryContainer)
        {
			return new DateTime(BitConverter.ToInt64(BinaryContainer.value,BinaryContainer.index));
        }
		/// <summary>
        /// 把字节容器类转换为DateTimeOffset类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator DateTimeOffset(BinaryContainer BinaryContainer)
        {
			return new DateTimeOffset(BitConverter.ToInt64(BinaryContainer.value,BinaryContainer.index),TimeSpan.Zero);
        }
		/// <summary>
        /// 把字节容器类转换为可为null的SByte类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator SByte?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return (SByte)BinaryContainer.value[0];
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Byte类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Byte?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BinaryContainer.value[0];
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Int16类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Int16?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToInt16(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的UInt16类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator UInt16?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToUInt16(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Int32类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Int32?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToInt32(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的UInt32类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator UInt32?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToUInt32(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Int64类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Int64?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToInt64(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的UInt64类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator UInt64?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToUInt64(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Boolean类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Boolean?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToBoolean(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Char类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Char?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToChar(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Decimal类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Decimal?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverterExt.ToDecimal(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Double类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Double?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToDouble(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的Single类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator Single?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return BitConverter.ToSingle(BinaryContainer.value,BinaryContainer.index);
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的DateTime类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator DateTime?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return new DateTime(BitConverter.ToInt64(BinaryContainer.value,BinaryContainer.index));
			}
			else
			{
				return null;
			}
        }
		/// <summary>
        /// 把字节容器类转换为可为null的DateTimeOffset类型
        /// </summary>
        /// <param name="BinaryContainer">字节容器</param>
        public static implicit operator DateTimeOffset?(BinaryContainer BinaryContainer)
        {
            if(BinaryContainer.value!=null)
			{
				return new DateTimeOffset(BitConverter.ToInt64(BinaryContainer.value,BinaryContainer.index),TimeSpan.Zero);
			}
			else
			{
				return null;
			}
        }
    }
}
