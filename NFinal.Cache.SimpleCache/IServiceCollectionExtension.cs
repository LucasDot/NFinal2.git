﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;

namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Cache_SimpleCache_Extension
    {
        /// <summary>
        /// 使用内存作为Session
        /// </summary>
        /// <param name="serviceCollection"></param>
        /// <param name="minutes">缓存时间</param>
        /// <param name="userSessionKey">用户key</param>
        public static void SetSessionSimple(this IServiceCollection serviceCollection, int minutes = 30)
        {
            serviceCollection.SetService<NFinal.Cache.ICache<string>, NFinal.Serialize.ISerializable>("session",typeof(NFinal.Cache.SimpleCache.SimpleCache)).Configure(minutes);
        }
        public static void SetCacheSimple(this IServiceCollection serviceCollection, int minutes = 30)
        {
            serviceCollection.SetService<NFinal.Cache.ICache<string>, NFinal.Serialize.ISerializable>(typeof(NFinal.Cache.SimpleCache.SimpleCache)).Configure(minutes);
        }
    }
}
