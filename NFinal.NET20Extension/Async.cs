﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Threading.Tasks
{
    public delegate void AsyncTask();
    public delegate void AsyncTask<T1>(T1 t1);
    public delegate void AsyncTask<T1, T2>(T1 t1, T2 t2);
    public delegate void AsyncTask<T1, T2, T3>(T1 t1, T2 t2, T3 t3);
    public delegate void AsyncTask<T1, T2, T3, T4>(T1 t1, T2 t2, T3 t3, T4 t4);
    public delegate R AsyncTaskR<R>();
    public delegate R AsyncTaskR<R, T>(T t);
    public delegate R AsyncTaskR<R, T1, T2>(T1 t1, T2 t2);
    public delegate R AsyncTaskR<R, T1, T2, T3>(T1 t1, T2 t2, T3 t3);
    public delegate R AsyncTaskR<R, T1, T2, T3, T4>(T1 t1, T2 t2, T3 t3, T4 t4);
    public static class Task
    {
        public static void Wait(this AsyncTask function)
        {
            function();
        }
        public static AsyncTask BeginTask(AsyncTask function)
        {
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                }
            };
        }
        public static R Wait<R>(this AsyncTaskR<R> function)
        {
            return function();
        }
        public static AsyncTaskR<R> BeginTask<R>(AsyncTaskR<R> function)
        {
            R retv = default(R);
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            retv = function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                    return retv;
                }
            };
        }
        public static AsyncTask BeginTask<T>(AsyncTask<T> function, T t1)
        {
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                }
            };
        }

        public static AsyncTaskR<R> BeginTask<R, T>(AsyncTaskR<R, T> function, T t1)
        {
            R retv = default(R);
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            retv = function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                    return retv;
                }
            };
        }
        public static AsyncTask BeginTask< T1, T2>(AsyncTask< T1, T2> function, T1 t1, T2 t2)
        {
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1, t2,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                }
            };
        }
        public static AsyncTaskR<R> BeginTask<R, T1,T2>(AsyncTaskR<R, T1,T2> function, T1 t1,T2 t2)
        {
            R retv = default(R);
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1,t2,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            retv = function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                    return retv;
                }
            };
        }
        public static AsyncTask BeginTask< T1, T2, T3>(AsyncTask< T1, T2, T3> function, T1 t1, T2 t2, T3 t3)
        {
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1, t2, t3,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                }
            };
        }
        public static AsyncTaskR<R> BeginTask<R, T1, T2,T3>(AsyncTaskR<R, T1, T2,T3> function, T1 t1, T2 t2,T3 t3)
        {
            R retv = default(R);
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1, t2, t3,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            retv = function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                    return retv;
                }
            };
        }
        public static AsyncTask BeginTask< T1, T2, T3, T4>(AsyncTask< T1, T2, T3, T4> function, T1 t1, T2 t2, T3 t3, T4 t4)
        {
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1, t2, t3, t4,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                }
            };
        }
        public static AsyncTaskR<R> BeginTask<R, T1, T2, T3,T4>(AsyncTaskR<R, T1, T2, T3,T4> function, T1 t1, T2 t2, T3 t3,T4 t4)
        {
            R retv = default(R);
            bool completed = false;

            object sync = new object();

            IAsyncResult asyncResult = function.BeginInvoke(t1, t2, t3,t4,
                    iAsyncResult =>
                    {
                        lock (sync)
                        {
                            completed = true;
                            retv = function.EndInvoke(iAsyncResult);
                            Monitor.Pulse(sync);
                        }
                    }, null);

            return delegate
            {
                lock (sync)
                {
                    if (!completed)
                    {
                        Monitor.Wait(sync);
                    }
                    return retv;
                }
            };
        }
        public static string HelloWorld()
        {
            return "Hello World!";
        }

        static void Main(string[] args)
        {
            var task = BeginTask(HelloWorld); // non-blocking call

           task.Wait(); // block and wait

        }
    }
}
