﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

#if NET20
namespace System
{
#if NET20
    public class Tuple<T1, T2>
    {
        public T1 Item1;
        public T2 Item2;
        public Tuple(T1 t1, T2 t2)
        {
            this.Item1 = t1;
            this.Item2 = t2;
        }
    }
    public class Tuple<T1, T2, T3>
    {
        public T1 Item1;
        public T2 Item2;
        public T3 Item3;
        public Tuple(T1 t1, T2 t2, T3 t3)
        {
            this.Item1 = t1;
            this.Item2 = t2;
            this.Item3 = t3;
        }
    }
    public class Tuple<T1, T2, T3, T4>
    {
        public T1 Item1;
        public T2 Item2;
        public T3 Item3;
        public T4 Item4;
        public Tuple(T1 t1, T2 t2, T3 t3,T4 t4)
        {
            this.Item1 = t1;
            this.Item2 = t2;
            this.Item3 = t3;
            this.Item4 = t4;
        }
    }

    //public delegate void Action();
    //public delegate void Action<T>(T t);
    //public delegate void Action<T1, T2>(T1 t1, T2 t2);
    //public delegate void Action<T1, T2, T3>(T1 t1, T2 t2);
    //public delegate void Action<T1, T2, T3, T4>(T1 t1, T2 t2, T3 t3, T4 t4);
    //public delegate TResult Func<TResult>();
    //public delegate TResult Func<T, TResult>(T t);
    //public delegate TResult Func<T1, T2, TResult>(T1 t1, T2 t2);
    //public delegate TResult Func<T1, T2, T3, TResult>(T1 t1, T2 t2, T3 t3);
    //public delegate TResult Func<T1, T2, T3, T4, TResult>(T1 t1, T2 t2, T3 t3, T4 t4);
#endif
}
//namespace System.Runtime.CompilerServices
//{
//    public class ExtensionAttribute : Attribute { }
//}
#endif