﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using JNTemplateEx = JinianNet.JNTemplate;
using System.Reflection;
using System.Reflection.Emit;
using NFinal.IO;
using System.Threading.Tasks;

namespace NFinal.View.JNTemplate
{
    public class JNTempalte:NFinal.View.ITemplate
    {
        public static NFinal.Logs.ILogger logger;
        public static void Configure(NFinal.Logs.ILogger logger)
        {
            JNTempalte.logger = logger;
        }
        public string RootPath { get; set; }
        public string Extension { get { return ".htm"; } }

        public string GetPath(string skinName, string assemblyName, string controllerFullName, string methodName)
        {
            string path = controllerFullName.Substring(assemblyName.Length);
            path = path.Replace('.', '/');
            if (string.IsNullOrEmpty(skinName))
            {
                path = "/" + assemblyName + "/" + path + "/" + methodName + Extension;
            }
            else
            {
                path = skinName + "/" + path + "/" + methodName + Extension;
            }
            return path;
        }
        private JNTemplateEx.Configuration.EngineConfig options;
        /// <summary>
        /// 获取绝对路径
        /// </summary>
        /// <param name="absoluteDirectory">绝对根目录</param>
        /// <param name="relativePath">相对路径</param>
        /// <returns></returns>
        public static string GetAbsolutePath(string absoluteDirectory, string relativePath)
        {
            //relativePath= relativePath.Trim('/');
            string[] relativeDirectories = relativePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string absolutePath = absoluteDirectory;
            for (int i = 0; i < relativeDirectories.Length; i++)
            {
                if (relativeDirectories[i] == "..")
                {
                    absolutePath = System.IO.Path.GetDirectoryName(absolutePath);
                }
                else if (relativeDirectories[i] == ".")
                {
                    absolutePath = absoluteDirectory;
                }
                else if (relativeDirectories[i] == "")
                {
                    absolutePath = absoluteDirectory;
                }
                else
                {
                    absolutePath = System.IO.Path.Combine(absolutePath, relativeDirectories[i]);
                }
            }
            return absolutePath;
        }
        public string Render(Writer writer, string relativePath, object model)
        {
            Type type = model.GetType();
            string filePath=GetAbsolutePath(RootPath, relativePath);

            JNTemplateEx.ITemplate template =JNTemplateEx.Engine.LoadTemplate(filePath);
            GetVariableScope(template.Context.TempData, model);
            StringWriter stringWriter = new StringWriter();
            try
            {
                template.Render(stringWriter);
                return stringWriter.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                stringWriter.Dispose();
            }
        }
        public static NFinal.Collections.FastDictionary<RuntimeTypeHandle, Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope>>
            getVarDelegate = new Collections.FastDictionary<RuntimeTypeHandle, Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope>>();
        public JNTemplateEx.Parser.VariableScope GetVariableScope(object model)
        {
            JNTemplateEx.Parser.VariableScope variableScope = new JNTemplateEx.Parser.VariableScope();
            return GetVariableScope(variableScope, model);
        }
        public JNTemplateEx.Parser.VariableScope GetVariableScope(JNTemplateEx.Parser.VariableScope variableScope, object model)
        {
            Type type = model.GetType();
            RuntimeTypeHandle runtimeTypeHandle = type.TypeHandle;
            if (!getVarDelegate.TryGetValue(runtimeTypeHandle, out Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope> GetVariableScopeDelegate))
            {
                getVarDelegate[runtimeTypeHandle] = GetVariableScopeFunc(model);
            }
            return GetVariableScopeDelegate(variableScope, model);
        }
        public class Model
        {
            public int Id;
            public string Name;
        }
        public void EmitTest()
        {
            var variableScope = new JNTemplateEx.Parser.VariableScope();
            var any = new { Id = 1, Name = "Lucas" };
            variableScope.Push("Id", any.Id);
            variableScope.Push("Name", any.Name);
            var model = new Model { Id = 1, Name = "Lucas" };
            variableScope.Push("Id", model.Id);
            variableScope.Push("Name", model.Name);
        }
        public static MethodInfo pushMethodInfo = typeof(JNTemplateEx.Parser.VariableScope)
            .GetMethod("Push", new Type[] { typeof(string), typeof(object) });
        public Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope> GetVariableScopeFunc(object model)
        {
            Type type = model.GetType();
            DynamicMethod dynamicMethod = new DynamicMethod("JNTemplate",typeof(JNTemplateEx.Parser.VariableScope)
                , new Type[] { typeof(JNTemplateEx.Parser.VariableScope), type });
            var iLGenerator = dynamicMethod.GetILGenerator();
            var variableScope= iLGenerator.DeclareLocal(typeof(JNTemplateEx.Parser.VariableScope));
            
            FieldInfo[] fields= type.GetFields();
            //List<KeyValuePair<string, FieldInfo>> fieldList = new List<KeyValuePair<string, FieldInfo>>();
            for (int i = 0; i < fields.Length; i++)
            {
                iLGenerator.Emit(OpCodes.Ldarg_0);
                iLGenerator.Emit(OpCodes.Ldstr,fields[i].Name);
                iLGenerator.Emit(OpCodes.Ldarg_1);
                iLGenerator.Emit(OpCodes.Ldfld,fields[i]);
                if (fields[i].FieldType
#if NET20 || NET40
                    .IsValueType)
#else
                    .GetTypeInfo().IsValueType)
#endif
                {
                    iLGenerator.Emit(OpCodes.Box,fields[i].FieldType);
                }
                iLGenerator.Emit(OpCodes.Callvirt,pushMethodInfo);
            }
            PropertyInfo[] propertys = type.GetProperties();
            for (int i = 0; i < propertys.Length; i++)
            {
                iLGenerator.Emit(OpCodes.Ldarg_0);
                iLGenerator.Emit(OpCodes.Ldstr, propertys[i].Name);
                iLGenerator.Emit(OpCodes.Ldarg_1);
                iLGenerator.Emit(OpCodes.Callvirt, propertys[i].GetGetMethod());
                if (propertys[i].PropertyType
#if NET20 || NET40
                    .IsValueType)
#else
                    .GetTypeInfo().IsValueType)
#endif
                {
                    iLGenerator.Emit(OpCodes.Box, propertys[i].PropertyType);
                }
                iLGenerator.Emit(OpCodes.Callvirt,pushMethodInfo);
            }
            iLGenerator.Emit(OpCodes.Ldloc, variableScope);
            iLGenerator.Emit(OpCodes.Ret);
            Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope> variableScopeFunc 
                = (Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope>)
                dynamicMethod.CreateDelegate(typeof(Func<JNTemplateEx.Parser.VariableScope,object, JNTemplateEx.Parser.VariableScope>));
            return variableScopeFunc;
        }

        public JNTempalte(string rootPath, object options)
        {
            if (options != null)
            {
                this.options = (JNTemplateEx.Configuration.EngineConfig)options;
            }
            else
            {
                this.options = JNTemplateEx.Configuration.EngineConfig.CreateDefault();
            }
            JNTemplateEx.Engine.Configure(this.options);
            this.RootPath = rootPath;
        }
#if !NET20
        public Task RenderAsync(Writer writer, string url, object model)
        {
            string content= Render(writer,url, model);
            return writer.WriteAsync(content);
        }
#else
        //public AsyncTask RenderAsync(Writer writer, string url, object model)
        //{
        //    string content = Render(writer, url, model);
        //    var task= Task.BeginTask(writer.Write, content);
        //    return task;
        //}
#endif
    }
}
