﻿using System;
using System.Collections.Generic;
using System.Text;
using NFinal.DependencyInjection;

namespace NFinal.DependencyInjection
{
    public static class IServiceCollection_Cache_RedisCache_Extension
    {
        /// <summary>
        /// 使用Redis作为Session
        /// </summary>
        /// <param name="serviceCollection"></param>
        /// <param name="minutes">缓存时间</param>
        /// <param name="configration">Redis配置</param>
        /// <param name="userSessionKey">用户key</param>
        public static void SetSessionRedis(this IServiceCollection serviceCollection, int minutes = 30, string configration = "localhost",bool rewrite=true)
        {
            serviceCollection.SetService<NFinal.Cache.ICache<string>, NFinal.Serialize.ISerializable>("session", typeof(NFinal.Cache.RedisCache.RedisCache),rewrite).Configure(configration, minutes);
        }
        public static void SetCacheRedis(this IServiceCollection serviceCollection, int minutes = 30, string configration = "localhost", bool rewrite = true)
        {
            serviceCollection.SetService<NFinal.Cache.ICache<string>, NFinal.Serialize.ISerializable>(typeof(NFinal.Cache.RedisCache.RedisCache), rewrite).Configure(configration, minutes);
        }
    }
}
