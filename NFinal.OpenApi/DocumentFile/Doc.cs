﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace NFinal.DocumentFile
{
    public class Doc
    {
        private XmlDocument doc;
        public Doc(string fileName)
        {
            doc = new XmlDocument();
            StreamReader sr = File.OpenText(fileName);
            doc.Load(sr);
            sr.Dispose();
            XmlNode assembly = doc.DocumentElement.SelectSingleNode("/doc/assembly");
            assemblyName = assembly.SelectSingleNode("name").InnerText;
            XmlNodeList memberNodeList = doc.DocumentElement.SelectNodes("/doc/members/member");
            members = new SortedDictionary<string, Member>();
            foreach (XmlNode memberNode in memberNodeList)
            {
                Member member = new Member(memberNode);
                members.Add(member.Name, member);
            }
        }
        public string assemblyName;
        public SortedDictionary<string, Member> members;
    }
}
