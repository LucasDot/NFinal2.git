﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace NFinal.DocumentFile
{
    public class Member
    {
        private XmlNode xmlNode;
        public Member(XmlNode xmlNode)
        {
            this.xmlNode = xmlNode;
            this._return = this.xmlNode.SelectSingleNode("returns")?.InnerText;
        }
        public string _name;
        public string Name
        {
            get
            {
                if (_name == null)
                {
                    XmlAttribute nameNode = this.xmlNode.Attributes["name"];
                    _name = nameNode.Value;
                }
                return _name;
            }
        }
        public string _summary;
        public string Summary
        {
            get
            {
                if (_summary == null)
                {
                    XmlNode summaryNode = this.xmlNode.SelectSingleNode("summary");
                    _summary = summaryNode.InnerText;
                }
                return _summary;
            }
        }
        public SortedList<string, Typeparam> _typeparams;
        public SortedList<string, Typeparam> Typeparams
        {
            get
            {
                if (_typeparams == null)
                {
                    _typeparams = new SortedList<string, Typeparam>();
                    XmlNodeList typeparamsNodeList = this.xmlNode.SelectNodes("typeparam");
                    Typeparam typeparam = null;
                    foreach (XmlNode typeparamsNode in typeparamsNodeList)
                    {
                        typeparam = new Typeparam();
                        typeparam.name = typeparamsNode.Attributes["name"].Value;
                        typeparam.value = typeparamsNode.InnerText;
                        _typeparams.Add(typeparam.name, typeparam);
                    }
                }
                return _typeparams;
            }
        }
        public SortedList<string, Param> _params;
        public SortedList<string, Param> Params
        {
            get
            {
                if (_params == null)
                {
                    _params = new SortedList<string, Param>();
                    XmlNodeList paramsNodeList = this.xmlNode.SelectNodes("param");
                    Param param = null;
                    foreach (XmlNode paramsNode in paramsNodeList)
                    {
                        param = new Param();
                        param.name = paramsNode.Attributes["name"].Value;
                        param.value = paramsNode.InnerText;
                        _params.Add(param.name, param);
                    }
                }
                return _params;
            }
        }
        public string _return;
        public string Return
        {
            get
            {
                return _return;
            }
        }
    }
}
