﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    public class MethodDocument
    {
        public string MethodName { get; set; }
        public string MethodDescription { get; set; }
        public WebApi.ParameterDocument parameters { get; set; }
        public WebApi.ParameterDocument Return { get; set; }
    }
}
