﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class WebApiAttribute : System.Attribute
    {
        /// <summary>
        /// 作者
        /// </summary>
        public string Author{ get; set; }
        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 最低版本支持
        /// </summary>
        public string MinVersion { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// 方法名
        /// </summary>
        public string MethodFullName { get; set; }
        /// <summary>
        /// 注释文档内容
        /// </summary>
        public MethodDocument MethodDocument { get; set; }
        /// <summary>
        /// WebApi注释
        /// </summary>
        /// <param name="author">作者</param>
        /// <param name="version">版本</param>
        /// <param name="minversion">最低版本</param>
        /// <param name="note">备注</param>
        public WebApiAttribute(string author, string version, string minversion,string note)
        {
            this.Author = author;
            this.Version = version;
            this.MinVersion = minversion;
            this.Note = note;
        }
    }
   

}
