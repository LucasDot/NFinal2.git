﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFinal.WebApi
{
    public class ParameterDocument
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
    }
}
